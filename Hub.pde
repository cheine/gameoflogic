/*
 * A Game of Logic (not to be confused with the book "The Game of Logic" by Lewis Caroll)
 * Copyright (C) 2018-2019  Christian Heine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

State makeInfos(State old, String[] infos)
{
  State s = old;
  if (infos != null && infos.length >= 2)
    for (int i = infos.length - 1; i != 0; --i)
      s = new Info(s, infos[0], infos[i]);

  return s;
}

String[] prepend(String e, String[] l)
{
  String[] r = new String[l.length+1];
  r[0] = e;
  for (int k = 0; k != l.length; ++k)
    r[k+1] = l[k];

  return r;
}

abstract class Problem
{
  String id;
  String[] requires;
  String label;
  String[] infos;
  Problem(String id, String[] requires, String label, String[] infos)
  {
    this.id = id;
    this.requires = requires;
    this.label = label;
    this.infos = infos;
  }
  abstract State run(State old);
}

class ProblemInfo extends Problem
{
  ProblemInfo(String id, String[] requires, String label, String[] infos)
  {
    super(id, requires, label, infos);
  }
  State run(State old)
  {
    State s = old;
    if (infos.length >= 2)
      for (int i = infos.length - 1; i != 0; --i)
        s = new Info(s, infos[0], infos[i]){public State click(float x, float y){State s = super.click(x, y); if (s == old) config.setSolved(id); return s;}};

    return s;
  }
}

class ProblemSet extends State
{

  State old;

  Button buttonHub = new Button("Hub", 27.5, 15, 4.5, 2);
  Map<Button, Problem> problems;
  Button selected = null;

  ProblemSet(State old, Problem[][] problems)
  {
    this.old = old;

    buttons.add(buttonHub);
    this.problems = new HashMap<Button, Problem>();
    float x = 0, y = 0;
    for (int i = 0; i != problems.length; ++i)
    {
      double w = -0.5;
      for (int j = 0; j != problems[i].length; ++j)
        w += max(2, 0.5 * ceil(2 * myTextWidth(filterFormula(problems[i][j].label)) + 1.5)) + 0.5;

      if ((x != 0 && x + w > 32) || problems[i].length == 0)
      {
        x = 0;
        ++y;
      }

      for (int j = 0; j != problems[i].length; ++j)
      {
        Button b = new Button(problems[i][j].label, x, 2.5 * y, max(2, 0.5 * ceil(2 * myTextWidth(filterFormula(problems[i][j].label)) + 1.5)), 2);
        buttons.add(b);
        this.problems.put(b, problems[i][j]);
        x += b.w + 0.5;
      }

      if (problems[i].length != 0)
        x += 1.0;
    }

    select(null);
  }

  void select(Button b)
  {
    if (selected != null)
      selected.mark = false;

    selected = b;
    if (selected != null)
      selected.mark = true;
  }

  void paint()
  {
    if (selected != null)
    {
      fill(BLACK);
      textFont(nFont);
      textAlign(LEFT);
      myText(problems.get(selected).infos[0], 0, 16);
    }

    for (Map.Entry<Button, Problem> e: problems.entrySet())
    {
      Button b = e.getKey();
      Problem p = e.getValue();

      b.enabled = true;
      for (String s: p.requires)
        b.enabled &= config.isSolved(s);

      b.done = config.isSolved(p.id);
    }

    super.paint();
  }

  State click(float x, float y)
  {
    Button b = find(x, y);
    if (b == buttonHub)
      return old;

    if (b != null && b == selected)
      return problems.get(selected) instanceof ProblemInfo ? problems.get(selected).run(this) : makeInfos(problems.get(selected).run(this), problems.get(selected).infos);

    select(b);
    redraw();

    return this;
  }

}

String formatFormulaSet(Formula[] f)
{
  if (f.length == 0)
    return "";

  String s = "{" + formatFormula(f[0]);
  for (int i = 1; i != f.length; ++i)
    s += ", " + formatFormula(f[i]);

  return s + "}";
}

String formatProof(Formula[] premises, Formula target)
{
  String s = formatFormulaSet(premises);
  s += '\u22A6' + formatFormula(target);
  return s;
}

String formatImplication(Formula[] premises, Formula target)
{
  String s = formatFormulaSet(premises);
  s += '\u22A7' + formatFormula(target);
  return s;
}

class Hub extends State
{

  class Planet extends Button
  {
    String baseLabel;
    String info;
    Problem[][] problems;

    Planet(float x, float y, String baseLabel, String info, Problem[][] problems)
    {
      super(baseLabel, x, y, 6.0, 5.5);
      this.baseLabel = baseLabel;
      this.info = info;
      this.problems = problems;
      update();
    }
    
    void update()
    {
      int solved = 0, total = 0;
      for (int i = 0; i != problems.length; ++i)
      {
        for (int j = 0; j != problems[i].length; ++j)
        {
          if (problems[i][j].id.equals(""))
            continue;

          ++total;
          if (config.isSolved(problems[i][j].id))
            ++solved;
        }
      }

      label = baseLabel + "\n" + (100 * solved) / max(1, total) + "%";
    }
  }

  State old;
  Planet curPlanet;
  boolean update = false;

  Hub(State old)
  {
    this.old = old;
    buttons.add(new Planet( 0.0, 3.0, "\u2112\u1D00\u029F\nSyntax", "Syntax der formalen Aussagenlogik", makeProblemsLProp()));
    buttons.add(new Planet( 6.5, 3.0, "\u2112\u1D00\u029F\nSemantik", "Semantik der formalen Aussagenlogik", makeProblemsSemanticsLProp()));
    buttons.add(new Planet(13.0, 3.0, "Eigenschaften\nBeziehungen", "Logische Eigenschaften und Beziehungen", makeProblemsTruthTable()));
    buttons.add(new Planet(19.5, 3.0, "\u2112\u1D00\u029F\nTableaux", "Tableaux f\u00FCr Aussagenlogik", makeProblemsTruthTreePropositional()));
    buttons.add(new Planet(26.0, 3.0, "\u2112\u1D00\u029F\nKNS", "Kalk\u00FCl nat\u00FCrlichen Schlie\u00DFens f\u00FCr Aussagenlogik", makeProblemsProof(true)));
    buttons.add(new Planet( 0.0, 9.0, "\u2112\u1D18\u029F\u2081\nSyntax", "Syntax der formalen Pr\u00E4dikatenlogik", makeProblemsLPred()));
    buttons.add(new Planet( 6.5, 9.0, "\u2112\u1D18\u029F\u2081\nSemantik", "Semantik der formalen Pr\u00E4dikatenlogik", makeProblemsSemanticsLPred()));
    buttons.add(new Planet(19.5, 9.0, "\u2112\u1D18\u029F\u2081\nTableaux", "Tableaux f\u00FCr Pr\u00E4dikatenlogik", makeProblemsTruthTreePredicate()));
    buttons.add(new Planet(26.0, 9.0, "\u2112\u1D18\u029F\u2081\nKNS", "Kalk\u00FCl nat\u00FCrlichen Schlie\u00DFens f\u00FCr Pr\u00E4dikatenlogik", makeProblemsProof(false)));

    //saveDependencies();

    setCurPlanet(null);
  }

  void saveDependencies()
  {
    List<String> deps = new ArrayList<String>();
    deps.add("strict digraph {");
    deps.add("  rankdir=\"LR\"");
    deps.add("  ranksep=\"2.5 equally\"");

    int curcolor = 0;
    int[] colors = {8,3,9,6,4,1,7,5,2};
    for (Button b: buttons)
    {
      if (!(b instanceof Planet))
        continue;

      Planet pl = (Planet)b;
      deps.add("  subgraph cluster_" + curcolor + " {");
      for (Problem[] ps: pl.problems)
      {
        for (Problem p: ps)
        {
          if (p.id.equals(""))
            continue;

          deps.add("    " + p.id + " [shape=box, style=filled, fillcolor=\"/pastel19/" + colors[curcolor] + "\"]");
          for (String s: p.requires)
            deps.add("    " + s + " -> " + p.id);
        }
      }
      deps.add("  }");

      curcolor++;
    }
    deps.add("}");
    saveStrings("dependencies.dot", deps.toArray(new String[0]));
  }

  void setCurPlanet(Planet p)
  {
    if (curPlanet != null)
      curPlanet.mark = false;

    curPlanet = p;
    if (curPlanet != null)
      curPlanet.mark = true;
  }

  void paint()
  {
    for (Button b: buttons)
    {
      if (!(b instanceof Planet))
        continue;

      Planet pl = (Planet)b;
      pl.enabled = false;
      for (Problem[] ps: pl.problems)
      {
        for (Problem p: ps)
        {
          boolean all = true;
          for (String s: p.requires)
            all &= config.isSolved(s);

          pl.enabled |= all;
        }
      }
    }

    fill(BLACK);
    textAlign(LEFT);
    textFont(eFont);
    scale(2.0);
    myText("Hub", 0, 0);
    scale(0.5);

    textFont(nFont);
    if (curPlanet != null)
      myText(curPlanet.info, 0, 16);

    if (update)
    {
      curPlanet.update();
      update = false;
    }

    super.paint();
  }

  State click(float x, float y)
  {
    Button b = find(x, y);
    if (b != null && b == curPlanet)
    {
      update = true;
      return new ProblemSet(this, curPlanet.problems);
    }

    setCurPlanet((Planet)b);
    redraw();
    return this;
  }
}

Formula make(boolean propositional, boolean exotic, int sentences, int operators)
{
  RandomFormula r = new RandomFormula(numMarks(propositional ? opSentence : opIndConst), propositional, exotic);
  Formula f;
  Set<Integer> s;
  do
  {
    f = r.generate(operators);
    s = new TreeSet<Integer>();
    f.allTerminals(propositional ? opSentence : opIndConst, s);
  } while (s.size() != sentences);
  return f;
}

static class Range
{
  String name;
  int min, max, value;
  Range(String name, int min, int max, int value)
  {
    this.name = name;
    this.min = min;
    this.max = max;
    this.value = value;
  }
}

class ParamSelect extends State
{
  State old;
  Range[] ranges;

  Button buttonDone = new Button("Fertig", 12.5, 12.5, 7.0, 2);

  ParamSelect(State old, Range[] ranges)
  {
    this.old = old;
    this.ranges = ranges;

    buttons.add(buttonDone);
    for (int i = 0; i != ranges.length; ++i)
    {
      buttons.add(new Button("-", 19, 11 - 1.5 * i, 1, 1));
      buttons.add(new Button("+", 21, 11 - 1.5 * i, 1, 1));
    }
  }

  void paint()
  {
    old.paint();
    noStroke();
    fill(WHITE, 191);
    rect(0, 0, 32, 17);
    fill(LTGRAY);
    rect(9.5, 12 - 1.5 * ranges.length, 13, 3 + 1.5 * ranges.length, 1);

    fill(BLACK);
    textFont(nFont);
    textAlign(RIGHT);
    for (int i = 0; i != ranges.length; ++i)
      myText(ranges[i].name + ":", 18.5, 11 - 1.5 * i);

    textAlign(CENTER);
    for (int i = 0; i != ranges.length; ++i)
      myText("" + ranges[i].value, 20.5, 11 - 1.5 * i);

    super.paint();
  }

  State click(float x, float y)
  {
    if (buttonDone.inside(x, y))
      return old;

    for (int i = 0; i != ranges.length; ++i)
    {
      if (buttons.get(2*i+1).inside(x, y) && ranges[i].value > ranges[i].min)
        --ranges[i].value;
      if (buttons.get(2*i+2).inside(x, y) && ranges[i].value < ranges[i].max)
        ++ranges[i].value;
    }

    redraw();
    return this;
  }
}

Problem[][] makeProblemsLProp()
{
  return new Problem[][]{
    {
      new ProblemInfo("lang_formal", new String[]{},
        "Formale Logik", toArray(translations.getJSONArray("lang_formal")))
    },
    {
    },
    {
      new ProblemLanguageMarks("lang_propositional_signs", new String[]{"lang_formal"},
        "Vokabular", true, toArray(translations.getJSONArray("lang_propositional_signs")))
    },
    {
    },
    {
      new ProblemSyntaxIntro("lang_propositional_syntax", new String[]{"lang_propositional_signs"}, "Syntax",
        toArray(translations.getJSONArray("lang_propositional_syntax")), true, new String[]{"-p1", "&p0p1", "-vp0p1", ">p0-p1", "#p2p2", "&vp0p1p2", ">p0#p1p2"}, true),
      new ProblemSyntaxRandom(new String[]{"lang_propositional_syntax"}, "Zufall", true, true),
      new ProblemWFF("lang_propositional_wff", new String[]{"lang_propositional_syntax"},
        "WFF erkennen", new String[]{"Wohlgeformte Formeln identifizieren"}, true),
      new ProblemFormulaOperators("lang_propositional_formula_operators", new String[]{"lang_propositional_syntax"},
        "Hauptoperator", toArray(translations.getJSONArray("lang_propositional_formula_operators")), true, true),
      new ProblemFormulaType("lang_propositional_formula_types", new String[]{"lang_propositional_formula_operators"},
        "Formeltyp", toArray(translations.getJSONArray("lang_propositional_formula_types")), true, true)
    },
    {
    },
    {
      new ProblemSyntaxIntro("lang_propositional_brackets", new String[]{"lang_propositional_syntax"}, "Klammern",
        toArray(translations.getJSONArray("lang_propositional_brackets")), true, new String[]{"&p0p1", "&-p0p1", "vvp0p1p2", ">vp1p2&p2p3"}, false),
      new ProblemSyntaxRandom(new String[]{"lang_propositional_brackets"}, "Zufall", true, false),
      new ProblemFormulaOperators("lang_propositional_formula_bracket_operators", new String[]{"lang_propositional_brackets", "lang_propositional_formula_operators"},
        "Hauptoperator", new String[]{"Bestimme Hauptoperator"}, true, false)
    }
  };
}

Problem[][] makeProblemsLPred()
{
  return new Problem[][]{
    {
      new ProblemLanguageMarks("lang_predicate_signs", new String[]{"lang_propositional_signs"}, "Vokabular", false, toArray(translations.getJSONArray("lang_predicate_signs")))
    },
    {
    },
    {
      new ProblemSyntaxIntro("lang_predicate_syntax", new String[]{"lang_predicate_signs", "lang_propositional_syntax"}, "Syntax",
        toArray(translations.getJSONArray("lang_predicate_syntax")), false, new String[]{"P1a2", "R2a1a4", "-=a1a2", ">P2a1-R1a0a0", "Ax0P0x0", "Ex0Ax1R0x0x1", "Ax0>P0x0Ex1=x0x1"}, true),
      new ProblemSyntaxRandom(new String[]{"lang_predicate_syntax"}, "Zufall", false, true),
      new ProblemWFF("lang_predicate_wff", new String[]{"lang_predicate_syntax"},
        "WFF erkennen", new String[]{"Wohlgeformte Formeln identifizieren"}, false),
      new ProblemFormulaOperators("lang_predicate_formula_operators", new String[]{"lang_predicate_syntax", "lang_propositional_formula_operators"},
        "Hauptoperator", new String[]{"Hauptoperator"}, false, true),
      new ProblemFormulaType("lang_predicate_formula_types", new String[]{"lang_propositional_formula_types", "lang_predicate_formula_operators"},
        "Formeltyp", toArray(translations.getJSONArray("lang_predicate_formula_types")), false, true)
    }
  };
}
