/*
 * A Game of Logic (not to be confused with the book "The Game of Logic" by Lewis Caroll)
 * Copyright (C) 2018-2019  Christian Heine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

class Proof extends State
{

  class Line
  {
    List<Integer> deps;
    Formula f;
    String rule;

    Line(List<Integer> deps, Formula f, String rule)
    {
      this.deps = deps;
      this.f = f;
      this.rule = rule;
    }

    String formatDeps()
    {
      String r = "{";
      if (!deps.isEmpty())
      {
        r += (deps.get(0) + 1);
        for (int i = 1; i != deps.size(); ++i)
          r += "," + (deps.get(i) + 1);
      }
      return r + "}";
    }
  }

  class MyFormulaEditor extends FormulaEditor
  {
    Proof p;
    Formula orMode = null;

    MyFormulaEditor(Proof p)
    {
      super(p, p.propositional, false, false);
      this.p = p;
    }

    void paint()
    {
      super.paint();

      if (orMode != null)
      {
        fill(BLACK);
        textFont(nFont);
        textAlign(CENTER);
        myText("Erweitere die Formel '" + formatFormula(orMode) + "'", 16, 0);
      }
    }

    void setOrMode(Formula f)
    {
      orMode = f;
      if (orMode == null)
        return;

      for (Button b: buttons)
      {
        if (b instanceof FormulaBox)
        {
          FormulaBox fb = (FormulaBox)b;
          if (fb.f.equals(f))
          {
            setFormula(fb);
            return;
          }
        }
      }

      addFormula(f, true);
    }

    State click(float x, float y)
    {
      State s = super.click(x, y);

      if (s == p)
      {
        if (curFormula == null)
          return s;

        if (orMode != null)
        {
          if (curFormula.f == orMode)
            return s;

          if (curFormula.f.op != opOR || !(curFormula.f.get(0).equals(orMode) || curFormula.f.get(1).equals(orMode)))
          {
            soundDing.play();
            return this;
          }

          p.addLine(new int[]{p.curline}, new int[]{}, new int[]{}, new Formula(curFormula.f), "$v$E");
        }
        else
        {
          p.addLine(new int[]{}, new int[]{p.lines.size()}, new int[]{}, curFormula.f, "A");
        }
      }
      
      return s;
    }
  }

  class AndElim extends State
  {
    Proof p;

    AndElim(Proof p)
    {
      this.p = p;
      buttons.add(new Button(formatFormula(p.lines.get(p.curline).f.get(0)), 7.5, 10.0, 17, 2));
      buttons.add(new Button(formatFormula(p.lines.get(p.curline).f.get(1)), 7.5, 12.5, 17, 2));
    }

    void paint()
    {
      p.paint();
      noStroke();
      fill(WHITE, 191);
      rect(0, 0, 32, 17);
      fill(LTGRAY);
      rect(7, 9.5, 18, 5.5, 1);
      super.paint();
    }

    State click(float x, float y)
    {
      Button b = find(x, y);
      if (b == null)
        return p;

      p.addLine(new int[]{curline}, new int[]{}, new int[]{}, p.lines.get(p.curline).f.get(b == buttons.get(0) ? 0 : 1), "$&$B");
      return p;
    }
  }

  class ForallElim extends State
  {
    Proof p;
    int n = numMarks(opIndConst);

    ForallElim(Proof p)
    {
      this.p = p;
      for (int i = 0; i != n; ++i)
        buttons.add(new Button(formatMark(opIndConst, i), 16 - 0.5 * (n * 2.5 - 0.5) + i * 2.5, 7.5, 2, 2));
    }

    void paint()
    {
      p.paint();
      noStroke();
      fill(WHITE, 191);
      rect(0, 0, 32, 17);
      fill(LTGRAY);
      rect((16 - 0.5 * (n * 2.5 - 0.5) - 0.5), 7, (n * 2.5 + 0.5), 3, 1);
      super.paint();
    }

    State click(float x, float y)
    {
      Button b = find(x, y);
      if (b == null)
      {
        soundDing.play();
        return this;
      }

      int i = 0;
      while (i < buttons.size() && buttons.get(i) != b)
        ++i;

      Line l = p.lines.get(p.curline);
      Formula f = l.f;
      Formula o = f.get(0);
      Formula n = new Formula(opIndConst, i);
      p.addLine(new int[]{p.curline}, new int[]{}, new int[]{}, new Formula(f.get(1), o, n),
        "$A$B(" + formatFormula(n) + "/" + formatFormula(o) + ")");

      return p;
    }
  }
  
  class ForallIntro extends SelectSentenceCore
  {
    Proof p;

    ForallIntro(Proof p)
    {
      super(p, p.lines.get(p.curline).f, opFORALL);
      this.p = p;

      for (int l: lines.get(p.curline).deps)
      {
        List<Integer> cl = p.lines.get(l).f.allTerminals(opIndConst);
        for (Button b: buttons.get(0))
          if (cl.contains(value.get(b)))
            b.enabled = false;
      }

      selected = new int[buttons.size()];
      for (int i = 0; i != selected.length; ++i)
      {
        int j = 0;
        while (j < buttons.get(i).length && !buttons.get(i)[j].enabled)
          j++;

        selected[i] = j;
        buttons.get(i)[selected[i]].mark = true;
      }

      updateFormula();
    }

    State click(float x, float y)
    {
      State r = super.click(x, y);
      if (r == p)
        p.addLine(new int[]{p.curline}, new int[]{}, new int[]{}, f,
          "$A$E(" + formatMark(opIndVar, value.get(buttons.get(1)[selected[1]])) + "/" + formatMark(opIndConst, value.get(buttons.get(0)[selected[0]])) + ")");

      return r;
    }
  }

  class ReplaceSome extends State
  {
    Proof p;
    Formula f, fo, fn;

    Button buttonDone = new Button("Fertig", (16 - 3.5), 10.0, 7, 2);
    Map<Button, Formula> lookup = new HashMap<Button, Formula>();

    void number(Formula f)
    {
      if (f.equals(fo))
      {
        f.op = fn.op;
        f.value = fn.value;
        Button b = new Button("", 2.5 * lookup.size(), 7.5, 2, 2);
        b.mark = true;
        lookup.put(b, f);
      }

      for (Formula sf: f.subformulas)
        number(sf);
    }

    ReplaceSome(Proof p, Formula f, Formula fo, Formula fn)
    {
      this.p = p;
      this.f = new Formula(f);
      this.fo = fo;
      this.fn = fn;

      number(this.f);
      if (lookup.isEmpty())
        throw new RuntimeException("No instances");

      for (Button b: lookup.keySet())
      {
        b.x += 16 - 0.5 * (lookup.size() * 2.5 - 0.5);
        buttons.add(b);
      }

      buttons.add(buttonDone);
    }

    void paint()
    {
      p.paint();
      noStroke();
      fill(WHITE, 191);
      rect(0, 0, 32, 17);

      fill(LTGRAY);
      String s = formatFormula(f);
      float N = max(7.0, max(2.5 * lookup.size() - 0.5, myTextWidth(s)));
      rect((16 - 0.5 * N - 0.5), 4.5, (N + 1.0), 8, 1);
      fill(BLACK);
      textAlign(CENTER);
      myText(s, 16, 5.5);
      super.paint();
    }

    State click(float x, float y)
    {
      Button b = find(x, y);
      if (b == null)
      {
        soundDing.play();
        return this;
      }

      if (b == buttonDone)
      {
        boolean atLeastOne = false;
        for (Map.Entry<Button, Formula> e: lookup.entrySet())
          atLeastOne |= e.getValue().equals(fn);

        if (!atLeastOne)
        {
          soundDing.play();
          return this;
        }

        return p;
      }

      b.mark = !b.mark;
      Formula f = lookup.get(b);
      if (b.mark)
      {
        f.op = fn.op;
        f.value = fn.value;
      }
      else
      {
        f.op = fo.op;
        f.value = fo.value;
      }
      redraw();

      return this;
    }
  }

  class ExistsIntroSubst extends ReplaceSome
  {
    Proof p;
    ExistsIntroSubst(Proof p, Formula of, Formula nf)
    {
      super(p, p.lines.get(curline).f, of, nf);
      f = new Formula(opEXISTS, -1, new Formula[]{nf, f});
      this.p = p;
    }
    State click(float x, float y)
    {
      State r = super.click(x, y);
      if (r == p)
        p.addLine(new int[]{p.curline}, new int[]{}, new int[]{}, f,
          "$E$E(" + formatFormula(fn) + "/" + formatFormula(fo) + ")");

      return r;
    }
  }

  class ExistsIntro extends SelectSentenceCore
  {
    Proof p;
    ExistsIntro(Proof p)
    {
      super(p, p.lines.get(curline).f, opEXISTS);
      this.p = p;
    }
    State click(float x, float y)
    {
      State r = super.click(x, y);
      if (r == p)
        return new ExistsIntroSubst(p, new Formula(opIndConst, value.get(buttons.get(0)[selected[0]])), new Formula(opIndVar, value.get(buttons.get(1)[selected[1]])));

      return r;
    }
  }

  class IdentityElim extends ReplaceSome
  {
    int lines[];
    IdentityElim(Proof p, Formula f, Formula fo, Formula fn, int[] lines)
    {
      super(p, f, fo, fn);
      this.lines = lines;
    }
    State click(float x, float y)
    {
      State r = super.click(x, y);
      if (r == p)
        p.addLine(lines, new int[]{}, new int[]{}, f, "$=$E");

      return r;
    }
  }

  class IdentityIntro extends State
  {
    Proof p;
    int n = numMarks(opIndConst);
    IdentityIntro(Proof p)
    {
      this.p = p;
      for (int i = 0; i != n; ++i)
        buttons.add(new Button(formatMark(opIndConst, i), 16 - 0.5 * (n * 2.5 - 0.5) + i * 2.5, 7.5, 2, 2));
    }
    void paint()
    {
      p.paint();
      noStroke();
      fill(WHITE, 191);
      rect(0, 0, 32, 17);
      fill(LTGRAY);
      rect(16 - 0.5 * (n * 2.5 - 0.5) - 0.5, 7, (n * 2.5 + 0.5), 3, 1);
      super.paint();
    }
    State click(float x, float y)
    {
      Button b = find(x, y);
      if (b == null)
      {
        soundDing.play();
        return this;
      }

      int i = 0;
      while (i < buttons.size() && buttons.get(i) != b)
        ++i;

      Formula s = new Formula(opIndConst, i);
      Formula f = new Formula(opIdentity, -1, new Formula[]{s, s});
      p.addLine(new int[]{}, new int[]{}, new int[]{}, f, "$=$I");

      return p;
    }
  }

  State old;
  String id;
  boolean propositional;
  Formula[] premises;
  Formula target;
  int steps;
  String hint, lesson;

  Button buttonDone = new Button("Fertig", 27.5, 15.0, 4.5, 2);
  Button buttonUndo = new Button("\u232B", 25.0, 15.0, 2.0, 2);
  Button buttonHelp = new Button("\u2139", 25.0, 12.5, 2.0, 2);
  Button buttonHint = new Button("Tipp",   27.5, 12.5, 4.5, 2);

  Button buttonA    = new Button("A",    25.0, 0.0, 2, 2);
  Button buttonANDI = new Button("$&$E", 27.5, 0.0, 2, 2);
  Button buttonANDE = new Button("$&$B", 30.0, 0.0, 2, 2);
  Button buttonIMPI = new Button("$>$E", 25.0, 2.5, 2, 2);
  Button buttonIMPE = new Button("$>$B", 25.0, 5.0, 2, 2);
  Button buttonORI  = new Button("$v$E", 27.5, 2.5, 2, 2);
  Button buttonORE  = new Button("$v$B", 27.5, 5.0, 2, 2);
  Button buttonNOTI = new Button("$-$E", 30.0, 2.5, 2, 2);
  Button buttonNOTE = new Button("$-$B", 30.0, 5.0, 2, 2);

  Button buttonFORALLI   = new Button("$A$E", 25.0,  7.5, 2, 2);
  Button buttonFORALLE   = new Button("$A$B", 25.0, 10.0, 2, 2);
  Button buttonEXISTSI   = new Button("$E$E", 27.5,  7.5, 2, 2);
  Button buttonEXISTSE   = new Button("$E$B", 27.5, 10.0, 2, 2);
  Button buttonIdentityI = new Button("$=$E", 30.0,  7.5, 2, 2);
  Button buttonIdentityE = new Button("$=$B", 30.0, 10.0, 2, 2);

  List<Line> lines;
  MyFormulaEditor fe;
  int curline;
  Button curOperator = null;
  int offset = 0;
  boolean changed = false;

  Proof(State old, String id, boolean propositional, Formula premises[], Formula target, int steps, String hint, String lesson)
  {
    this.old = old;
    this.id = id;
    this.propositional = propositional;
    this.premises = premises;
    this.target = target;
    this.steps = steps;
    this.hint = hint;
    this.lesson = lesson;

    buttons.add(buttonDone);
    buttons.add(buttonUndo);
    buttons.add(buttonHelp);
    buttons.add(buttonHint);
    buttons.add(buttonA);
    buttons.add(buttonIMPE);
    buttons.add(buttonIMPI);
    buttons.add(buttonNOTE);
    buttons.add(buttonNOTI);
    buttons.add(buttonANDE);
    buttons.add(buttonANDI);
    buttons.add(buttonORE);
    buttons.add(buttonORI);

    if (!propositional)
    {
      buttons.add(buttonFORALLE);
      buttons.add(buttonFORALLI);
      buttons.add(buttonEXISTSE);
      buttons.add(buttonEXISTSI);
      buttons.add(buttonIdentityE);
      buttons.add(buttonIdentityI);
    }

    lines = new ArrayList<Line>();
    fe = new MyFormulaEditor(this);

    setCurline(-1);
  }

  void addLine(int[] base_deps, int[] plus, int[] minus, Formula f, String rule)
  {
    List<Integer> deps = new ArrayList<Integer>();
    for (int i: plus)
      deps.add(i);

    boolean first = true;
    for (int k: minus)
    {
      rule += (first ? " " : ",") + (k+1) + "!";
      first = false;
    }

    for (int n: base_deps)
    {
      List<Integer> ndeps = lines.get(n).deps;
      int i = 0, j = 0;
      while (i < deps.size() && j < ndeps.size())
      {
        if (deps.get(i) <= ndeps.get(j))
        {
          if (deps.get(i) == ndeps.get(j))
            j++;

          i++;
        }
        else
        {
          deps.add(i, ndeps.get(j++));
        }
      }

      while (j < ndeps.size())
        deps.add(ndeps.get(j++));

      rule += (first ? " " : ",") + (n + 1);
      first = false;
    }

    for (int i: minus)
      deps.remove(new Integer(i));

    lines.add(new Line(deps, f, filterFormula(rule)));
    offset = max(0, lines.size() - 14);
    setOperator(null);
    setCurline(lines.size()-1);
    setChanged(true);
  }
  
  void setChanged(boolean changed)
  {
    this.changed = changed;
  }

  void addAssertion(Formula f)
  {
    addLine(new int[]{}, new int[]{lines.size()}, new int[]{}, f, "A");
  }

  void paint()
  {
    fill(BLACK);
    textAlign(LEFT);
    textFont(nFont);
    if (target != null)
    {
      String s = "Zu zeigen: '" + formatProof(premises, target) + "'" + (steps == 0 ? "" : " (≥" + steps + " Zeilen)");
      myText(s, 0, 0);
    }
    else if (curline != -1)
    {
      Line cl = lines.get(curline);
      Formula[] f = new Formula[cl.deps.size()];
      int n = 0;
      for (Integer l: cl.deps)
        f[n++] = lines.get(l).f;

      String s = "Zeile zeigt: '" + formatProof(f, cl.f) + "'";
      myText(s, 0, 0);
    }

    noStroke();
    for (int i = -1; i < 15; ++i)
    {
      int l = i + offset;
      if (l < 0 || lines.size() <= l)
        continue;

      if (curline == l)
      {
        fill(GRAY);
        myRect(0, (i + 2), 24.5, 1);
      }
      fill(BLACK);
      textAlign(RIGHT);
      myText((l + 1) + ":", 1.5, i + 2);
      myText(lines.get(l).rule, 24.5, i + 2);
      textAlign(LEFT);
      myText(lines.get(l).formatDeps(), 1.5, i + 2);
      myText(formatFormula(lines.get(l).f), 6.0, i + 2);
    }

    for (int i = 0; i < 20; ++i)
    {
      fill(WHITE, 64);
      rect(0, 1, 24.5, i / 20.0);
    }

    for (int i = 0; i < 20; ++i)
    {
      fill(WHITE, 64);
      rect(0, 17 - i / 20.0, 24.5, i / 20.0);
    }

    super.paint();
  }

  String checkProof()
  {
    if (lines.isEmpty())
      return "Leerer Beweis";

    Line line = lines.get(lines.size() - 1);
    if (!line.f.equals(target))
      return "Formel stimmt nicht mit Beweisziel \u00FCberein!";

    List<String> need = new ArrayList<String>();
    for (Formula f: premises)
      need.add(formatFormula(f));

    List<String> have = new ArrayList<String>();
    for (int i: line.deps)
      have.add(formatFormula(lines.get(i).f));
    
    for (String s: have)
      if (!need.contains(s))
        return "Zus\u00E4tzliche Annahme: '" + s + "'!";

    /*for (String s: need)
      if (!have.contains(s))
        return "Fehlende Pr\u00E4misse: '" + s + "'!";*/

    return null;
  }

  boolean canApply(Button b, Line l)
  {
    if (b == buttonDone || b == buttonHelp)
      return true;

    if (b == buttonUndo)
      return !lines.isEmpty();

    if (b == buttonHint)
      return hint != null && !hint.equals("");

    if (curOperator == buttonHelp)
      return true;

    return (/*l == null &&*/ (b == buttonA || b == buttonIdentityI)) ||
      (l != null && (b == buttonANDI || b == buttonORI ||
        (b == buttonIMPE && l.f.op == opIFTHEN) ||
        (b == buttonIMPI && l.rule.equals("A")) ||
        (b == buttonNOTE && l.f.op == opNOT && l.f.get(0).op == opNOT) ||
        (b == buttonNOTI && l.f.op == opIFTHEN && l.f.get(1).op == opAND &&
          l.f.get(1).get(0).negate().equals(l.f.get(1).get(1))) ||
        (b == buttonANDE && l.f.op == opAND) ||
        (b == buttonORE && l.f.op == opOR) ||
        (b == buttonFORALLE && l.f.op == opFORALL) ||
        (b == buttonFORALLI && l.f.allTerminals(opIndVar).size() < 3 && !l.f.allTerminals(opIndConst).isEmpty()) ||
        (b == buttonEXISTSE && l.f.op == opEXISTS) ||
        (b == buttonEXISTSI && l.f.allTerminals(opIndVar).size() < 3 && !l.f.allTerminals(opIndConst).isEmpty()) ||
        (b == buttonIdentityE && l.f.op == opIdentity && !l.f.get(0).equals(l.f.get(1)))));
  }

  State click(float x, float y)
  {
    Button b = find(x, y);
    if (curOperator == buttonHelp)
    {
      setOperator(null);
      setCurline(-1);
      Map<String, String[]> trans = getStrings("proof");
      if (b == buttonA)
        return makeInfos(this, trans.get("assert"));
      if (b == buttonIMPE)
        return makeInfos(this, trans.get("cond_elim"));
      if (b == buttonIMPI)
        return makeInfos(this, trans.get("cond_intro"));
      if (b == buttonNOTE)
        return makeInfos(this, trans.get("neg_elim"));
      if (b == buttonNOTI)
        return makeInfos(this, trans.get("neg_intro"));
      if (b == buttonANDE)
        return makeInfos(this, trans.get("conj_elim"));
      if (b == buttonANDI)
        return makeInfos(this, trans.get("conj_intro"));
      if (b == buttonORE)
        return makeInfos(this, trans.get("adj_elim"));
      if (b == buttonORI)
        return makeInfos(this, trans.get("adj_intro"));
      if (b == buttonFORALLE)
        return makeInfos(this, trans.get("forall_elim"));
      if (b == buttonFORALLI)
        return makeInfos(this, trans.get("forall_intro"));
      if (b == buttonEXISTSE)
        return makeInfos(this, trans.get("exists_elim"));
      if (b == buttonEXISTSI)
        return makeInfos(this, trans.get("exists_intro"));
      if (b == buttonIdentityE)
        return makeInfos(this, trans.get("identity_elim"));
      if (b == buttonIdentityI)
        return makeInfos(this, trans.get("identity_intro"));
      if (b == buttonHelp)
        return makeInfos(this, trans.get("propositional_intro"));
    }
    else if (b == buttonHelp)
    {
      setOperator(b);
      setCurline(-1);
      return new Message(this, "Hilfe", "W\u00E4hle Hilfe nochmal um allgemeine Hilfe zum Kalk\u00FCl nat\u00FCrlichen Schlie\u00DFens zu erhalten. W\u00E4hle eine Schaltfl\u00E4che f\u00FCr Regeln um Hilfe zu dieser Regel zu erhalten.", new String[]{"zur\u00FCck"});
    }
    else if (b == buttonDone)
    {
      if (lines.isEmpty() || !changed)
        return old;

      if (target == null)
        return new Message(this, "Abbrechen?", "", new String[]{"zur Aufgabe", "zum Men\u00FC"});

      String error = checkProof();
      if (error == null)
      {
        if (id != null && !id.equals(""))
        {
          config.setSolved(id);
          /*PrintWriter output = createWriter(id + ".txt");
          int i = 0;
          for (Line l: lines)
            output.println(++i + l.formatDeps() + ":\t" + formatFormula(l.f) + "\t" + l.rule);

          output.close();*/
        }

        setChanged(false);
        soundWin.play();
        return new Message(this, "Richtig", lesson, new String[]{"zur Aufgabe", "zum Men\u00FC"});
      }
      else
      {
        soundDing.play();
        return new Message(this, "Nicht richtig", error, new String[]{"zur Aufgabe", "zum Men\u00FC"});
      }
    }
    else if (b == buttonHint && hint != null && !hint.equals(""))
    {
      return new Message(this, "Hinweis", hint, new String[]{"zur Aufgabe"});
    }
    else if (b == buttonUndo)
    {
      if (lines.isEmpty())
      {
        soundDing.play();
        return this;
      }

      setChanged(true);
      lines.remove(lines.size()-1);
      offset = max(0, lines.size() - 14);
      setOperator(null);
      setCurline(-1);
    }
    else if (b == buttonA)
    {
      fe.setOrMode(null);
      return fe;
    }
    else if (b == buttonIMPE || b == buttonIMPI || b == buttonNOTE || b == buttonNOTI || b == buttonANDE || b == buttonANDI || b == buttonORE || b == buttonORI ||
      b == buttonFORALLE || b == buttonFORALLI || b == buttonEXISTSE || b == buttonEXISTSI || b == buttonIdentityE)
    {
      if (curline == -1 || !canApply(b, lines.get(curline)))
      {
        soundDing.play();
        return this;
      }
      else if (b == buttonNOTE)
      {
        addLine(new int[]{curline}, new int[]{}, new int[]{}, lines.get(curline).f.get(0).get(0), b.label);
      }
      else if (b == buttonNOTI)
      {
        addLine(new int[]{curline}, new int[]{}, new int[]{}, lines.get(curline).f.get(0).negate(), b.label);
      }
      else if (b == buttonANDE)
      {
        return new AndElim(this);
      }
      else if (b == buttonORI)
      {
        fe.setOrMode(lines.get(curline).f);
        return fe;
      }
      else if (b == buttonFORALLE)
      {
        return new ForallElim(this);
      }
      else if (b == buttonFORALLI)
      {
        try
        {
          return new ForallIntro(this);
        }
        catch (Exception e)
        {
          soundDing.play();
          return this;
        }
      }
      else if (b == buttonEXISTSI)
      {
        try
        {
          return new ExistsIntro(this);
        }
        catch (Exception e)
        {
          soundDing.play();
          return this;
        }
      }
      else
      {
        setOperator(b);
      }
    }
    else if (b == buttonIdentityI)
    {
      return new IdentityIntro(this);
    }
    else if (0 <= x && x < 24.5 && 2 <= y && y < min(2 - offset + lines.size(), 16))
    {
      int line = int(y) - 2 + offset;
      if (curOperator == buttonIMPE)
      {
        Formula f = lines.get(curline).f;
        Formula f0 = f.get(0);
        Formula f1 = f.get(1);
        if (!f0.equals(lines.get(line).f))
        {
          soundDing.play();
          return this;
        }

        addLine(new int[]{curline, line}, new int[]{}, new int[]{}, f1, curOperator.label);
      }
      else if (curOperator == buttonIMPI)
      {
        addLine(new int[]{line}, new int[]{}, new int[]{curline}, new Formula(opIFTHEN, -1, new Formula[]{lines.get(curline).f, lines.get(line).f}), curOperator.label);
      }
      else if (curOperator == buttonANDI)
      {
        addLine(new int[]{curline, line}, new int[]{}, new int[]{}, new Formula(opAND, -1, new Formula[]{lines.get(curline).f, lines.get(line).f}), curOperator.label);
      }
      else if (curOperator == buttonORE)
      {
        Formula f = lines.get(curline).f;
        Formula sf0 = f.get(0);
        Formula sf1 = f.get(1);
        Formula lf = lines.get(line).f;
        if (lf.equals(sf0.negate()))
        {
          addLine(new int[]{curline, line}, new int[]{}, new int[]{}, sf1, curOperator.label);
        }
        else if (lf.equals(sf1.negate()))
        {
          addLine(new int[]{curline, line}, new int[]{}, new int[]{}, sf0, curOperator.label);
        }
        else
        {
          soundDing.play();
          return this;
        }
      }
      else if (curOperator == buttonEXISTSE)
      {
        Formula f = lines.get(curline).f;
        Formula sf0 = f.get(0);
        Formula sf1 = f.get(1);
        Formula lf = lines.get(line).f;
        if (lf.op != opIFTHEN)
        {
          soundDing.play();
          return this;
        }

        int i = 0;
        while (i != numMarks(opIndConst))
        {
          if (sf1.equals(new Formula(lf.get(0), new Formula(opIndConst, i), sf0)))
            break;

          ++i;
        }

        if (i == numMarks(opIndConst))
        {
          soundDing.play();
          return this;
        }

        List<Integer> c = lf.get(1).allTerminals(opIndConst);
        if (c.contains(i))
        {
          soundDing.play();
          return this;
        }
  
        for (int l: lines.get(line).deps)
        {
          List<Integer> cl = lines.get(l).f.allTerminals(opIndConst);
          if (cl.contains(i))
          {
            soundDing.play();
            return this;
          }
        }

        addLine(new int[]{curline, line}, new int[]{}, new int[]{}, lf.get(1), config.get(opEXISTS) + "B");
      }
      else if (curOperator == buttonIdentityE)
      {
        try
        {
          Formula f = lines.get(curline).f;
          return new IdentityElim(this, lines.get(line).f, f.get(0), f.get(1), new int[]{curline, line});
        }
        catch (Exception e)
        {
          soundDing.play();
          return this;
        }
      }
      else
      {
        setCurline(line);
      }
    }
    else
    {
      setOperator(null);
      setCurline(-1);
    }

    redraw();
    return this;
  }
  
  float lastX = 0, lastY = 0;
  void moved(float x, float y)
  {
    lastX = x;
    lastY = y;
  }

  void drag(float x, float y)
  {
    if (y - lastY > 1)
    {
      offset = max(min(offset - 1, lines.size() - 14), 0);
      lastY = y;
    }
    else if (lastY - y > 1)
    {
      offset = max(min(offset + 1, lines.size() - 14), 0);
      lastY = y;
    }
    redraw();
  }

  void setOperator(Button b)
  {
    if (curOperator != null)
      curOperator.mark = false;

    curOperator = b;
    if (curOperator != null)
      curOperator.mark = true;
  }

  void setCurline(int line)
  {
    curline = line;
    for (Button bb: buttons)
      bb.enabled = canApply(bb, curline == -1 ? null : lines.get(curline));
  }

  State feedback(int x)
  {
    if (x == 1)
      return old;

    return this;
  }

}

class ProblemProof extends Problem
{
  boolean propositional;
  Formula[] premises;
  Formula target;
  int steps;
  String hint, lesson;

  ProblemProof(String id, String[] requires, String label, String[] infos, boolean propositional, String[] premises, String target, int steps, String hint, String lesson)
  {
    super(id, requires, label, infos);
    this.propositional = propositional;
    this.premises = new Formula[premises.length];
    for (int i = 0; i != premises.length; ++i)
      this.premises[i] = new Formula(premises[i]);

    this.target = target != null ? new Formula(target) : null;
    this.steps = steps;
    this.hint = hint;
    this.lesson = lesson;
    if (this.infos.length == 0)
      this.infos = new String[]{"Zu zeigen: '" + formatProof(this.premises, this.target) + "'" + (steps == 0 ? "" : " (≥" + steps + " Zeilen)")};
  }

  State run(State old)
  {
    Proof p = new Proof(old, id, propositional, premises, target, steps, hint, lesson);

    if (!id.equals("proof_assert"))
    {
      for (Formula f: p.premises)
        p.addAssertion(f);

      p.setCurline(-1);
      p.setChanged(false);
    }

    return p;
  }
}

static String[] toArray(JSONArray a)
{
  if (a == null)
    return new String[]{};

  String[] r = new String[a.size()];
  for (int i = 0; i != a.size(); ++i)
    r[i] = a.getString(i);

  return r;
}

Problem[][] makeProblemsProof(boolean propositional)
{
  Map<String, String[]> stringsProof = getStrings("proof");
  JSONArray p = loadJSONArray(propositional ? "problems_proof_propositional.json" : "problems_proof_predicate.json");
  Problem[][] r = new Problem[p.size()+1][];
  for (int i = 0; i != p.size(); ++i)
  {
    JSONArray s = p.getJSONArray(i);
    r[i] = new Problem[s.size()];
    for (int j = 0; j != s.size(); ++j)
    {
      JSONObject o = s.getJSONObject(j);
      String id = "proof_" + o.getString("id");
      String[] depends = toArray(o.getJSONArray("depends"));
      for (int k = 0; k != depends.length; ++k)
        depends[k] = "proof_" + depends[k];

      String[] info = stringsProof.get(o.getString("id"));
      if (info == null)
        info = new String[]{};

      String label = o.getString("label", "" + char('A' + i) + char('1' + j));

      r[i][j] = id.equals("proof_propositional_intro") ? new ProblemInfo(id, prepend("lang_propositional_formula_bracket_operators", depends), label, info) :
        id.equals("proof_predicate_intro") ? new ProblemInfo(id, prepend("lang_predicate_formula_operators", depends), label, info) :
        new ProblemProof(id, depends, label, info, propositional,
          toArray(o.getJSONArray("premises")), o.getString("target"), o.getInt("steps", 0), o.getString("hint"), o.getString("lesson", ""));
    }
  }
  r[p.size()] = new Problem[]{new ProblemProof("", new String[]{propositional ? "proof_propositional_intro" : "proof_predicate_intro"}, "W", new String[]{"beliebigen Beweis durchführen"}, propositional, new String[]{}, null, 0, null, "")};
  return r;
}
