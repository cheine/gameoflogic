/*
 * A Game of Logic (not to be confused with the book "The Game of Logic" by Lewis Caroll)
 * Copyright (C) 2018-2019  Christian Heine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//char derivesSign = '\u22A2';
//char derivedSign = '\u22A3';
//char followsSign = '\u22A8';
//char nderivesSign = '\u22AC';
//char nfollowsSign = '\u22AD';
//char thereforeSign = '\u2234';
//char becauseSign = '\u2235';
//char possibleSign = '\u25C7'; /* '\u25CA' '\u22C4' */
//char necessarySign = '\u25FB'; /* '\u25A1' '\u25FD' */
//char markInterpretationScript = '\u2110';
//char markInterpretation = '\u2111';
//char markLanguage = '\u2112';
//char followsBidirSign = '\u22DA';
//char derivesBidirSign = '\u22DB';

static final char markCheck = '\u2713'; /* heavy: '\u2714' */ 
static final char markBallow = '\u2717'; /* heavy: '\u2718' */

static final String superscriptDigit = "\u2070\u00B9\u00B2\u00B3\u2074\u2075\u2076\u2077\u2078\u2079";
static final String superscriptLatin = "\u1D2C\u1D2E" + "C" + "\u1D30\u1D31" + "F" + "\u1D33\u1D34\u1D35\u1D36\u1D37" +
  "\u1D38\u1D39\u1D3A\u1D3C\u1D3E" + "Q" + "\u1D3F" + "S" + "\u1D40\u1D41" + "V" + "\u1D42" + "X" + "Y" + "Z";

// small exists \u2C7B
//char smallcaps[] /* missing: Q and X */ = {"A\u1D00 B\u0299 C\u1D04 D\u1D05 E\u1D07 F\uA730 G\u0262 H\u029C I\u026A J\u1D0A K\u1D0B L\u029F M\u1D0D N\u0274 O\u1D0F P\u1D18 R\u0280 S\uA731 T\u1D1B U\u1D1C V\u1D20 W\u1D21 Y\u028f Z\1D22"

// lightning sign 21AF
// chess figures & card signs 2654ff, die faces 2680
// greek letters 0391--03A9, 03B1--03C9

static Map<Operator, String[]> markConfigKeys;
static Map<Operator, String[]> makeMarkConfigKeys()
{
  Map<Operator, String[]> r = new HashMap<Operator, String[]>();
  r.put(opTrue,   new String[]{"markTrue",   "w"});
  r.put(opFalse,  new String[]{"markFalse",  "f"});
  r.put(opNOT,    new String[]{"markNOT",    "\u00AC"});
  r.put(opAND,    new String[]{"markAND",    "\u2227"});
  r.put(opOR,     new String[]{"markOR",     "\u2228"});
  r.put(opIFTHEN, new String[]{"markIFTHEN", "\u2192"});
  r.put(opIF,     new String[]{"markIF",     "\u2190"});
  r.put(opIFF,    new String[]{"markIFF",    "\u2194"});
  r.put(opXOR,    new String[]{"markXOR",    "\u21AE"});
  r.put(opNAND,   new String[]{"markNAND",   "\u2191"});
  r.put(opNOR,    new String[]{"markNOR",    "\u2193"});
  r.put(opFORALL, new String[]{"markFORALL", "\u2200"});
  r.put(opEXISTS, new String[]{"markEXISTS", "\u2203"});
  return r;
}

class Config
{
  private final String configPath = ".gameoflogic";
  private JSONObject config;
  private Map<Operator, String> marks;
  private boolean changed = false;
  private Set<String> solved = new HashSet<String>();

  private Map<Operator, String> lookupUse = new HashMap<Operator, String>();

  Config()
  {
    try
    {
      config = loadJSONObject(configPath);
    }
    catch (Exception e)
    {
      config = new JSONObject();
    }

    lookupUse.put(opNAND, "useNAND");
    lookupUse.put(opNOR,  "useNOR");
    lookupUse.put(opIF,   "useIF");
    lookupUse.put(opXOR,  "useXOR");
    lookupUse.put(opSentence, "useSmallSentenceLetters");

    marks = new HashMap<Operator, String>();
    marks.put(opIOTA,      "\u03B9");
    marks.put(opSentence,  getUse(opSentence) ? "pqrst" : "ABCDE");
    marks.put(opIndConst,  "abcde");
    marks.put(opIndVar,    "xyz");
    marks.put(opPredicate, "FGH");
    marks.put(opRelation,  "RST");
    marks.put(opIdentity,  "=");
    marks.put(opIndConstM, "\u03B1\u03B2");
    marks.put(opIndVarM,   "\u03BD");
    marks.put(opPredicateM,"\u03B8");
    marks.put(opRelationM, "\u03A0");
    marks.put(opSentenceM, "\u03C6\u03C8");

    markConfigKeys = makeMarkConfigKeys();
    loadMarks();
    if (config.isNull("solved"))
      config.setJSONArray("solved", new JSONArray());

    JSONArray solved = config.getJSONArray("solved");
    for (int i = 0; i != solved.size(); ++i)
      this.solved.add(solved.getString(i));
  }

  String get(String k, String def)
  {
    return config.getString(k, def);
  }

  void set(String k, String v)
  {
    if (config.isNull(k) || !config.getString(k).equals(v))
    {
      config.setString(k, v);
      changed = true;
    }
  }

  String get(Operator op)
  {
    return marks.get(op);
  }

  boolean getUse(Operator op)
  {
    return config.getBoolean(lookupUse.get(op), false);
  }

  void setUse(Operator op, boolean v)
  {
    String k = lookupUse.get(op);
    if (config.isNull(k) || config.getBoolean(k) != v)
    {
      config.setBoolean(k, v);
      changed = true;
      if (op == opSentence)
        marks.put(opSentence,  v ? "pqrst" : "ABCDE");
    }
  }

  boolean isSolved(String id)
  {
    return solved.contains(id);
  }

  void setSolved(String id)
  {
    if (isSolved(id))
      return;

    solved.add(id);
    JSONArray s = config.getJSONArray("solved");
    s.setString(s.size(), id);
    saveJSONObject(config, configPath);
  }

  void loadMarks()
  {
    for (Map.Entry<Operator, String[]> e: markConfigKeys.entrySet())
      marks.put(e.getKey(), get(e.getValue()[0], e.getValue()[1]));
  }
  
  void saveMarks()
  {
    for (Map.Entry<Operator, String[]> e: markConfigKeys.entrySet())
      set(e.getValue()[0], get(e.getKey()));
  }

  void save()
  {
    if (!changed)
      return;

    saveJSONObject(config, configPath);
    changed = false;
  }
}

int numMarks(Operator op)
{
  return config.get(op).length();
}

class ConfigEdit extends State
{

  String makeLabel(char[] values)
  {
    String s = "" + values[0];
    for (int i = 1; i != values.length; ++i)
      s += "/" + values[i];

    return s;
  }

  class MarkButton extends Button
  {
    Operator[] ops;
    char[] values;
    MarkButton(Operator[] ops, char[] values, float x, float y)
    {
      super(makeLabel(values), x, y, 2, 2);
      this.ops = ops;
      this.values = values;
    }
  }

  State old;
  Button buttonBig = new Button("A,B,C,D,E", 6, 15.0, 4.5, 2);
  Button buttonSmall = new Button("p,q,r,s,t", 11, 15.0, 4.5, 2);
  Button buttonSave = new Button(translations.getString("buttonSave"), 17.5, 15, 7.0, 2);
  Button buttonDone = new Button(translations.getString("buttonAbort"), 25.0, 15, 7.0, 2);
  boolean useNAND;
  boolean useNOR;
  boolean useIF;
  boolean useXOR;
  boolean useSmall;

  void addButtons(Operator[] ops, char[][] marks, float x, float y)
  {
    for (int i = 0; i != marks.length; ++i)
      buttons.add(new MarkButton(ops, marks[i], x + 0.5 + (i - marks.length) * 2.5,  y));
  }

  ConfigEdit(State old)
  {
    this.old = old;

    addButtons(new Operator[]{opTrue, opFalse}, new char[][]{{'w','f'},{'\u22A4','\u22A5'},{'1','0'},{'L','0'}}, 15.5, 0);
    addButtons(new Operator[]{opFORALL, opEXISTS}, new char[][]{{'\u2200','\u2203'},{'\u0245','V'},{'\u22C0','\u22C1'}}, 32, 0);
    addButtons(new Operator[]{opAND},    new char[][]{{'.'}, {'\u1D27'}, {'\u2227'}, {'&'}}, 15.5,  2.5);
    addButtons(new Operator[]{opOR},     new char[][]{{'\u1D20'}, {'\u2228'}, {'|'}}, 15.5,  5.0);
    addButtons(new Operator[]{opIFTHEN}, new char[][]{{'\u2192'}, {'\u21D2'}, {'\u2283'}}, 15.5,  7.5);
    addButtons(new Operator[]{opIFF},    new char[][]{{'\u2194'}, {'\u21D4'}, {'\u2261'}}, 15.5, 10.0);
    addButtons(new Operator[]{opNOT},    new char[][]{{'\u00AC'}, {'-'}, {'~'}, {'!'}}, 15.5, 12.5);
    addButtons(new Operator[]{opNAND},   new char[][]{{'|'}, {'\u2191'}, {'\u22BC'}}, 32,  2.5);
    addButtons(new Operator[]{opNOR},    new char[][]{{'\u2193'}, {'\u22BD'}}, 32,  5.0);
    addButtons(new Operator[]{opIF},     new char[][]{{'\u2190'}, {'\u21D0'}, {'\u2282'}}, 32,  7.5);
    addButtons(new Operator[]{opXOR},    new char[][]{{'\u21AE'}, {'\u21CE'}, {'\u2295'}, {'\u22BB'}}, 32, 10.0);

    buttons.add(buttonBig);
    buttons.add(buttonSmall);
    buttons.add(buttonSave);
    buttons.add(buttonDone);

    useNAND = config.getUse(opNAND);
    useNOR  = config.getUse(opNOR);
    useIF   = config.getUse(opIF);
    useXOR  = config.getUse(opXOR);
    useSmall = config.getUse(opSentence);

    updateButtons();
  }
  
  void updateButtons()
  {
    buttonBig.mark = !useSmall;
    buttonSmall.mark = useSmall;

    for (Button b: buttons)
    {
      if (!(b instanceof MarkButton))
        continue;

      MarkButton m = (MarkButton)b;
      m.mark = true;
      for (int i = 0; i != m.ops.length; ++i)
      {
        if (config.marks.get(m.ops[i]).charAt(0) != m.values[i])
          m.mark = false;
      }
    }
  }

  void paint()
  {
    textFont(nFont);
    textAlign(LEFT);
    fill(BLACK);
    myText(translations.getString("configTruthValues"), 0.0,  0.5);
    myText(translations.getString("configAND"),         0.0,  3.0);
    myText(translations.getString("configOR"),          0.0,  5.5);
    myText(translations.getString("configCOND"),        0.0,  8.0);
    myText(translations.getString("configBICOND"),      0.0, 10.5);
    myText(translations.getString("configNOT"),         0.0, 13.0);
    myText(translations.getString("configQuant"),      17.5,  0.5);
    myText(translations.getString("configNAND"),       17.5,  3.0);
    myText(translations.getString("configNOR"),        17.5,  5.5);
    myText(translations.getString("configREPL"),       17.5,  8.0);
    myText(translations.getString("configXOR"),        17.5, 10.5);
    myText(translations.getString("configSentenceLetters"), 0.0, 15.5);
    textAlign(CENTER);
    myText(useNAND ? "\u2611" : "\u2610", 17,  3.0);
    myText(useNOR  ? "\u2611" : "\u2610", 17,  5.5);
    myText(useIF   ? "\u2611" : "\u2610", 17,  8.0);
    myText(useXOR  ? "\u2611" : "\u2610", 17, 10.5);

    super.paint();
  }

  State click(float x, float y)
  {
    Button b = find(x, y);

    if (b == buttonBig)
    {
      useSmall = false;
      updateButtons();
    }

    if (b == buttonSmall)
    {
      useSmall = true;
      updateButtons();
    }

    if (b == buttonDone)
    {
      config.loadMarks();
      updateButtons();
      return old;
    }

    if (b == buttonSave)
    {
      config.setUse(opNAND, useNAND);
      config.setUse(opNOR,  useNOR);
      config.setUse(opIF,   useIF);
      config.setUse(opXOR,  useXOR);
      config.setUse(opSentence, useSmall);
      config.saveMarks();
      config.save();
      return old;
    }

    if (b != null && b instanceof MarkButton)
    {
      MarkButton m = (MarkButton)b;
      for (int i = 0; i != m.ops.length; ++i)
        config.marks.put(m.ops[i], "" + m.values[i]);

      updateButtons();
    }

    if (16.5 <= x && x < 18 &&  2.5 <= y && y <  4.5)
      useNAND = !useNAND;
    if (16.5 <= x && x < 18 &&  5.0 <= y && y <  7.0)
      useNOR  = !useNOR;
    if (16.5 <= x && x < 18 &&  7.5 <= y && y <  9.5)
      useIF   = !useIF;
    if (16.5 <= x && x < 18 && 10.0 <= y && y < 12.0)
      useXOR  = !useXOR;

    redraw();
    return this;
  }
}
