/*
 * A Game of Logic (not to be confused with the book "The Game of Logic" by Lewis Caroll)
 * Copyright (C) 2018-2019  Christian Heine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

int[] randomPermutation(int n)
{
  int[] p = new int[n];
  for (int i = 0; i != n; ++i)
  {
    int j = int(random(i + 1));
    p[i] = p[j];
    p[j] = i;
  }

  return p;
}

static class MultipleChoiceTest
{
  String question;
  String[] correct;
  String[] incorrect;
  MultipleChoiceTest(String question, String[] correct, String[] incorrect)
  {
    this.question = question;
    this.correct = correct;
    this.incorrect = incorrect;
  }
  boolean okay(String x)
  {
    for (int i = 0; i != correct.length; ++i)
      if (x.equals(correct[i]))
        return true;

    return false;
  }
}

class Progress
{
  String id;
  boolean[] done;
  int cur;
  boolean checking;

  Progress(String id, int n)
  {
    this.id = id;
    done = new boolean[n];
    for (int i = 0; i != n; ++i)
      done[i] = false;

    cur = 0;
  }

  int numCorrect()
  {
    int n = 0;
    for (boolean b: done)
      if (b)
        n++;

    return n;
  }

  void set(boolean correct)
  {
    done[cur] = correct;
    if (!correct)
    {
      soundFail.play();
    }
    else
    {
      if (numCorrect() == done.length)
      {
        soundWin.play();
        if (!id.equals(""))
          config.setSolved(id);
      }
      else
      {
        soundAchieve.play();
      }
    }

    checking = true;
  }

  void next()
  {
    int i = cur;
    do
    {
      i = (i+1) % done.length;
    } while (i != cur && done[i]);
    cur = i;

    checking = false;
  }

}

class MultipleChoice extends State
{

  State old;
  MultipleChoiceTest[] tests;
  Progress p;
  Button doneButton = new Button("Men\u00FC", 27.5, 0, 4.5, 2);
  Button nextButton = new Button("Weiter", 27.5, 2.5, 4.5, 2);
  boolean multiple = false;

  MultipleChoice(State old, String id, MultipleChoiceTest[] tests)
  {
    this.old = old;
    this.tests = tests;
    p = new Progress(id, tests.length);

    for (MultipleChoiceTest t: tests)
      multiple |= t.correct.length > 1;

    buttons.add(doneButton);
    buttons.add(nextButton);

    setCur(0);
  }

  void setCur(int cur)
  {
    while (buttons.size() > 2)
      buttons.remove(buttons.size() - 1);

    String[] options = new String[tests[cur].correct.length + tests[cur].incorrect.length];
    for (int i = 0; i != tests[cur].correct.length; ++i)
      options[i] = tests[cur].correct[i];

    for (int i = 0; i != tests[cur].incorrect.length; ++i)
      options[tests[cur].correct.length + i] = tests[cur].incorrect[i];

    int[] p = randomPermutation(options.length);
    for (int i = 0; i != options.length; ++i)
      buttons.add(new Button(options[i], 0, 15 - 2.5 * p[i], 27, 2));
  }

  void paint()
  {
    fill(LTGRAY);
    rect(0, 0, 27, 0.5);
    fill(BLUE);
    rect(0, 0, (27.0 * p.numCorrect()) / p.done.length, 0.5);

    fill(BLACK);
    textFont(nFont);
    textAlign(LEFT);
    textWriter(tests[p.cur].question, 27, 1);

    if (p.checking)
    {
      for (int i = 2; i != buttons.size(); ++i)
        myText(buttons.get(i).mark == tests[p.cur].okay(buttons.get(i).label) ? "" + markCheck : "" + markBallow,
          buttons.get(i).x + buttons.get(i).w + 0.5, buttons.get(i).y + 0.5);
    }

    textAlign(CENTER);
    if (multiple)
      myText("(mehrere Antworten k\u00F6nnen richtig sein)", 13.5, 4);

    super.paint();
  }

  State click(float x, float y)
  {
    Button b = find(x, y);
    if (b == doneButton)
      return old;

    if (b == nextButton && !p.checking)
    {
      boolean correct = true;
      for (int i = 2; i != buttons.size(); ++i)
      {
        boolean check = buttons.get(i).mark == tests[p.cur].okay(buttons.get(i).label);
        correct &= check;
      }

      p.set(correct);
    }
    else if (b == nextButton && p.checking)
    {
      p.next();
      if (p.done[p.cur])
        return old;
      else
        setCur(p.cur);
    }
    else if (b != null && !p.checking)
    {
      if (multiple)
      {
        b.mark = !b.mark;
      }
      else
      {
        for (int i = 2; i != buttons.size(); ++i)
          buttons.get(i).mark = false;

        b.mark = true;
      }
    }

    redraw();
    return this;
  }

}

static List<String> toList(String[] a)
{
  List<String> l = new ArrayList<String>();
  for (String s: a)
    l.add(s);

  return l;
}

static String[] toArray(List<String> l)
{
  String[] a = new String[l.size()];
  int n = 0;
  for (String s: l)
    a[n++] = s;

  return a;
}

MultipleChoiceTest makeTest(String question, String[] correct, String[] maybeCorrect, String[] incorrect, String[] maybeIncorrect, List<String> confusers)
{
  List<String> nc = toList(correct);
  List<String> ni = toList(incorrect);

  for (String s: maybeCorrect)
    if (random(2) < 1)
      nc.add(s);

  for (String s: maybeIncorrect)
    if (random(2) < 1)
      ni.add(s);

  while (nc.size() + ni.size() < 5)
  {
    String x = confusers.get(int(random(confusers.size())));
    boolean found = false;
    for (String s: correct)
      found |= s.equals(x);

    for (String s: maybeCorrect)
      found |= s.equals(x);

    for (String s: ni)
      found |= s.equals(x);

    if (!found)
      ni.add(x);
  }

  return new MultipleChoiceTest(question, toArray(nc), toArray(ni)); 
}

class ProblemLanguageMarks extends Problem
{
  boolean propositional;

  ProblemLanguageMarks(String id, String[] depends, String label, boolean propositional, String[] infos)
  {
    super(id, depends, label, infos);
    this.propositional = propositional;
  }

  List<String[]> listLetters(Operator op, String type)
  {
    List<String[]> r = new ArrayList<String[]>();
    for (int i = 0; i != numMarks(op); ++i)
      r.add(new String[]{formatMark(op, i), type});

    return r;
  }

  MultipleChoiceTest makeAugmentedTest(String question, boolean logical, String[] correct, String[] incorrect, List<String> confusers)
  {
    List<String> nc = new ArrayList<String>();
    List<String> ni = new ArrayList<String>();
    (logical ? nc : ni).add("logisches Zeichen");
    return makeTest(question, correct, toArray(nc), incorrect, toArray(ni), confusers);
  }

  State run(State old)
  {
    List<String[]> lsigns = new ArrayList<String[]>();
    lsigns.add(new String[]{formatMark(opNOT,    0), "Negationszeichen"});
    lsigns.add(new String[]{formatMark(opAND,    0), "Konjunktionszeichen"});
    lsigns.add(new String[]{formatMark(opOR,     0), "Disjunktionszeichen"});
    lsigns.add(new String[]{formatMark(opIFTHEN, 0), "Implikationszeichen"});
    lsigns.add(new String[]{formatMark(opIFF,    0), "Bikonditionalzeichen"});
    /*
    if (config.getUse(opXOR )) lsigns.add(new String[]{formatMark(opXOR,  0), "Antivalenzzeichen"});
    if (config.getUse(opIF  )) lsigns.add(new String[]{formatMark(opIF,   0), "Reflektionszeichen"});
    if (config.getUse(opNAND)) lsigns.add(new String[]{formatMark(opNAND, 0), "Schefferscher Strich"});
    if (config.getUse(opNOR )) lsigns.add(new String[]{formatMark(opNOR,  0), "Peircescher Pfeil"});
    */
    List<String[]> qsigns = new ArrayList<String[]>();
    qsigns.add(new String[]{formatMark(opFORALL, 0), "Allquantor"});
    qsigns.add(new String[]{formatMark(opEXISTS, 0), "Existenzquantor"});
    qsigns.add(new String[]{formatMark(opIdentity, 0), "Identit\u00E4tszeichen"});

    List<String> confusers = new ArrayList<String>();
    for (String s: new String[]{"Satzzeichen", "Satzbuchstabe", "Hilfszeichen", "Individuenkonstante", "Individuenvariable", "Relation",
      "kein Zeichen", "Triadisches Zeichen", "Happelsche Funktion", "Tautologiezeichen", "Kontradiktionszeichen", "Widderzeichen", "Kontraktionszeichen"})
      confusers.add(s);

    for (String[] s: lsigns)
      confusers.add(s[1]);

    for (String[] s: qsigns)
      confusers.add(s[1]);

    String conf = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+*/%$§;,?<>";
    List<String> confusingSigns = new ArrayList<String>();
    for (int i = 0; i != conf.length(); ++i)
    {
      Operator[] ops = {opSentence, opIndConst, opIndVar, opPredicate, opRelation};
      boolean found = false;
      for (Operator op: ops)
        for (int j = 0; j != numMarks(op); ++j)
          found |= formatMark(op, j).equals("" + conf.charAt(j));

      if (!found)
        confusingSigns.add("" + conf.charAt(i));
    }

    List<MultipleChoiceTest> t = new ArrayList<MultipleChoiceTest>();
    if (propositional)
    {
      int[] p = randomPermutation(numMarks(opSentence));
      for (int i = 0; i != min(2, p.length); ++i)
        t.add(makeAugmentedTest("Was ist das Zeichen '" + formatMark(opSentence, p[i]) + "' innerhalb der Sprache \u2112\u1D00\u029F?",
          false, new String[]{"Satzbuchstabe"}, new String[]{}, confusers));

      for (String[] s: lsigns)
        t.add(makeAugmentedTest("Was ist das Zeichen '" + s[0] + "' innerhalb der Sprache \u2112\u1D00\u029F?",
          true, new String[]{s[1]}, new String[]{}, confusers));

      t.add(makeAugmentedTest("Was ist das Zeichen '" + (random(2) < 1 ? "(" : ")") + "' innerhalb der Sprache \u2112\u1D00\u029F?",
        false, new String[]{"Hilfszeichen"}, new String[]{}, confusers));

      t.add(makeTest("Was ist das Zeichen '" + formatMark(opIndConst, int(random(numMarks(opIndConst)))) + "' innerhalb der Sprache \u2112\u1D00\u029F?",
        new String[]{"kein Zeichen"}, new String[]{}, new String[]{}, new String[]{}, confusers));
      t.add(makeTest("Was ist das Zeichen '" + formatMark(opIndVar, int(random(numMarks(opIndVar)))) + "' innerhalb der Sprache \u2112\u1D00\u029F?",
        new String[]{"kein Zeichen"}, new String[]{}, new String[]{}, new String[]{}, confusers));
      t.add(makeTest("Was ist das Zeichen '" + formatMark(opPredicate, int(random(numMarks(opPredicate)))) + "' innerhalb der Sprache \u2112\u1D00\u029F?",
        new String[]{"kein Zeichen"}, new String[]{}, new String[]{}, new String[]{}, confusers));
      t.add(makeTest("Was ist das Zeichen '" + formatMark(opRelation, int(random(numMarks(opRelation)))) + "' innerhalb der Sprache \u2112\u1D00\u029F?",
        new String[]{"kein Zeichen"}, new String[]{}, new String[]{}, new String[]{}, confusers));
      t.add(makeTest("Was ist das Zeichen '" + qsigns.get(int(random(qsigns.size())))[0] + "' innerhalb der Sprache \u2112\u1D00\u029F?",
        new String[]{"kein Zeichen"}, new String[]{}, new String[]{}, new String[]{}, confusers));
    }
    else
    {
      t.add(makeTest("Was ist das Zeichen '" + formatMark(opSentence, int(random(numMarks(opSentence)))) + "' innerhalb der Sprache \u2112\u1D18\u029F\u2081?",
        new String[]{"kein Zeichen"}, new String[]{}, new String[]{"Satzbuchstabe"}, new String[]{}, confusers));

      int[] p = randomPermutation(numMarks(opIndConst));
      for (int i = 0; i != min(2, p.length); ++i)
        t.add(makeAugmentedTest("Was ist das Zeichen '" + formatMark(opIndConst, p[i]) + "' innerhalb der Sprache \u2112\u1D18\u029F\u2081?",
          false, new String[]{"Individuenkonstante"}, new String[]{"Individuenvariable"}, confusers));

      t.add(makeAugmentedTest("Was ist das Zeichen '" + formatMark(opIndVar, int(random(numMarks(opIndVar)))) + "' innerhalb der Sprache \u2112\u1D18\u029F\u2081?",
        false, new String[]{"Individuenvariable"}, new String[]{"Individuenkonstante"}, confusers));
      t.add(makeAugmentedTest("Was ist das Zeichen '" + formatMark(opPredicate, int(random(numMarks(opPredicate)))) + "' innerhalb der Sprache \u2112\u1D18\u029F\u2081?",
        false, new String[]{"Pr\u00E4dikat"}, new String[]{"Relation"}, confusers));
      t.add(makeAugmentedTest("Was ist das Zeichen '" + formatMark(opRelation, int(random(numMarks(opRelation)))) + "' innerhalb der Sprache \u2112\u1D18\u029F\u2081?",
        false, new String[]{"Relation"}, new String[]{"Pr\u00E4dikat"}, confusers));

      p = randomPermutation(lsigns.size());
      for (int i = 0; i != min(2, p.length); ++i)
        t.add(makeTest("Was ist das Zeichen '" + lsigns.get(p[i])[0] + "' innerhalb der Sprache \u2112\u1D18\u029F\u2081?",
          new String[]{lsigns.get(p[i])[1]}, new String[]{}, new String[]{}, new String[]{}, confusers));

      for (String[] s: qsigns)
        t.add(makeAugmentedTest("Was ist das Zeichen '" + s[0] + "' innerhalb der Sprache \u2112\u1D18\u029F\u2081?",
          true, new String[]{s[1]}, new String[]{}, confusers));
    }

    int[] p = randomPermutation(t.size());
    MultipleChoiceTest[] tests = new MultipleChoiceTest[t.size()];
    for (int i = 0; i != t.size(); ++i)
      tests[i] = t.get(p[i]);

    return new MultipleChoice(old, id, tests);
  }
}

class ProblemWFF extends Problem
{
  boolean propositional;
  ProblemWFF(String id, String[] depends, String label, String[] infos, boolean propositional)
  {
    super(id, depends, label, infos);
    this.propositional = propositional;
  }

  State run(State old)
  {
    JSONObject o = loadJSONObject("problems_wff.json");
    JSONArray incorrect = o.getJSONArray(propositional ? "propositional" : "predicate");
    List<MultipleChoiceTest> tests = new ArrayList<MultipleChoiceTest>();
    int[] p = randomPermutation(incorrect.size());
    RandomFormula r = new RandomFormula(5, propositional, false);

    int n = p.length;
    while (n > 0)
    {
      int x = min(n, int(1 + random(1.5) + random(1.5)));
      String[] cor = new String[5 - x], inc = new String[x];
      int m = 0;
      while (m < 5 - x)
      {
        String s = formatFormula(r.generate(int(random(1.5) + random(1.5))), true);
        boolean found = false;
        for (int j = 0; j != m; ++j)
          found |= cor[j].equals(s);

        if (!found)
          cor[m++] = s;
      }

      for (int j = 0; j != x; ++j)
        inc[j] = filterFormula("$" + incorrect.getString(p[--n]) + "$");

      tests.add(new MultipleChoiceTest("Welche Formeln sind wohlgeformt?", cor, inc));
    }

    MultipleChoiceTest[] t = new MultipleChoiceTest[tests.size()];
    for (int i = 0; i != t.length; ++i)
      t[i] = tests.get(i);

    return new MultipleChoice(old, id, t);
  }
}

class ProblemFormulaType extends Problem
{
  boolean propositional;
  boolean brackets;
  ProblemFormulaType(String id, String[] depends, String label, String[] infos, boolean propositional, boolean brackets)
  {
    super(id, depends, label, infos);
    this.propositional = propositional;
    this.brackets = brackets;
  }
  State run(State old)
  {
    RandomFormula r = new RandomFormula(5, propositional, false);
    if (propositional)
    {
      r.probNOT = 40;
      r.probAND = 15;
      r.probOR = 15;
      r.probIFTHEN = 15;
      r.probIFF = 15;
    }
    else
    {
      r.probNOT = 40;
      r.probAND = 10;
      r.probOR = 10;
      r.probIFTHEN = 10;
      r.probIFF = 10;
      r.probFORALL = 10;
      r.probEXISTS = 10;
    }
    List<MultipleChoiceTest> tests = new ArrayList<MultipleChoiceTest>();

    int[] p = randomPermutation(20);
    String[] confusers = propositional ? new String[]{"einfache Formel", "komplexe Formel", "Literal", "Negation", "negierte Negation",
        "Konjunktion", "negierte Konjunktion", "Disjunktion", "negierte Disjunktion", "Implikation", "negierte Implikation", "Bikonditional", "negiertes Bikonditional"} :
      new String[]{"atomare Formel", "molekulare Formel", "Literal", "Negation", "negierte Negation", "Konjunktion", "negierte Konjunktion",
        "Disjunktion", "negierte Disjunktion", "Implikation", "negierte Implikation", "Bikonditional", "negiertes Bikonditional", "Allquantifikation", "Existenzquantifikation", "negierte Allquantifikation", "negierte Existenzquantifikation"};

    for (int i = 0; i != 20; ++i)
    {
      Formula f;
      do
      {
        f = r.generate(brackets ? (p[i] + 2) / 4 : 2 + i / 4);
      } while (!brackets && bracketPrecedence(f));
      String question = "Was ist der Typ der Formel '" + formatFormula(f, brackets) + "'?";

      if (f.isAtomic())
        tests.add(makeTest(question, new String[]{confusers[0]}, new String[]{"Literal"}, new String[]{confusers[1]}, new String[]{}, toList(confusers)));
      else if (f.op == opAND)
        tests.add(makeTest(question, new String[]{"Konjunktion"}, new String[]{confusers[1]}, new String[]{"Disjunktion"}, new String[]{}, toList(confusers)));
      else if (f.op == opOR)
        tests.add(makeTest(question, new String[]{"Disjunktion"}, new String[]{confusers[1]}, new String[]{"Konjunktion"}, new String[]{}, toList(confusers)));
      else if (f.op == opIFTHEN)
        tests.add(makeTest(question, new String[]{"Implikation"}, new String[]{confusers[1]}, new String[]{"Bikonditional"}, new String[]{}, toList(confusers)));
      else if (f.op == opIFF)
        tests.add(makeTest(question, new String[]{"Bikonditional"}, new String[]{confusers[1]}, new String[]{"Implikation"}, new String[]{}, toList(confusers)));
      else if (f.op == opFORALL)
        tests.add(makeTest(question, new String[]{"Allquantifikation"}, new String[]{confusers[1]}, new String[]{"Existenzquantifikation"}, new String[]{confusers[0]}, toList(confusers)));
      else if (f.op == opEXISTS)
        tests.add(makeTest(question, new String[]{"Existenzquantifikation"}, new String[]{confusers[1]}, new String[]{"Allquantifikation"}, new String[]{confusers[0]}, toList(confusers)));
      else if (f.op == opNOT && f.get(0).isAtomic())
        tests.add(makeTest(question, new String[]{"Negation"}, new String[]{"Literal", confusers[1]}, new String[]{}, new String[]{confusers[0]}, toList(confusers)));
      else if (f.op == opNOT && f.get(0).op == opNOT)
        tests.add(makeTest(question, new String[]{"negierte Negation"}, new String[]{"Negation", confusers[1]}, new String[]{}, new String[]{confusers[0]}, toList(confusers)));
      else if (f.op == opNOT && f.get(0).op == opAND)
        tests.add(makeTest(question, new String[]{"negierte Konjunktion"}, new String[]{"Negation", confusers[1]}, new String[]{"negierte Disjunktion"}, new String[]{}, toList(confusers)));
      else if (f.op == opNOT && f.get(0).op == opOR)
        tests.add(makeTest(question, new String[]{"negierte Disjunktion"}, new String[]{"Negation", confusers[1]}, new String[]{"negierte Konjunktion"}, new String[]{}, toList(confusers)));
      else if (f.op == opNOT && f.get(0).op == opIFTHEN)
        tests.add(makeTest(question, new String[]{"negierte Implikation"}, new String[]{"Negation", confusers[1]}, new String[]{}, new String[]{confusers[0]}, toList(confusers)));
      else if (f.op == opNOT && f.get(0).op == opIFF)
        tests.add(makeTest(question, new String[]{"negiertes Bikonditional"}, new String[]{"Negation", confusers[1]}, new String[]{}, new String[]{confusers[0]}, toList(confusers)));
      else if (f.op == opNOT && f.get(0).op == opFORALL)
        tests.add(makeTest(question, new String[]{"negierte Allquantifikation"}, new String[]{"Negation", confusers[1]}, new String[]{}, new String[]{confusers[0]}, toList(confusers)));
      else if (f.op == opNOT && f.get(0).op == opEXISTS)
        tests.add(makeTest(question, new String[]{"negierte Existenzquantifikation"}, new String[]{"Negation", confusers[1]}, new String[]{}, new String[]{confusers[0]}, toList(confusers)));
    }

    MultipleChoiceTest[] t = new MultipleChoiceTest[tests.size()];
     for (int i = 0; i != t.length; ++i)
       t[i] = tests.get(i);

    return new MultipleChoice(old, id, t);
  }
}

class ProblemFormulaOperators extends Problem
{
  boolean propositional;
  boolean brackets;
  ProblemFormulaOperators(String id, String[] depends, String label, String[] infos, boolean propositional, boolean brackets)
  {
    super(id, depends, label, infos);
    this.propositional = propositional;
    this.brackets = brackets;
  }
  State run(State old)
  {
    RandomFormula r = new RandomFormula(5, propositional, false);
    if (propositional)
    {
      r.probNOT = 20;
      r.probAND = 20;
      r.probOR = 20;
      r.probIFTHEN = 20;
      r.probIFF = 20;
    }
    else
    {
      r.probNOT = 15;
      r.probAND = 15;
      r.probOR = 15;
      r.probIFTHEN = 15;
      r.probIFF = 15;
      r.probFORALL = 15;
      r.probEXISTS = 10;
    }
    List<MultipleChoiceTest> tests = new ArrayList<MultipleChoiceTest>();

    Operator[] confuserOps = propositional ? new Operator[]{opNOT, opAND, opOR, opIFTHEN, opIFF} : new Operator[]{opNOT, opAND, opOR, opIFTHEN, opIFF, opFORALL, opEXISTS};
    String[] confusers = new String[confuserOps.length];
    for (int i = 0; i != confusers.length; ++i)
      confusers[i] = formatMark(confuserOps[i], 0);

    for (int i = 0; i != 15; ++i)
    {
      Formula f;
      do
      {
        f = r.generate(2 + i / 3);
      } while (!brackets && !bracketPrecedence(f));
      tests.add(makeTest("Welches Zeichen markiert den Hauptoperator der Formel '" + formatFormula(f, brackets) + "'?", new String[]{formatMark(f.op, -1)}, new String[]{}, new String[]{}, new String[]{}, toList(confusers)));
    }

    MultipleChoiceTest[] t = new MultipleChoiceTest[tests.size()];
     for (int i = 0; i != t.length; ++i)
       t[i] = tests.get(i);

    return new MultipleChoice(old, id, t);
  }
}
