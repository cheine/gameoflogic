/*
 * A Game of Logic (not to be confused with the book "The Game of Logic" by Lewis Caroll)
 * Copyright (C) 2018-2019  Christian Heine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// connector type, front, order, middle, order
static final Map<Operator, String[][]> connectors = initConnectors();
static Map<Operator, String[][]> initConnectors()
{
  Map<Operator, String[][]> r = new HashMap<Operator, String[][]>();
  r.put(opAND, new String[][]{
    {"", "", "und", "", ""},
    {"", "", "aber", "", ""},
    {"", "", "*zugleich", "", ""},
    {"", "", "*zudem", "", ""},
    {"", "", "zugleich", "<", ""},
    {"", "", "obgleich", ">", ""},
    {"", "", "obwohl", ">", ""},
    {"", "", "w\u00E4hrend", ">", ""},
    {"obgleich", ">", "", "<", ""},
    {"obwohl", ">", "", "<", ""},
    {"w\u00E4hrend", ">", "", "<", ""},
  });
  r.put(opOR, new String[][]{
    {"", "", "oder", "", ""},
  });
  r.put(opIFTHEN, new String[][]{
    {"wenn", ">", "dann", "<", ""},
    {"wenn", ">", "", "<", "", ""},
    {"falls", ">", "", "<", "", ""},
    {"*nur dann", "", "wenn", ">", ""},
    {"*nur", "", "wenn", ">", ""},
    {"*nur", "", "falls", ">", ""},
  });
  r.put(opIF, new String[][]{
    {"*dann", "", "wenn", ">", ""},
    {"", "", "wenn", ">", ""},
    {"", "", "falls", ">", ""},
    {"", "", "sofern", ">", ""},
    {"nur wenn", ">", "", "<", ""},
    {"nur falls", ">", "", "<", ""},
  });
  r.put(opIFF, new String[][]{
    {"*genau dann", "", "wenn", ">", ""},
    {"*dann und nur dann", "", "wenn", ">", ""},
    {"genau dann wenn", ">", "", "<", ""},
  });
  r.put(opXOR, new String[][]{
    {"entweder", "", "oder", "", ""},
    {"entweder", "<", "oder", "", ""},
    {"", "", "au\u00DFer", "-", "*"},
    {"", "", "es sei denn", "-", "*"},
  });
  r.put(opNOR, new String[][]{
    {"weder", "<", "noch", "<", ""},
  });
  return r;
}

Formula randomSentenceLiteral()
{
  Formula f = new Formula(opSentence, int(random(numMarks(opSentence))));
  return random(1.5) < 1 ? f : f.negate();
}

Formula randomFormulaForTranslationAssociative(int operators)
{
  if (operators == 0)
    return randomSentenceLiteral();

  Formula f = new Formula(random(2) < 1 ? opAND : opOR, -1, new Formula[]{randomSentenceLiteral(), randomSentenceLiteral()});
  while (--operators > 0)
    f = new Formula(f.op, -1, new Formula[]{f, randomSentenceLiteral()});

  return f;
}

Formula randomFormulaForTranslation(int operators)
{
  if (operators == 0)
    return randomSentenceLiteral();

  float r = random(1);
  if ((r -= 0.15) < 0)
    return randomFormulaForTranslation(operators - 1).negate();

  if ((r -= 0.50) < 0)
    return randomFormulaForTranslationAssociative(operators);

  int n = int(random(operators-1));
  Formula f = new Formula(opIFF, -1, new Formula[]{randomFormulaForTranslationAssociative(n), randomFormulaForTranslationAssociative(operators-1-n)});

  if ((r -= 0.2) < 0)
    f.op = opIFTHEN;

  return f;
}

Formula randVar(List<Formula> canUse)
{
  int r = int(random(canUse.size() + numMarks(opIndConst)));
  return r < canUse.size() ? canUse.get(r) : new Formula(opIndConst, r - canUse.size());
}

Formula randomLiteral(List<Formula> mustUse, List<Formula> canUse)
{
  Formula f;

  if (mustUse.size() == 2 || random(2) < 1)
  {
    f = new Formula(opRelation, int(random(numMarks(opRelation))), new Formula[]{randVar(canUse), randVar(canUse)});

    if (!mustUse.isEmpty())
    {
      int r = int(random(2));
      f.subformulas[r] = mustUse.get(0);
      if (mustUse.size() == 2)
        f.subformulas[1 - r] = mustUse.get(1);
    }

    if (random(3) < 1)
    {
      f.op = opIdentity;
      f.value = -1;
    }
  }
  else
  {
    f = new Formula(opPredicate, int(random(numMarks(opPredicate))), new Formula[]{mustUse.size() == 1 ? mustUse.get(0) : randVar(canUse)});
  }

  return random(4) < 1 ? f.negate() : f;
}

Formula randomChain(int operators, Operator op, List<Formula> mustUse, List<Formula> canUse)
{
  if (operators > 0 && random(4) < 1)
    return randomQuantification(operators - 1, mustUse, canUse);

  Formula f = randomLiteral(mustUse, canUse);
  if (mustUse.size() > 1)
    mustUse.remove(mustUse.get(0));

  for (int i = 0; i != operators; ++i)
    f = new Formula(op, -1, new Formula[]{randomLiteral(mustUse, canUse), f});

  return f;
}

static boolean contains(List<Formula> l, Formula x)
{
  for (Formula f: l)
    if (f.equals(x))
      return true;

  return false;
}

Formula randomQuantification(int operators, List<Formula> mustUse, List<Formula> canUse)
{
  Formula f = new Formula(opIndVar, 0), sf;
  while (contains(canUse, f))
    f.value++;

  List<Formula> nMustUse = new ArrayList<Formula>(mustUse);
  List<Formula> nCanUse = new ArrayList<Formula>(canUse);
  nMustUse.add(f);
  nCanUse.add(f);

  if (operators > 1 && random(3) < 1)
  {
    int n = int(random(operators - 1));
    sf = new Formula(opIFTHEN, -1, new Formula[]{
      randomChain(n, random(3) < 2 ? opAND : opOR, nMustUse, nCanUse),
      randomChain(operators - 1 - n, random(3) < 2 ? opAND : opOR, nMustUse, nCanUse)
    });
  }
  else
  {
    sf = randomChain(operators, random(3) < 2 ? opAND : opOR, nMustUse, nCanUse);
  }
  f = new Formula(random(2) < 1 ? opFORALL : opEXISTS, -1, new Formula[]{f, sf});

  return random(4) < 1 ? f.negate() : f;
}

/*Formula randomFormula(int operators, List<Formula> mustUse, List<Formula> canUse)
{
  if (operators == 0)
    return randomLiteral(mustUse, canUse);

  float r = random(1);
  if ((r -= 0.25) < 0)
    return randomQuantification(operators - 1, mustUse, canUse);

  if ((r -= 0.15) < 0)
    return randomFormula(operators - 1, mustUse, canUse).negate();

  
}*/

int[] randomUnique(int k, int n)
{
  int[] r = new int[k];
  boolean unique;
  for (int i = 0; i != k; ++i)
    do
    {
      r[i] = int(random(n));
      unique = true;
      for (int j = 0; j != i; ++j)
        unique &= r[j] != r[i];
    } while (!unique);

  return r;
}

class TranslationScheme
{
  Map<Integer, Formula> sentences;
  Map<Integer, String> names;
  Map<Integer, String[]> predicates;
  Map<Integer, String[]> relations;

  String evaluate(Formula f)
  {
    String r = evaluate(f, "", '-');
    return r.substring(0,1).toUpperCase() + r.substring(1) + ".";
  }

  private String reorder(String[] s, char order)
  {
    String[] x = {s[1], s[2], trim(s[3] + " " + s[4])};
    if (s[0].startsWith("*"))
      x[2] = trim(s[0].substring(1) + " " + x[2]);

    String t;
    if (order == '<')
      t = trim(x[1] + " " + trim(x[0] + " " + x[2]));
    else if (order == '>')
      t = trim(x[0] + " " + trim(x[2] + " " + x[1]));
    else
      t = trim(x[0] + " " + trim(x[1] + " " + x[2]));
      
    if (!s[0].startsWith("*"))
      t = trim(s[0] + " " + t);

    return t;
  }

  private String evaluateLiteral(Formula f, String clause, char order)
  {
    Formula fpos = f.op != opNOT ? f : f.get(0);
    if (fpos.op == opSentence)
    {
      Formula x = sentences.get(fpos.value);
      return evaluate(f.op != opNOT ? x : x.negate(), clause, order);
    }

    if (fpos.op == opPredicate || fpos.op == opRelation || fpos.op == opIdentity)
    {
      String[] s;
      if (fpos.op == opPredicate)
      {
        Formula sf = fpos.get(0);
        String[] x = predicates.get(fpos.value);
        s = new String[]{clause, sf.op == opIndConst ? names.get(sf.value) : formatFormula(sf, false), x[0], "", x[1]};
      }
      else
      {
        Formula sf0 = fpos.get(0);
        Formula sf1 = fpos.get(1);
        String[] x = fpos.op == opRelation ? relations.get(fpos.value) : new String[]{"ist", "identisch mit"};
        String n0 = sf0.op == opIndConst ? names.get(sf0.value) : formatFormula(sf0, false);
        String n1 = sf1.op == opIndConst ? names.get(sf1.value) : formatFormula(sf1, false);
        if (sf0.equals(sf1))
          n1 = "sich selbst";

        s = new String[]{clause, n0, x[0], x[1].equals("") ? n1 : "", x[1].equals("") ? "" : x[1] + " " + n1};
      }

      if (f.op == opNOT)
        s[4] = trim("nicht " + s[4]);

      return reorder(s, order);
    }

    return null;
  }

  private String evaluate(Formula f, String clause, char order)
  {
    if (f.isLiteral())
      return evaluateLiteral(f, clause, order);

    Formula fpos = f.op != opNOT ? f : f.get(0);
    if (fpos.op == opFORALL)
      return reorder(new String[]{clause, trim("f\u00FCr " + (f.op == opNOT ? "nicht" : "")) + " jede Person " + formatFormula(fpos.get(0), false), "gilt", "", ""}, order) + ", dass " + evaluate(fpos.get(1), "", '>');

    if (fpos.op == opEXISTS)
      return reorder(new String[]{clause, "es", "gibt", "", (f.op == opNOT ? "keine" : "mindestens eine") + " Person " + formatFormula(fpos.get(0), false)}, order) + ", so dass " + evaluate(fpos.get(1), "", '>');

    if (fpos.op == opIOTA)
      return null;

    if (f.op == opNOT)
      return reorder(new String[]{clause, "es", "ist", "", "nicht der Fall"}, order) + ", dass " + evaluate(f.get(0), "", '>');

    String[][] t = connectors.get(f.op);
    String[] x;
    do
    {
      x = t[int(random(t.length))];
    } while (!clause.equals("") && !x[0].equals(""));

    x[0] = trim(clause + " " + x[0]);
    return evaluate(f.get(0), x[0], x[1].equals("") ? order : x[1].charAt(0)) +
      ", " + evaluate(f.get(1), x[2], x[3].equals("") ? order : x[3].charAt(0));
  }

}

static final String[] properNames = {"Adam", "Bettina", "Chris", "Dorothea", "Emil", "Fabrizia", "Gustav", "Hanna", "Ingo", "Jasmin",
  "Klaus", "Lena", "Martin", "Nadine", "Otto", "Paula", "Ronald", "Sabine", "Theresa", "Uwe", "Vera", "Wilbur", "Zora"};
static final String[] adjectives = {"alt", "jung", "h\u00FCbsch", "flei\u00DFig", "schlau", "geizig", "geschw\u00E4tzig"};
static final String[] verbsIntransitive = {"sitzt", "lacht", "l\u00E4uft", "rennt"};
static final String[] verbsTransitive = {"mag", "kennt"};
static final String[][] verbsAdverbial = {{"tanzt", "mit"}, {"spricht", "mit"}, {"l\u00E4stert", "\u00FCber"}, {"geht", "zu"}};

TranslationScheme randomTranslationScheme(int numNames, int numPredicates, int numRelations)
{
  TranslationScheme t = new TranslationScheme();

  t.names = new HashMap<Integer, String>();
  int[] x = randomUnique(numNames, properNames.length);
  for (int i = 0; i != numNames; ++i)
    t.names.put(i, properNames[x[i]]);

  t.predicates = new HashMap<Integer, String[]>();
  x = randomUnique(numPredicates, adjectives.length + verbsIntransitive.length);
  for (int i = 0; i != numPredicates; ++i)
    t.predicates.put(i, x[i] < adjectives.length ? new String[]{"ist", adjectives[x[i]]} : new String[]{verbsIntransitive[x[i] - adjectives.length], ""});

  t.relations = new HashMap<Integer, String[]>();
  x = randomUnique(numRelations, verbsTransitive.length + verbsAdverbial.length);
  for (int i = 0; i != numRelations; ++i)
    t.relations.put(i, x[i] < verbsTransitive.length ? new String[]{verbsTransitive[x[i]], ""} : verbsAdverbial[x[i] - verbsTransitive.length]);

  t.sentences = new HashMap<Integer, Formula>();
  for (int i = 0; i != 2; ++i)
    t.sentences.put(i, new Formula(opPredicate, i, new Formula[]{new Formula(opIndConst, i)}));

  for (int i = 2; i != 5; ++i)
    t.sentences.put(i, new Formula(opRelation, i - 2, new Formula[]{new Formula(opIndConst, i), new Formula(opIndConst, int(random(numNames)))}));

  return t;
}

class SyntaxTaskVariants extends SyntaxTask
{
  SyntaxTaskVariants(String task, boolean brackets, Formula[] solutions)
  {
    super(task, brackets, solutions);
    for (int i = 0; i != solutions.length; ++i)
      solutions[i] = solutions[i].normalizeQuantification();
  }
  boolean check(Formula f)
  {
    return super.check(f.normalizeQuantification());
  }
}

class ProblemTranslation extends Problem
{
  boolean propositional;
  SyntaxTask[] tasks;
  ProblemTranslation(String id, String[] depends, String label, String[] infos, boolean propositional, SyntaxTask[] tasks)
  {
    super(id, depends, label, infos);
    this.propositional = propositional;
    this.tasks = tasks;
  }

  State run(State old)
  {
    return new Syntax(old, id, propositional, false, tasks, false, true);
  }
}

class ProblemTranslationRandom extends Problem
{
  boolean propositional;
  ProblemTranslationRandom(String[] depends, String label, String[] infos, boolean propositional)
  {
    super("", depends, label, infos);
    this.propositional = propositional;
  }
  State run(State old)
  {
    String base = "";
    TranslationScheme t = randomTranslationScheme(5, 3, 3);
    SyntaxTask[] tasks = new SyntaxTask[15];
    if (propositional)
    {
      for (int i = 0; i != 5; ++i)
      {
        Formula f = t.sentences.get(i);
        if (f != null)
          base += (base.equals("") ? "" : " <<br>> ") + formatMark(opSentence, i) +  ": '" + t.evaluate(f) + "'";
      }
      
      base += " <<par>> Baue eine Formel, die folgendem Satz entspricht: <<br>> ";
      
      for (int i = 0; i != 15; ++i)
      {
        Formula f = randomFormulaForTranslation((2 + i) / 5);
        tasks[i] = new SyntaxTaskVariants(base + "'" + t.evaluate(f) + "'", false, new Formula[]{f});
      }
    }
    else
    {
      for (int j = 0; j != 5; ++j)
        base += (j == 0 ? "" : "   ") + formatMark(opIndConst, j) + ": '" + t.names.get(j) + "'";

      if (t.names.size() != 0)
        base += " <<br>> ";

      for (int j = 0; j != 3; ++j)
        base += (j == 0 ? "" : "   ") + formatMark(opPredicate, j) + ": '... " + t.predicates.get(j)[0] + " " + t.predicates.get(j)[1] + "'";

      if (t.predicates.size() != 0)
        base += " <<br>> ";

      for (int j = 0; j != 3; ++j)
        base += (j == 0 ? "" : "   ") + formatMark(opRelation, j) + ": '... " + t.relations.get(j)[0] + " " + t.relations.get(j)[1] + " ...'";

      base += " <<par>> Baue eine Formel, die folgendem Satz entspricht: <<br>> ";

      for (int i = 0; i != 15; ++i)
      {
        Formula f = randomQuantification(i / 5, new ArrayList<Formula>(), new ArrayList<Formula>());
        tasks[i] = new SyntaxTaskVariants(base + "'" + t.evaluate(f) + "'", false, new Formula[]{f});
      }
    }

    return new Syntax(old, id, propositional, false, tasks, false, true);
  }
}

Problem[] makeProblemsTranslation(boolean propositional)
{
  JSONArray a = loadJSONArray(propositional ? "problems_translation_propositional.json" : "problems_translation_predicate.json");
  Problem[] result = new Problem[2+a.size()];
  result[0] = new ProblemInfo(propositional ? "sem_propositional_formalization" : "sem_predicate_formalization",
    propositional ? new String[]{"lang_propositional_syntax"} : new String[]{"lang_predicate_syntax", "sem_propositional_formalization"},
    "Formalisierung", toArray(translations.getJSONArray(propositional ? "sem_propositional_formalization" : "sem_predicate_formalization")));

  for (int i = 0; i != a.size(); ++i)
  {
    JSONObject o = a.getJSONObject(i);
    String id = o.getString("id");
    String base = "";
    JSONArray s;
    if (propositional)
    {
      JSONArray legend = o.getJSONArray("simple");
      for (int j = 0; j != legend.size(); ++j)
        base += (j == 0 ? "" : " <<br>> ") + formatMark(opSentence, j) + ": '" + legend.getString(j) + "'";

      if (legend.size() != 0)
        base += " <<par>> ";

      s = o.getJSONArray("complex");
    }
    else
    {
      JSONArray individuals = o.getJSONArray("individuals");
      for (int j = 0; j != individuals.size(); ++j)
        base += (j == 0 ? "" : "   ") + formatMark(opIndConst, j) + ": '" + individuals.getString(j) + "'";

      if (individuals.size() != 0)
        base += " <<br>> ";

      JSONArray predicates = o.getJSONArray("predicates");
      for (int j = 0; j != predicates.size(); ++j)
        base += (j == 0 ? "" : "   ") + formatMark(opPredicate, j) + ": '" + predicates.getString(j) + "'";

      if (predicates.size() != 0)
        base += " <<br>> ";

      JSONArray relations = o.getJSONArray("relations");
      for (int j = 0; j != relations.size(); ++j)
        base += (j == 0 ? "" : "   ") + formatMark(opRelation, j) + ": '" + relations.getString(j) + "'";

      if (individuals.size() != 0 || predicates.size() != 0 || relations.size() != 0)
        base += " <<par>> ";

      s = o.getJSONArray("sentences");
    }

    base += "Baue eine Formel, die folgendem Satz entspricht: <<br>> ";

    SyntaxTask[] tasks = new SyntaxTask[s.size()];
    for (int k = 0; k != s.size(); ++k)
    {
      JSONArray p = s.getJSONArray(k);
      Formula[] f = new Formula[p.size() - 1];
      for (int j = 1; j != p.size(); ++j)
        f[j-1] = new Formula(p.getString(j));

      tasks[k] = new SyntaxTaskVariants(base + "'" + p.getString(0) + "'", false, f);
    }

    result[1+i] = new ProblemTranslation("sem_trans_" + id, new String[]{propositional ? "sem_propositional_formalization" : "sem_predicate_formalization"}, "\u00DC" + (1+i), new String[]{"\u00DCbersetzung"}, propositional, tasks);
  }

  result[result.length-1] = new ProblemTranslationRandom(new String[]{propositional ? "sem_propositional_formalization" : "sem_predicate_formalization"}, "\u00DCZ", new String[]{"Zuf\u00E4llige \u00DCbersetzung", "Diese Aufgaben sind zuf\u00E4llig generiert. Sie sind zum Teil sehr absurd. Nicht ernstnehmen."}, propositional);

  return result;
}
