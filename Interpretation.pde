/*
 * A Game of Logic (not to be confused with the book "The Game of Logic" by Lewis Caroll)
 * Copyright (C) 2018-2019  Christian Heine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

void subformulas(Formula f, List<Formula> sf)
{
  if (f.op == opSentence)
    return;

  if (f.op != opPredicate && f.op != opRelation && f.op != opIdentity && !f.isQuantification())
    for (Formula g: f.subformulas)
      subformulas(g, sf);

  sf.add(f);
}

HashMap<Operator, String> guides;
HashMap<Operator, String> initGuides()
{
  HashMap<Operator, String> r = new HashMap<Operator, String>();
  r.put(opSentence, "Ein Formel kann wahr (" + config.get(opTrue) + ") oder falsch (" + config.get(opFalse) + ") sein.");
  r.put(opNOT, "Eine Formel '$-S0$' ist wahr genau dann, wenn '$S0$' falsch ist.");
  r.put(opAND, "Eine Formel '$S0&S1$' ist wahr genau dann, wenn sowohl '$S0$' als auch '$S1$' wahr sind.");
  r.put(opOR, "Eine Formel '$S0vS1$' ist wahr genau dann, wenn '$S0$' oder '$S1$' wahr ist.");
  r.put(opIFTHEN, "Eine Formel '$S0>S1$' ist wahr genau dann, wenn '$S0$' falsch oder '$S1$' wahr ist.");
  r.put(opIF, "Eine Formel '$S0<S1$' ist wahr genau dann, wenn '$S0$' wahr oder '$S1$' falsch ist.");
  r.put(opIFF, "Eine Formel '$S0#S1$' ist wahr genau dann, wenn '$S0$' den selben Wahrheitswert hat wie '$S1$'.");
  r.put(opXOR, "Eine Formel '$S0+S1$' ist wahr genau dann, wenn entweder '$S0$' oder '$S1$' wahr ist.");
  r.put(opNAND, "Eine Formel '$S0^S1$' ist wahr genau dann, wenn nicht sowohl '$S0$' als auch '$S1$' wahr sind.");
  r.put(opNOR, "Eine Formel '$S0_S1$' ist wahr genau dann, wenn sowohl weder '$S0$' noch '$S1$' wahr sind.");
  r.put(opPredicate, "Eine Formel '$B0b0$' ist wahr genau dann, wenn die Extension von '$b0$' in der Extension von '$B0$' vorkommt.");
  r.put(opRelation, "Eine Formel '$C0b0b1$' ist wahr genau dann, wenn die Extension von '$b0$' und '$b1$' als Paar in der Extension von '$C0$' vorkommen.");
  r.put(opIdentity, "Eine Formel '$b0=b1$' ist wahr genau dann, wenn '$b0$' und '$b1$' die gleiche Extension haben.");
  r.put(opFORALL, "Eine Formel '$Ac0S0(c0)$' ist wahr genau dann, wenn '$S0(b0)$' f\u00FCr jede Interpretation, die sich nur in der Extension von '$b0$' unterscheidet, wahr ist."); 
  r.put(opEXISTS, "Eine Formel '$Ec0S0(c0)$' ist wahr genau dann, wenn '$S0(b0)$' f\u00FCr eine Interpretation, die sich nur in der Extension von '$b0$' unterscheidet, wahr ist.");
  return r;
}

class IntPair implements Comparable<IntPair>
{
  int first;
  int second;
  IntPair(int first, int second)
  {
    this.first = first;
    this.second = second;
  }

  int compareTo(IntPair o)
  {
    int x = first - o.first;
    return x == 0 ? second - o.second : x;
  }
}

class Interpretation
{
  Set<Integer> truths;
  String[] universe;
  Map<Integer, Integer> individuals;
  Map<Integer, Set<Integer>> predicates;
  Map<Integer, Set<IntPair>> relations;

  Interpretation()
  {
  }

  private int value(Formula f, Map<Integer, Integer> variables)
  {
    if (f.op == opIndConst && individuals.get(f.value) != null)
      return individuals.get(f.value);

    if (f.op == opIndVar && variables.get(f.value) != null)
      return variables.get(f.value);

    throw new RuntimeException("individual not found");
  }

  boolean evaluate(Formula f)
  {
    return evaluate(f, new HashMap<Integer, Integer>());
  }

  private boolean evaluate(Operator op, boolean l, boolean r)
  {
    if (op == opAND)
      return l && r;

    if (op == opOR)
      return l || r;

    if (op == opIFTHEN)
      return !l || r;

    if (op == opIF)
      return l || !r;

    if (op == opIFF)
      return l == r;

    if (op == opXOR)
      return l != r;

    if (op == opNAND)
      return !l || !r;

    if (op == opNOR)
      return !l && !r;

    throw new RuntimeException("cannot evaluate");
  }

  private boolean evaluate(Formula f, Map<Integer, Integer> variables)
  {
    if (f.op == opPredicate)
      return predicates.get(f.value).contains(value(f.get(0), variables));

    if (f.op == opRelation)
      return relations.get(f.value).contains(new IntPair(value(f.get(0), variables), value(f.get(1), variables)));

    if (f.op == opIdentity)
      return value(f.get(0), variables) == value(f.get(1), variables);

    if (f.op == opFORALL)
    {
      Map<Integer, Integer> myVariables = new HashMap<Integer, Integer>(variables);
      for (int i = 0; i != universe.length; ++i)
      {
        myVariables.put(f.get(0).value, i);
        if (!evaluate(f.get(1), myVariables))
          return false;
      }
      return true;
    }

    if (f.op == opEXISTS)
    {
      Map<Integer, Integer> myVariables = new HashMap<Integer, Integer>(variables);
      for (int i = 0; i != universe.length; ++i)
      {
        myVariables.put(f.get(0).value, i);
        if (evaluate(f.get(1), myVariables))
          return true;
      }
      return false;
    }

    if (f.op == opSentence)
      return truths.contains(f.value);

    if (f.op == opNOT)
      return !evaluate(f.get(0), variables);

    return evaluate(f.op, evaluate(f.get(0), variables), evaluate(f.get(1), variables));
  }
}

Interpretation randomInterpretation(int numSentences)
{
  Interpretation r = new Interpretation();
  r.truths = new TreeSet<Integer>();
  int[] p = randomPermutation(numSentences);
  for (int i = 0; i != numSentences / 2; ++i)
    r.truths.add(p[i]);

  return r;
}

Interpretation randomInterpretation(int numElements, int numPredicates, int numRelations)
{
  Interpretation r = new Interpretation();
  r.universe = new String[numElements];
  r.individuals = new HashMap<Integer, Integer>();
  r.predicates = new HashMap<Integer, Set<Integer>>();
  r.relations = new HashMap<Integer, Set<IntPair>>();
  for (int i = 0; i != numElements; ++i)
    r.universe[i] = "" + (i + 1);

  for (int i = 0; i != numMarks(opIndConst); ++i)
    r.individuals.put(i, int(random(numElements)));

  for (int i = 0; i != numMarks(opPredicate); ++i)
    r.predicates.put(i, new TreeSet<Integer>());

  for (int i = 0; i != numPredicates; ++i)
    r.predicates.get(int(random(r.predicates.size()))).add(int(random(numElements)));

  for (int i = 0; i != numMarks(opRelation); ++i)
    r.relations.put(i, new TreeSet<IntPair>());

  for (int i = 0; i != numRelations; ++i)
    r.relations.get(int(random(r.relations.size()))).add(new IntPair(int(random(numElements)), int(random(numElements))));

  return r;
}

char markTruthValue(boolean b)
{
  return (b ? config.get(opTrue) : config.get(opFalse)).charAt(0);
}

/*
 * difficulty
 * 0
 * 1 no guide
 * 2 no hints
 * 3 no automatic checks
 * 4 no highlights
 */

class InterpretationTest extends State
{

  State old;
  String id;
  Interpretation interp;
  List<Formula> formulas;
  int difficulty;

  char[] status;
  int curline;
  int skip;

  Button buttonTrue = new Button(config.get(opTrue),  27.5, 0, 2, 2);
  Button buttonFalse = new Button(config.get(opFalse), 30.0, 0, 2, 2);
  Button buttonHint = new Button("Tipp", 27.5, 2.5, 4.5, 2);
  Button buttonDone = new Button("Fertig", 27.5, 15, 4.5, 2);

  InterpretationTest(State old, String id, Interpretation interp, List<Formula> formulas, int difficulty)
  {
    this.old = old;
    this.id = id;
    this.interp = interp;
    this.formulas = formulas;
    this.difficulty = difficulty;

    status = new char[formulas.size()];
    for (int i = 0; i != status.length; ++i)
      status[i] = '-';

    curline = 0;

    buttons.add(buttonTrue);
    buttons.add(buttonFalse);
    buttons.add(buttonHint);
    buttons.add(buttonDone);
    
    if (difficulty >= 2)
      buttonHint.enabled = false;
  }

  void paint()
  {
    fill(BLACK);
    textFont(nFont);
    textAlign(CENTER);

    int line = 0;
    if (interp.truths != null)
    {
      for (int i = 0; i != numMarks(opSentence); ++i)
      {
        if (difficulty < 4 && curline < status.length)
        {
          for (Formula sf: formulas.get(curline).subformulas)
          {
            if (sf.op == opSentence && sf.value == i)
            {
              fill(LTBLUE);
              myRect(2.5 * i, line, 2, 1);
              fill(BLACK);
            }
          }
        }

        myText(formatMark(opSentence, i) + ": " + markTruthValue(interp.truths.contains(i)), (1 + 2.5 * i), line);
      }

      ++line;
    }

    textAlign(LEFT);
    if (interp.universe != null)
    {
      String s = "U: {" + interp.universe[0];
      for (int i = 1; i != interp.universe.length; ++i)
        s += ", " + interp.universe[i];
      s += '}';
      myText(s, 0, line++);

      for (int i = 0; i != numMarks(opIndConst); ++i)
        myText(formatMark(opIndConst, i) + ": " + interp.universe[interp.individuals.get(i)], 4 * i, line);
        
      line++;

      for (int i = 0; i != numMarks(opPredicate); ++i)
      {
        s = formatMark(opPredicate, i) + ": {";
        boolean first = true;
        for (int e: interp.predicates.get(i))
        {
          if (first)
            first = false;
          else
            s += ", ";
          s += interp.universe[e];
        }
        s += "}";

        myText(s, 0, line++);
      }

      for (int i = 0; i != numMarks(opRelation); ++i)
      {
        s = formatMark(opRelation, i) + ": {";
        boolean first = true;
        for (IntPair p: interp.relations.get(i))
        {
          if (first)
            first = false;
          else
            s += ", ";
          s += "\u27e8" + interp.universe[p.first] + "," + interp.universe[p.second] + "\u27e9";
        }
        s += "}";

        myText(s, 0, line++);
      }
    }

    noStroke();
    skip = ++line;

    for (int i = 0; i != status.length; ++i)
    {
      if (i == curline)
      {
        fill(LTRED);
        myRect(0, line, 27, 1);
        fill(BLACK);
      }
      
      if (difficulty < 4 && curline < status.length)
      {
        for (Formula sf: formulas.get(curline).subformulas)
        {
          if (formulas.get(i).equals(sf))
          {
            fill(LTBLUE);
            myRect(0, line, 27, 1);
            fill(BLACK);
          }
        }
      }

      myText(formatFormula(formulas.get(i), false) + ": " + status[i], 0, line++);
    }

    if (difficulty < 1 && curline < formulas.size())
      textWriter(guides.get(formulas.get(curline).op), 27, 15);

    super.paint();
  }

  void enter(boolean b)
  {
    if (curline >= status.length || (difficulty < 3 && b != interp.evaluate(formulas.get(curline))))
    {
      soundDing.play();
      return;
    }

    status[curline++] = markTruthValue(b);
  }

  String check()
  {
    int missing = 0, error = 0;
    for (int i = 0; i != status.length; ++i)
    {
      if (status[i] != config.get(opTrue).charAt(0) && status[i] != config.get(opFalse).charAt(0))
        missing++;
      else if (status[i] != markTruthValue(interp.evaluate(formulas.get(i))))
        error++;
    }

    return missing == 0 && error == 0 ? "" : "Es gibt <<red>> <<emph>> " + error + " <<normal>> Fehler und <<blue>> <<emph>> " + missing + " <<normal>> fehlende Werte.";
  }

  State click(float x, float y)
  {
    Button b = find(x, y);
    if (b == buttonDone)
    {
      String c = check();
      if (c.equals(""))
      {
        if (!id.equals(""))
          config.setSolved(id);

        soundWin.play();
        return new Message(this, "Alles richtig", "", new String[]{"zur Aufgabe", "zum Men\u00FC"});
      }
      else
      {
        soundDing.play();
        return new Message(this, "Nicht ganz richtig", c, new String[]{"zur Aufgabe", "zum Men\u00FC", "Korrektur"});
      }
    }

    if (b == buttonTrue)
      enter(true);

    if (b == buttonFalse)
      enter(false);

    if (b == buttonHint && curline < status.length)
      enter(interp.evaluate(formulas.get(curline)));

    if (difficulty >= 3 && 0 <= x && x < 27 && skip <= y && y < (skip * formulas.size()))
      curline = int(y) - skip;

    redraw();
    return this;
  }

  State key(char code)
  {
    if (code == 'w' || code == 'W' || code == 't' || code == 'T' || code == '1' || code == 'l')
      enter(true);
    else if(code == 'f' || code == 'F' || code == '0')
      enter(false);
    else
      soundDing.play();

    redraw();
    return this;
  }

  State feedback(int x)
  {
    if (x == 1)
      return old;

    if (x == 2)
      for (int i = 0; i != status.length; ++i)
        if (status[i] != markTruthValue(interp.evaluate(formulas.get(i))))
          status[i] = '-';

    return this;
  }

}

class Model extends InterpretationTest
{
  Button buttonModel = new Button("Modell", 0, 15, 7, 2);
  Button buttonCounter = new Button("Gegenmodell", 7.5, 15, 7, 2);
  Model(State old, String id, Interpretation interp, List<Formula> formulas, int difficulty)
  {
    super(old, id, interp, formulas, difficulty);
    buttons.add(buttonModel);
    buttons.add(buttonCounter);
  }
  void paint()
  {
    textAlign(LEFT);
    textFont(nFont);
    fill(BLACK);
    noStroke();
    myText("Was ist diese Interpretation f\u00FCr die Formel: '" + formatFormula(formulas.get(formulas.size() - 1)) + "'?", 0, 13.5);
    super.paint();
  }
  String check()
  {
    String c = super.check();
    if (!c.equals(""))
      return c;

    if (buttonModel.mark == buttonCounter.mark)
      return "W\u00E4hle entweder 'Modell' oder 'Gegenmodell' aus.";

    if (markTruthValue(buttonModel.mark) != status[status.length - 1])
      return "Die Interpretation ist ein " + (buttonModel.mark ? "Modell" : "Gegenmodell");

    return "";
  }
  State click(float x, float y)
  {
    if (buttonModel.inside(x, y))
    {
      buttonModel.mark = !(buttonCounter.mark = false);
    }
    else if (buttonCounter.inside(x, y))
    {
      buttonCounter.mark = !(buttonModel.mark = false);
    }
    else
    {
      return super.click(x, y);
    }
    redraw();
    return this;
  }
}

class ProblemInterpretation extends Problem
{
  boolean propositional;
  int difficulty;
  Formula[] formulas;

  ProblemInterpretation(String id, String[] depends, String label, String[] infos, boolean propositional, int difficulty, String[] formulas)
  {
    super(id, depends, label, infos);
    this.propositional = propositional;
    this.formulas = new Formula[formulas.length];
    this.difficulty = difficulty;
    for (int i = 0; i !=  formulas.length; ++i)
      this.formulas[i] = new Formula(formulas[i]);
  }

  State run(State old)
  {
    Interpretation interp;
    if (propositional)
    {
      Set<Integer> s = new TreeSet<Integer>();
      for (Formula f: formulas)
        f.allTerminals(opSentence, s);

      interp = randomInterpretation(s.size());
    }
    else
    {
      interp = randomInterpretation(6, 8, 12);
    }

    List<Formula> f = new ArrayList<Formula>();
    for (Formula x: formulas)
      f.add(x);

    return new InterpretationTest(old, id, interp, f, difficulty);
  }
}

class ProblemInterpretationRandom extends Problem
{
  Range[] ranges = {new Range("Schwierigkeit", 1, 4, 4), new Range("Operatoren", 1, 8, 3), new Range("Formeln", 1, 8, 8)};
  boolean propositional;

  ProblemInterpretationRandom(String id, String[] depends, boolean propositional)
  {
    super(id, depends, "Z", new String[]{"Zuf\u00E4llige Interpretation"});
    this.propositional = propositional;
  }

  State run(State old)
  {
    return new ParamSelect(old, ranges){
      public State click(float x, float y){
        State s = super.click(x, y);
        if (s == old)
        {
          Interpretation interp = propositional ? randomInterpretation(5) : randomInterpretation(6, 8, 12);
          RandomFormula r = new RandomFormula(5, propositional, true);
          List<Formula> f = new ArrayList<Formula>();
          for (int k = 0; k != ranges[2].value; ++k)
            f.add(r.generate(ranges[1].value));

          return new InterpretationTest(old, id, interp, f, ranges[0].value);
        }
        return s;
      }
    };
  }
}

class ProblemModelRandom extends Problem
{
  Range[] ranges = {new Range("Schwierigkeit", 1, 4, 3), new Range("Operatoren", 1, 8, 6)};
  boolean propositional;

  ProblemModelRandom(String id, String[] depends, boolean propositional)
  {
    super(id, depends, "Z", new String[]{"Zuf\u00E4lliges Modell/Gegenmodell"});
    this.propositional = propositional;
  }

  State run(State old)
  {
    return new ParamSelect(old, ranges){
      public State click(float x, float y){
        State s = super.click(x, y);
        if (s == old)
        {
          Interpretation interp = propositional ? randomInterpretation(5) : randomInterpretation(6, 8, 12);
          RandomFormula r = new RandomFormula(5, propositional, true);
          List<Formula> f = new ArrayList<Formula>();
          subformulas(r.generate(ranges[1].value), f);
          return new Model(old, id, interp, f, ranges[0].value);
        }
        return s;
      }
    };
  }
}

Problem[] makeProblemsInterpretationOptional()
{
  List<Problem> p = new ArrayList<Problem>();
  if (config.getUse(opXOR))
    p.add(new ProblemInterpretation("sem_propositional_interpretation_xor",  new String[]{"sem_propositional_interpretation"}, "$+$", new String[]{"Interpretation der Antivalenz"}, true, 0, new String[]{"+p0p1", "+p1p0", "+p0p0", "+p1p1"}));
  if (config.getUse(opIF))
    p.add(new ProblemInterpretation("sem_propositional_interpretation_if",   new String[]{"sem_propositional_interpretation"}, "$<$", new String[]{"Interpretation der Reflektion"}, true, 0, new String[]{"<p0p1", "<p1p0", "<p0p0", "<p1p1"}));
  if (config.getUse(opNAND))
    p.add(new ProblemInterpretation("sem_propositional_interpretation_nand", new String[]{"sem_propositional_interpretation"}, "$^$", new String[]{"Interpretation des Schefferschen Strichs"}, true, 0, new String[]{"^p0p1", "^p1p0", "^p0p0", "^p1p1"}));
  if (config.getUse(opNOR))
    p.add(new ProblemInterpretation("sem_propositional_interpretation_nor",  new String[]{"sem_propositional_interpretation"}, "$_$", new String[]{"Interpretation des Peirceschen Zeichens"}, true, 0, new String[]{"_p0p1", "_p1p0", "_p0p0", "_p1p1"}));

  p.add(new ProblemInterpretationRandom("", new String[]{"sem_propositional_interpretation_complex"}, true));

  Problem[] r = new Problem[p.size()];
  for (int i = 0; i != r.length; ++i)
    r[i] = p.get(i);

  return r;
}

Problem[] makeProblemsTruthTableSemanticsOptional()
{
  List<Problem> p = new ArrayList<Problem>();
  if (config.getUse(opXOR))
    p.add(new ProblemTruthTable("sem_truth_table_xor",  new String[]{"sem_truth_table"}, "$+$", new String[]{"Wahrheitswerttabelle f\u00FCr Antivalenz"}, new Formula("+p0p1"), 0, ""));
  if (config.getUse(opIF))
    p.add(new ProblemTruthTable("sem_truth_table_if",   new String[]{"sem_truth_table"}, "$<$", new String[]{"Wahrheitswerttabelle f\u00FCr Reflexion"}, new Formula("<p0p1"), 0, ""));
  if (config.getUse(opNAND))
    p.add(new ProblemTruthTable("sem_truth_table_nand", new String[]{"sem_truth_table"}, "$^$", new String[]{"Wahrheitswerttabelle f\u00FCr Schefferschen Strich"}, new Formula("^p0p1"), 0, ""));
  if (config.getUse(opNOR))
    p.add(new ProblemTruthTable("sem_truth_table_nor",  new String[]{"sem_truth_table"}, "$_$", new String[]{"Wahrheitswerttabelle f\u00FCr Peirceschen Pfeil"}, new Formula("_p0p1"), 0, ""));

  Problem[] r = new Problem[p.size()];
  for (int i = 0; i != r.length; ++i)
    r[i] = p.get(i);

  return r;
}

Problem[][] makeProblemsSemanticsLProp()
{
  return new Problem[][]{
    makeProblemsTranslation(true),
    {
    },
    new Problem[]{
      new ProblemInfo("sem_propositional_interpretation", new String[]{"lang_propositional_syntax"},
        "Interpretation", toArray(translations.getJSONArray("sem_propositional_interpretation"))),
      new ProblemInterpretation("sem_propositional_interpretation_neg",  new String[]{"sem_propositional_interpretation"}, "$-$", new String[]{"Interpretation der Negation"}, true, 0, new String[]{"-p0", "-p1"}),
      new ProblemInterpretation("sem_propositional_interpretation_and",  new String[]{"sem_propositional_interpretation"}, "$&$", new String[]{"Interpretation der Konjunktion"}, true, 0, new String[]{"&p0p1", "&p1p0", "&p0p0", "&p1p1"}),
      new ProblemInterpretation("sem_propositional_interpretation_or",   new String[]{"sem_propositional_interpretation"}, "$v$", new String[]{"Interpretation der Disjunktion"}, true, 0, new String[]{"vp0p1", "vp1p0", "vp0p0", "vp1p1"}),
      new ProblemInterpretation("sem_propositional_interpretation_cond", new String[]{"sem_propositional_interpretation"}, "$>$", new String[]{"Interpretation der Implikation"}, true, 0, new String[]{">p0p1", ">p1p0", ">p0p0", ">p1p1"}),
      new ProblemInterpretation("sem_propositional_interpretation_iff",  new String[]{"sem_propositional_interpretation"}, "$#$", new String[]{"Interpretation des Bikonditional"}, true, 0, new String[]{"#p0p1", "#p1p0", "#p0p0", "#p1p1"}),
      new ProblemInterpretation("sem_propositional_interpretation_complex", new String[]{"sem_propositional_interpretation_neg", "sem_propositional_interpretation_and", "sem_propositional_interpretation_or",
        "sem_propositional_interpretation_cond", "sem_propositional_interpretation_iff"}, "()", new String[]{"Geschachtelte Formeln", "Geschachtelte Formeln werden in der Reihenfolge ausgewertet, wie sie im Formeleditor gebaut werden, d.h. Teilformeln werden zuerst ausgewertet."}, true, 2, new String[]{"vp1p2", "-vp1p2", "#p3p1", ">p0#p3p1", "&-vp1p2>p0#p3p1"})
    },
    makeProblemsInterpretationOptional(),
    {
    },
    new Problem[]{
      new ProblemInfo("sem_models", new String[]{"sem_propositional_interpretation_complex"}, "Modelle", toArray(translations.getJSONArray("sem_models"))),
      new ProblemModelRandom("", new String[]{"sem_models"}, true),
    },
    {
    },
    new Problem[]{
      new ProblemInfo("sem_truth_table", new String[]{"sem_propositional_interpretation_complex"}, "Tabellen", toArray(translations.getJSONArray("sem_truth_table"))),
      new ProblemTruthTable("sem_truth_table_neg",  new String[]{"sem_truth_table"}, "$-$", new String[]{"Wahrheitswerttabelle f\u00FCr Negation"}, new Formula("-p0"), 0, ""),
      new ProblemTruthTable("sem_truth_table_and",  new String[]{"sem_truth_table"}, "$&$", new String[]{"Wahrheitswerttabelle f\u00FCr Konjunktion"}, new Formula("&p0p1"), 0, ""),
      new ProblemTruthTable("sem_truth_table_or",   new String[]{"sem_truth_table"}, "$v$", new String[]{"Wahrheitswerttabelle f\u00FCr Disjunktion"}, new Formula("vp0p1"), 0, ""),
      new ProblemTruthTable("sem_truth_table_cond", new String[]{"sem_truth_table"}, "$>$", new String[]{"Wahrheitswerttabelle f\u00FCr Implikation"}, new Formula(">p0p1"), 0, ""),
      new ProblemTruthTable("sem_truth_table_iff",  new String[]{"sem_truth_table"}, "$#$", new String[]{"Wahrheitswerttabelle f\u00FCr Bikonditional"}, new Formula("#p0p1"), 0, ""),
    },
    makeProblemsTruthTableSemanticsOptional(),
    new Problem[]{
      new ProblemTruthTable("sem_truth_table_complex2", new String[]{"sem_truth_table_neg", "sem_truth_table_and", "sem_truth_table_or",
        "sem_truth_table_cond", "sem_truth_table_iff"}, "(1)", toArray(translations.getJSONArray("sem_truth_table_complex2")), new Formula("&-p0p1"), 2, ""),
      new ProblemTruthTable("sem_truth_table_complex3", new String[]{"sem_truth_table_complex2"}, "(2)", toArray(translations.getJSONArray("sem_truth_table_complex3")), new Formula("&p0>p0p1"), 3, ""),
      new ProblemTruthTable("sem_truth_table_complex4", new String[]{"sem_truth_table_complex3"}, "(3)", toArray(translations.getJSONArray("sem_truth_table_complex4")), new Formula("&p0>p1--p0"), 4, ""),
      new ProblemTruthTable("sem_truth_table_complex5", new String[]{"sem_truth_table_complex4"}, "(4)", toArray(translations.getJSONArray("sem_truth_table_complex5")), new Formula("&->p0p1vp1--p0"), 5, ""),
      new ProblemTruthTable("sem_truth_table_complex6", new String[]{"sem_truth_table_complex5"}, "(5)", toArray(translations.getJSONArray("sem_truth_table_complex6")), new Formula("&&p0p1p2"), 6, ""),
      new ProblemTruthTable("sem_truth_table_complex", new String[]{"sem_truth_table_complex6"}, "(6)", toArray(translations.getJSONArray("sem_truth_table_complex")), new Formula("&-vp1p0>p0#p2p1"), 6, ""),
      new ProblemTruthTableRandom(),
      new ProblemTruthTableSelect()
    }
  };
}

Problem[][] makeProblemsSemanticsLPred()
{
  return new Problem[][]{
    makeProblemsTranslation(false),
    {
    },
    new Problem[]{
      new ProblemInfo("sem_predicate_interpretation", new String[]{"lang_predicate_syntax", "sem_propositional_interpretation"},
      "Interpretation", toArray(translations.getJSONArray("sem_predicate_interpretation"))),
      new ProblemInterpretation("sem_predicate_interpretation_identity", new String[]{"sem_predicate_interpretation"}, "$=$", new String[]{"Interpretation der Identit\u00E4t"}, false, 0, new String[]{"=a0a1", "=a1a2", "=a2a3", "=a3a4", "=a0a0"}),
      new ProblemInterpretation("sem_predicate_interpretation_preciate", new String[]{"sem_predicate_interpretation"}, "$B0b0$", new String[]{"Interpretation von Pr\u00E4dikaten"}, false, 0, new String[]{"P0a1", "P1a0", "P2a0", "P0a3", "P2a4", "P1a2"}),
      new ProblemInterpretation("sem_predicate_interpretation_relation", new String[]{"sem_predicate_interpretation"}, "$C0b0b1$", new String[]{"Interpretation von Relationen"}, false, 0, new String[]{"R0a1a0", "R1a0a2", "R2a0a0", "R0a0a1", "R2a4a2", "R1a2a3"}),
      new ProblemInterpretation("sem_predicate_interpretation_forall", new String[]{"sem_predicate_interpretation"}, "$A$", new String[]{"Interpretation des Allquantors"}, false, 0, new String[]{"Ax0P0x0", "Ax1P0x1", "Ax0P1x0", "Ax0P2x0", "Ax0Ax1R0x0x1"}),
      new ProblemInterpretation("sem_predicate_interpretation_exists", new String[]{"sem_predicate_interpretation"}, "$E$", new String[]{"Interpretation des Existenzquantors"}, false, 0, new String[]{"Ex0P0x0", "Ex1P0x1", "Ex0P1x0", "Ex0P2x0"}),
      new ProblemInterpretationRandom("", new String[]{"sem_predicate_interpretation"}, false)
    }
  };
}

/*
  void paintX(int n, int x, int y, boolean black)
  {
    if ((x+y)%2 == 0)
    {
      fill(WHITE);
      myText("" + char('\u2654' + n + (black ? 0 : 6)), (x + 0.5), y);
    }
    else
    {
      fill(BLACK);
      myText("" + char('\u2654' + n + (black ? 6 : 0)), (x + 0.5), y);
    }
  }

  void paintChecker()
  {
    scale(2);
    stroke(strokeWeightH);
    textAlign(CENTER);
    for (int i = 0; i != 8; ++i)
    {
      for (int j = 0; j != 8; ++j)
      {
        if ((i+j)%2 == 0)
          fill(BLACK);
        else
          noFill();

        rect(i, j, 1, 1);
        paintX(i % 6, i, j, j < 4);
      }
    }

    textAlign(LEFT);
    myText("\u265A\u265B\u265C\u265D\u265E\u265F \u2660\u2663\u2665\u2666", 0, 8);
  }

*/
