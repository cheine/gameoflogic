/*
 * A Game of Logic (not to be confused with the book "The Game of Logic" by Lewis Caroll)
 * Copyright (C) 2018-2019  Christian Heine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.HashSet;

import processing.sound.SoundFile;

// some porgram-wide constants relating to fonts and font sizes
PFont nFont, eFont; // a normal and a bold font
final float fontSize = 48; // font size in points
final float realAscent = 43; // amount of space over font baseline in pixels
final float realDescent = 13; // amount of space under baseline in pixels
final float realHeightX = 56; // distance between two baselines in pixels 
final float strokeWeightV = fontSize / realHeightX * 0.099; // vertical stroke weight of selected font
final float strokeWeightH = fontSize / realHeightX * 0.083; // horizontal stroke weight of selected font

// neutral stroke and fill colors
final color BLACK = color(0);
final color LTGRAY = color(255 - 8);
final color GRAY = color(255 - 24);
final color WHITE = color(255);

// fill signal colors
final color RED = color(255, 255 - 48, 255 - 48);
final color BLUE = color(255 - 48, 255 - 48, 255);
final color LTRED = color(255, 255 - 16, 255 - 16);
final color LTBLUE = color(255 - 16, 255 - 16, 255);

// neutral and signal text colors
final color TEXTGRAY = color(127);
final color TEXTRED = color(191, 0, 0);
final color TEXTBLUE = color(0, 0, 191);

// draws a rounded rect
void myRect(float x, float y, float w, float h)
{
  rect(x, y, w, h, 0.2 * h);
}

// width of string in virtual coordinate system
float myTextWidth(String t)
{
  return textWidth(t) / realHeightX;
}

// draw text at virtual coordinates
void myText(String t, float x, float y)
{
  pushMatrix();
  scale(1 / realHeightX);
  text(t, x * realHeightX, y * realHeightX + realAscent);
  popMatrix();
}

// replaces symbol code enclosed by "$" signs with symbols 
String filterFormula(String s)
{
  String r = "";
  int i = 0;
  boolean inFormula = false;
  while (i != s.length())
  {
    if (s.charAt(i) == '$')
    {
      ++i;
      inFormula = !inFormula;
      continue;
    }

    if (!inFormula)
    {
      r += s.charAt(i++);
      continue;
    }
    
    char c = s.charAt(i++);
    Operator op = operatorLookup.get(c);
    if (op == null)
    {
      r += c;
      continue;
    }

    int number = 0;
    String signature = signatures.get(op);
    if (signature != null && signature.length() > 1 && signature.charAt(1) == '#')
      while (i < s.length() && '0' <= s.charAt(i) && s.charAt(i) <= '9')
        number = 10 * number + (s.charAt(i++) - '0');

    r += formatMark(op, number);
  }
  return r;
}

// format and write multiline text, supports tags <<br>> for line break, <<par>> for linebreak plus 0.5 line space, coloring, emphasis, etc.
void textWriter(String txt, float max, float y)
{
  txt = filterFormula(txt);
  String[] lines = txt.split("\n");
  for (String l: lines)
  {
    fill(BLACK);
    textFont(nFont);
    String[] words = l.split(" ");
    float x = 0;
    for (String w: words)
    {
      if (w.startsWith("<<") && w.endsWith(">>"))
      {
        String c = w.substring(2, w.length() - 2);
        if (c.equals("br"))
        {
          x = 0;
          y += 1;
        }
        else if (c.equals("par"))
        {
          x = 0;
          y += 1.5;
        }
        else if (c.equals("normal"))
        {
          fill(BLACK);
          textFont(nFont);
        }
        else if (c.equals("red"))
        {
          fill(TEXTRED);
        }
        else if (c.equals("blue"))
        {
          fill(TEXTBLUE);
        }
        else if (c.equals("emph"))
        {
          textFont(eFont);
        }
        else
        {
          println("unknown: '" + c + "'");
        }
      }
      else
      {
        float d = myTextWidth(w);
        if (x != 0 && x + d > max)
        {
          x = 0;
          y += 1;
        }
        myText(w, x, y);
        x += d + myTextWidth(" ");
      }
    }
    y += 1;
  }
}

// helper class for a clickable element
class Button
{

  String label;
  float x, y, w, h; // x,y, width, height
  boolean mark = false;
  boolean done = false;
  boolean enabled = true;

  Button(String label, float x, float y, float w, float h)
  {
    this.label = label;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  void paint()
  {
    noStroke();
    if (mark)
    {
      fill(RED);
    }
    else if (done)
    {
      fill(BLUE);
    }
    else
    {
      fill(GRAY);
    }

    myRect(x, y, w, h);

    if (enabled)
      fill(BLACK);
    else
      fill(TEXTGRAY);
    textFont(nFont);
    textAlign(CENTER);
    String[] lines = label.split("\n");
    for (int i = 0; i != lines.length; ++i)
      myText(filterFormula(lines[i]), x + 0.5 * w, y + 0.5 * (h + 2 * i - lines.length));
  }

  boolean inside(float x, float y)
  {
    return enabled && this.x <= x && x < this.x + w && this.y <= y && y < this.y + h;
  }

}

// superclass for the different "screens" of the program, form states of a finite state automaton
class State
{
  List<Button> buttons = new ArrayList<Button>();

  void paint()
  {
    for (Button b: buttons)
      b.paint();
  }

  Button find(float x, float y)
  {
    for (Button b: buttons)
      if (b.inside(x, y))
        return b;

    return null;
  }

  void drag(float x, float y) {}
  State click(float x, float y) { return this; }
  State key(char code) { return this; }
  State feedback(int x) { return this; }
}

// "screen" for showing info and tutorial text
class Info extends State
{

  State old;
  String title, info;

  Info(State old, String title, String info)
  {
    this.old = old;
    this.title = title;
    this.info = info;

    if (info.equals(""))
    {
      RandomFormula r = new RandomFormula(5, false, true);
      for (int i = 0; i != 10; ++i)
        this.info += formatFormula(r.generate(10), false) + '\n';
    }

    buttons.add(new Button(translations.getString("buttonNext"), 27.5, 15, 4.5, 2));
  }

  void paint()
  {
    fill(BLACK);
    textAlign(LEFT);
    scale(2);
    textWriter("<<emph>> " + title, 16, 0);
    scale(0.5);
    textWriter(info, 32, 3);
    super.paint();
  }

  State click(float x, float y)
  {
    if (buttons.get(0).inside(x, y))
      return old;

    return this;
  }

}

// "screen" for overlaying a small message window over another screen
class Message extends State
{

  final float maxX = 24;
  final float maxY = 13;

  State old;
  String title, info;
  String[] options;

  Message(State old, String title, String info, String[] options)
  {
    this.old = old;
    this.title = title;
    this.info = info;
    this.options = options;

    float x = 0.5 * (32 - maxX);
    float y = 0.5 * (17 - maxY);
    float w = (maxX + 0.5) / options.length;
    for (int i = 0; i != options.length; ++i)
      buttons.add(new Button(options[i], x + i * w, y + maxY - 2, w - 0.5, 2));
  }

  void paint()
  {
    old.paint();
    noStroke();
    fill(WHITE, 191);
    rect(0, 0, 32, 17);

    pushMatrix();
    translate(0.5 * (32 - maxX), 0.5 * (17 - maxY));

    noStroke();
    fill(LTGRAY);
    rect(-0.5, -0.5, maxX + 1, maxY + 1, 1);

    fill(BLACK);
    textAlign(LEFT);
    textFont(eFont);
    myText(title, 0, 0);
    textFont(nFont);
    textWriter(info, maxX, 2);
    popMatrix();
    super.paint();
  }

  State click(float x, float y)
  {
    for (int i = 0; i != buttons.size(); ++i)
      if (buttons.get(i).inside(x, y))
        return old.feedback(i);

    return this;
  }
}

Config config; // current config
JSONObject translations; // translations of some tutorials
SoundFile soundWin, soundDing, soundKey, soundAchieve, soundFail; // different sounds
State state; // current "screen"

// helper for retrieving translation text
Map<String, String[]> getStrings(String name)
{
  Map<String, String[]> m = new HashMap<String, String[]>();
  JSONArray a = translations.getJSONArray(name);
  for (int i = 0; i != a.size(); ++i)
  {
    JSONArray aa = a.getJSONArray(i);
    String[] x = new String[aa.size() - 1];
    for (int j = 0; j != x.length; ++j)
      x[j] = aa.getString(j+1);

    m.put(aa.getString(0), x);
  }
  return m;
}

// immutable sketch settings
void settings()
{
  size(854, 480);
}

// sketch initialization
void setup()
{
  surface.setResizable(true);
  surface.setIcon(loadImage("icon.png"));
  surface.setTitle("A Game of Logic");
  frameRate(60);
  declareOperators();
  nFont = createFont("DejaVuSans.ttf", fontSize, true);
  eFont = createFont("DejaVuSans-Bold.ttf", fontSize, true);
  soundAchieve = new SoundFile(this, "322929__rhodesmas__success-04.wav");
  soundDing = new SoundFile(this, "322931__rhodesmas__incorrect-01.wav");
  soundKey = new SoundFile(this, "322898__rhodesmas__ui-02.wav");
  soundWin = new SoundFile(this, "320775__rhodesmas__win-02.wav");
  soundFail = new SoundFile(this, "342756__rhodesmas__failure-01.wav");
  config = new Config();
  guides = initGuides();
  translations = loadJSONObject("strings_de.json");
  state = new Title();
}

// the following is an ugly hack to support redraw on size changes
boolean dirty = true;
int old_width = 0;
int old_height = 0;
void redraw()
{
  dirty = true;
  super.redraw();
}

void draw()
{
  if (!dirty && old_width == width && old_height == height)
    return;

  dirty = false;
  old_width = width;
  old_height = height;

  background(WHITE);

  // scale and center virtual drawing area of 32 * 17 units
  float zoom = min(width / 32, height / (1080.0 / realHeightX));
  pushMatrix();
  translate(width / 2, height / 2);
  scale(zoom);
  translate(-16.0, -8.5);
  state.paint();
  popMatrix();
}

void restate(State s)
{
  if (s == null)
    exit();

  if (s != state)
  {
    state = s;
    redraw();
  }
}

void mouseDragged()
{
  float zoom = min(width / 32, height / (1080.0 / realHeightX));
  if (state != null)
    state.drag((mouseX - 0.5 * width) / zoom + 16, (mouseY - 0.5 * height) / zoom + 8.5);
}

void mouseClicked()
{
  float zoom = min(width / 32, height / (1080.0 / realHeightX));
  if (state != null)
    restate(state.click((mouseX - 0.5 * width) / zoom + 16, (mouseY - 0.5 * height) / zoom + 8.5));
}

// cancel quit on ESC
void keyPressed()
{
  if (key == ESC)
    key = '\0';
}

void keyTyped()
{
  if (state != null)
    restate(state.key(key));
}

class Title extends State
{

  Button buttonStart = new Button(translations.getString("buttonStart"), 12.5, 11, 7, 2);
  Button buttonConfig = new Button(translations.getString("buttonConfig"), 5.0, 15, 7, 2);
  Button buttonCredits = new Button(translations.getString("buttonCredits"), 12.5, 15, 7, 2);
  Button buttonQuit = new Button(translations.getString("buttonQuit"), 20.0, 15, 7, 2);

  Title()
  {
    buttons.add(buttonStart);
    buttons.add(buttonConfig);
    buttons.add(buttonCredits);
    buttons.add(buttonQuit);
  }

  void paint()
  {
    noStroke();
    fill(BLACK);
    pushMatrix();
    translate(16, 7.5);
    textAlign(CENTER);
    textFont(nFont);
    pushMatrix();
    String t = "A G\u1D00\u1D0D\u1D07 \u1D0F\uA730 L\u1D0F\u0262\u026A\u1D04";
    scale(32 / myTextWidth(t));
    myText(t, 0, -1);
    popMatrix();
    pushMatrix();
    t = "(not to be confused with the book \u00BBThe Game of Logic\u00AB by Lewis Caroll)";
    scale(32 / myTextWidth(t));
    myText(t, 0, -0.5);
    popMatrix();
    popMatrix();
    super.paint();
  }

  State click(float x, float y)
  {
    Button b = find(x, y);

    if (b == buttonQuit)
      return null;

    if (b == buttonConfig)
      return new ConfigEdit(this);

    if (b == buttonStart)
      return new Hub(this);

    if (b == buttonCredits)
      return new Info(this, "A Game of Logic", "https://bitbucket.org/cheine/gameoflogic\n\n" +
        "Copyright \u00a9 2018-2019 by Christian Heine\n" +
        "This is free software; see the source for copying conditions.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\n" +
        "Uses sounds by Andy Rhode CC-3.0-BY\nhttps://freesound.org/people/rhodesmas\n\n" +
        "Written using Processing\nhttps://processing.org)");

    return this;
  }

  State key(char code)
  {
    if (code == ESC)
      return null;

    return this;
  }

}
