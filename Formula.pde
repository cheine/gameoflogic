/*
 * A Game of Logic (not to be confused with the book "The Game of Logic" by Lewis Caroll)
 * Copyright (C) 2018-2019  Christian Heine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// to translate from serialized formula to internal representation
static Map<Character, Operator> operatorLookup = new HashMap<Character, Operator>();

static final class Operator
{
  char transition; // for serialization
  Operator(char transition)
  {
    this.transition = transition;
    operatorLookup.put(transition, this);
  }
}

static final Operator opNIL       = new Operator(' '); // dummy operator for formating formulas without outer brackets
static final Operator opTrue      = new Operator('T');
static final Operator opFalse     = new Operator('F');
static final Operator opIndConst  = new Operator('a');
static final Operator opIndVar    = new Operator('x');
static final Operator opIdentity  = new Operator('=');
static final Operator opPredicate = new Operator('P');
static final Operator opRelation  = new Operator('R');
static final Operator opSentence  = new Operator('p');
static final Operator opNOT       = new Operator('-');
static final Operator opAND       = new Operator('&');
static final Operator opOR        = new Operator('v');
static final Operator opIFTHEN    = new Operator('>');
static final Operator opIF        = new Operator('<');
static final Operator opIFF       = new Operator('#');
static final Operator opXOR       = new Operator('+');
static final Operator opNAND      = new Operator('^');
static final Operator opNOR       = new Operator('_');
static final Operator opFORALL    = new Operator('A');
static final Operator opEXISTS    = new Operator('E');
static final Operator opIOTA      = new Operator('i');
static final Operator opIndConstM  = new Operator('b'); // metavariable
static final Operator opIndVarM    = new Operator('c'); // metavariable
static final Operator opPredicateM = new Operator('B'); // metavariable
static final Operator opRelationM  = new Operator('C'); // metavariable
static final Operator opSentenceM  = new Operator('S'); // metavariable

// format properties for an operator
static class Format
{
  int precedence;
  boolean associative;
  Format(int precedence, boolean associative)
  {
    this.precedence = precedence;
    this.associative = associative;
  }
}

// turn an operator and an operator id into a mark and a subscript
String formatMark(Operator op, int id)
{
  String m = config.get(op);
  String result = "";
  for (int value = id / m.length(); value > 0; value /= 10)
    result = char('\u2080' + value % 10) + result;

  return m.charAt(id % m.length()) + result;
}

static HashMap<Operator, String> signatures = new HashMap<Operator, String>();
static HashMap<Operator, Format> formats = new HashMap<Operator, Format>();

static void declareOperator(Operator op, String signature, int precedence, boolean associative)
{
  signatures.put(op, signature);
  formats.put(op, new Format(precedence, associative));
}

static void declareOperators()
{
  declareOperator(opIndConst,  "I#",   0, false);
  declareOperator(opIndConstM, "I#",   0, false);
  declareOperator(opIndVar,    "I#",   0, false);
  declareOperator(opIndVarM,   "I#",   0, false);
  declareOperator(opIOTA,      "IIT",  0, false);
  declareOperator(opPredicate, "T#I",  0, false);
  declareOperator(opPredicateM,"T#I",  0, false);
  declareOperator(opRelation,  "T#II", 0, false);
  declareOperator(opRelationM, "T#II", 0, false);
  declareOperator(opIdentity,  "TII",  1, false);
  declareOperator(opFORALL,    "TIT",  0, true);
  declareOperator(opEXISTS,    "TIT",  0, true);
  declareOperator(opSentence,  "T#",   0, false);
  declareOperator(opSentenceM, "T#",   0, false);
  declareOperator(opNOT,       "TT",   0, false);
  declareOperator(opAND,       "TTT",  2, true);
  declareOperator(opOR,        "TTT",  2, true);
  declareOperator(opIFTHEN,    "TTT",  3, false);
  declareOperator(opIF,        "TTT",  3, false);
  declareOperator(opIFF,       "TTT",  4, true);
  declareOperator(opXOR,       "TTT",  4, true);  // either-or
  declareOperator(opNAND,      "TTT",  4, false); // not-both
  declareOperator(opNOR,       "TTT",  4, false); // neither-nor
  declareOperator(opNIL,       "",     9, false); // dummy for brackets
}

// formula representation
static class Formula
{
  Operator op;
  int value = -1;
  final Formula[] subformulas;

  // construct formula without children
  Formula(Operator op, int value)
  {
    this(op, value, new Formula[0]);
  }

  // construct formula with children
  Formula(Operator op, int value, Formula[] subformulas)
  {
    this.op = op;
    this.value = value;
    this.subformulas = subformulas;
  }

  // deep copy of a formula
  Formula(Formula f)
  {
    op = f.op;
    value = f.value;
    subformulas = new Formula[f.subformulas.length];
    for (int i = 0; i != subformulas.length; ++i)
      subformulas[i] = new Formula(f.get(i));
  }

  // deep copy of a fomula, replace all occurenced equal to o with a deep copy of the formula n
  Formula(Formula f, Formula o, Formula n)
  {
    Formula x = f.equals(o) ? n : f;
    op = x.op;
    value = x.value;
    subformulas = new Formula[f.subformulas.length];
    for (int i = 0; i != subformulas.length; ++i)
      subformulas[i] = new Formula(f.get(i), o, n);
  }

  // construct formula for a serialized representation
  Formula(String s)
  {
    op = operatorLookup.get(s.charAt(0));
    String signature = signatures.get(op);
    int i = 1;
    List<Formula> l = new ArrayList<Formula>();
    for (int j = 0; j != signature.length() - 1; ++j)
    {
      if (signature.charAt(1+j) == '#')
      {
        value = 0;
        while (i < s.length() && '0' <= s.charAt(i) && s.charAt(i) <= '9')
          value = 10 * value + s.charAt(i++) - '0';
      }
      else
      {
        Formula f = new Formula(s.substring(i));
        i += f.toString().length();

        if (signature.charAt(1+j) != signatures.get(f.op).charAt(0))
          println("Formula parsing error!");

        l.add(f);
      }
    }

    subformulas = new Formula[l.size()];
    for (int k = 0; k != l.size(); ++k)
      subformulas[k] = l.get(k);
  }

  // give i-th subformula
  Formula get(int i)
  {
    return subformulas[i];
  }

  // deep check if two formulas are equal
  boolean equals(Formula f)
  {
    if (op != f.op || value != f.value || subformulas.length != f.subformulas.length)
      return false;

    for (int i = 0; i != subformulas.length; ++i)
      if (!get(i).equals(f.get(i)))
        return false;

    return true;
  }

  // create serialized representation of formula
  String toString()
  {
    String result = "" + op.transition;
    if (value != -1)
      result += value;

    for (Formula f: subformulas)
      result += f.toString();

    return result;
  }

  // check if formula is atomic
  boolean isAtomic()
  {
    return op == opSentence || op == opPredicate || op == opRelation || op == opIdentity;
  }

  // check if formula is atomic or negation of atomic formula
  boolean isLiteral()
  {
    return (op != opNOT ? this : get(0)).isAtomic();
  }

  // check if formula is a kind of quantification
  boolean isQuantification()
  {
    return op == opFORALL || op == opEXISTS || op == opIOTA;
  }

  // negate formula
  Formula negate()
  {
    return new Formula(opNOT, -1, new Formula[]{new Formula(this)});
  }

  // give a list of ids of all nodes with operator op
  List<Integer> allTerminals(Operator op)
  {
    Set<Integer> s = new TreeSet<Integer>();
    allTerminals(op, s);
    return new ArrayList<Integer>(s);
  }

  private void allTerminals(Operator op, Set<Integer> l)
  {
    if (this.op == op)
      l.add(value);

    for (Formula sf: subformulas)
      sf.allTerminals(op, l);
  }

  Formula normalizeQuantification()
  {
    return normalizeQuantification(new HashMap<Integer, Integer>());
  }

  private Formula normalizeQuantification(Map<Integer, Integer> m)
  {
    if (op == opIndVar)
      return new Formula(op, m.get(value));

    if (isQuantification())
    {
      m = new HashMap<Integer, Integer>(m);
      m.put(get(0).value, m.size());
    }

    Formula[] f = new Formula[subformulas.length];
    for (int i = 0; i != f.length; ++i)
      f[i] = subformulas[i].normalizeQuantification(m);

    return new Formula(op, value, f);
  }

}

// count how many bracket pairs would be left out when formating the formula using the precedence rule
int countBracketPrecedence(Formula f)
{
  int r = 0;
  int p = formats.get(f.op).precedence;
  for (Formula sf: f.subformulas)
  {
    int q = formats.get(sf.op).precedence;
    r += (p > 0 && q > 0 && p > q) ? 1 : 0; 
    r += countBracketPrecedence(sf);
  }

  return r;
}

// test whether main operator is not imminent
boolean bracketPrecedence(Formula f)
{
  int p = formats.get(f.op).precedence; // precedence > 0 indicates infix operator
  if (p <= 0)
    return false;

  int p0 = formats.get(f.get(0).op).precedence;
  int p1 = formats.get(f.get(1).op).precedence;
  return (p0 > 0 && p > p0) || (p1 > 0 && p > p1);
}

// create String representation of formula with possibly leaving out brackets according to outer&precedence, and/or associative convention
// recursive, top-level call uses opNIL for value of outer
String formatFormula(Formula f, Operator outer, boolean precedence, boolean associative)
{
  Format t = formats.get(f.op);
  String result = formatMark(f.op, f.value);
  String x[] = new String[f.subformulas.length];
  for (int i = 0; i != x.length; ++i)
    x[i] = formatFormula(f.get(i), f.op, precedence, associative);

  if (t.precedence != 0)
  {
    result = x[0] + "\u200A" + result + "\u200A" + x[1];
    Format y = formats.get(outer);
    if ((f.op != outer && (!precedence || t.precedence >= y.precedence)) || (f.op == outer && (!associative || !t.associative)))
      result = "(" + result + ")";
  }
  else
  {
    if (f.isQuantification() && x[1].charAt(0) != '(')
      x[0] += "\u200A";

    for (String s: x)
      result += s;
  }
  return result;
}

// format formula leaving out brackets according to precedence, but not associative convention
String formatFormula(Formula f)
{
  return formatFormula(f, opNIL, true, false);
}

// format formula leaving out all or no brackets according to conventions
String formatFormula(Formula f, boolean brackets)
{
  return formatFormula(f, opNIL, !brackets, !brackets);
}

// generator for random formulas
class RandomFormula
{

  int individuals; // number of individuals/atomic sentences
  boolean propositional; // whether propositional formulas are to be generated
  
  // operator probabilities, in percent
  int probNOT    = 15; 
  int probAND    = 30;
  int probOR     = 10;
  int probIFTHEN = 15;
  int probIFF    = 10;
  int probNAND   =  0;
  int probNOR    =  0;
  int probIF     =  0;
  int probXOR    =  0;
  int probFORALL = 10;
  int probEXISTS = 10;

  // initialize random formula generator
  RandomFormula(int individuals, boolean propositional, boolean exotic)
  {
    this.individuals = individuals;
    this.propositional = propositional;
    if (propositional)
    {
      probNOT    +=  5;
      probAND    +=  5;
      probIFTHEN +=  5;
      probIFF    +=  5;
      probFORALL -= 10;
      probEXISTS -= 10;
    }

    if (exotic && config.getUse(opNAND))
    {
      probNAND += 5;
      probNOT -= 2;
      probAND -= 3;
    }

    if (exotic && config.getUse(opNOR))
    {
      probNOR += 5;
      probNOT -= 3;
      probAND -= 2;
    }

    if (exotic && config.getUse(opXOR))
    {
      probXOR += 5;
      probIFF -= 5;
    }

    if (exotic && config.getUse(opIF))
    {
      probIF += 5;
      probIFTHEN -= 5;
    }
  }

  // attempt to add a quantor to the formula, if it fails add a negation instead
  private Formula ifPossibleQuantifyElseNegate(Formula f, Operator op)
  {
    List<Integer> c = f.allTerminals(opIndConst);
    List<Integer> v = f.allTerminals(opIndVar);
    if (c.isEmpty() || v.size() > 3)
      return f.negate();

    Formula randConst = new Formula(opIndConst, c.get(int(random(c.size()))));
    Formula randVar = new Formula(opIndVar, int(random(3 - v.size())));
    while (v.contains(randVar.value))
      ++randVar.value;

    return new Formula(op, -1, new Formula[]{randVar, new Formula(f, randConst, randVar)});
  }

  Formula randIndConst()
  {
    return new Formula(opIndConst, int(random(individuals)));
  }

  // generate formula with given number of operators
  Formula generate(int operators)
  {
    if (operators == 0)
    {
      if (propositional)
        return new Formula(opSentence, int(random(individuals)));

      int x = int(random(20));
      if (x < 5)
        return new Formula(opIdentity, -1, new Formula[]{randIndConst(), randIndConst()});

      if (x < 10)
        return new Formula(opRelation, int(random(numMarks(opRelation))), new Formula[]{randIndConst(), randIndConst()});

      return new Formula(opPredicate, int(random(numMarks(opPredicate))), new Formula[]{randIndConst()});
    }

    int r = int(random(100));
    if ((r -= probNOT) < 0)
      return generate(operators - 1).negate();

    if ((r -= probFORALL) < 0)
      return ifPossibleQuantifyElseNegate(generate(operators - 1), opFORALL);

    if ((r -= probEXISTS) < 0)
      return ifPossibleQuantifyElseNegate(generate(operators - 1), opEXISTS);

    Operator op;
    int pivot = int(random(operators - 1));
    Formula[] sf = new Formula[]{generate(pivot), generate(operators - 1 - pivot)};
    if ((r -= probAND) < 0) op = opAND;
    else if ((r -= probOR) < 0) op = opOR;
    else if ((r -= probIFTHEN) < 0) op = opIFTHEN;
    else if ((r -= probIF) < 0) op = opIF;
    else if ((r -= probIFF) < 0) op = opIFF;
    else if ((r -= probXOR) < 0) op = opXOR;
    else if ((r -= probNAND) < 0) op = opNAND;
    else op = opNOR;
    return new Formula(op, -1, sf);
  }

}

class FormulaBox extends Button
{
  Formula f;
  boolean deletable;

  FormulaBox(Formula f, boolean brackets, boolean deletable, float x, int y)
  {
    super(brackets ? formatFormula(f, true) : formatFormula(f), x, 15 - 2.5 * y, 1, 2);
    this.f = f;
    this.deletable = deletable;
    super.w = max(2, ceil(myTextWidth(label) + 0.5));
  }

  void reset(float x, int y)
  {
    super.x = x;
    super.y = 15 - 2.5 * y;
  }
}

class SelectSentenceCore extends State
{

  final float maxX = 17;

  State old;
  Formula fo;
  Operator op;
  Button buttonDone = new Button(translations.getString("buttonOK"), 16 - 3.5, 12.5, 7, 2);
  Formula f;
  List<Button[]> buttons = new ArrayList<Button[]>();
  Map<Button, Integer> value = new HashMap<Button, Integer>();
  int[] selected;

  List<Integer> makeList(int n)
  {
    List<Integer> l = new ArrayList<Integer>();
    for (int i = 0; i != n; ++i)
      l.add(i);

    return l;
  }

  void addButtons(Operator op, List<Integer> l)
  {
    Button[] fbs;
    fbs = new Button[l.size()];
    for (int i = 0; i != fbs.length; ++i)
      value.put(fbs[i] = new Button(formatMark(op, l.get(i)), 16 - 0.5 * (2.5 * fbs.length - 0.5) + 2.5 * i, 10.0 - 2.5 * buttons.size(), 2, 2), l.get(i));

    buttons.add(fbs);
  }

  SelectSentenceCore(State old, Formula fo, Operator op)
  {
    this.old = old;
    this.fo = fo;
    this.op = op;

    if (op == opFORALL || op == opEXISTS || op == opIOTA)
    {
      addButtons(opIndConst, fo.allTerminals(opIndConst));
      addButtons(opIndVar, makeList(numMarks(opIndVar)));
      List<Integer> l = fo.allTerminals(opIndVar);
      for (int i: l)
        if (i < buttons.get(1).length)
          buttons.get(1)[i].enabled = false;
    }
    else
    {
      addButtons(opIndConst, makeList(numMarks(opIndConst)));
      if (op == opIdentity || op == opRelation)
        addButtons(opIndConst, makeList(numMarks(opIndConst)));

      if (op == opPredicate || op == opRelation)
        addButtons(op, makeList(numMarks(op)));
    }

    selected = new int[buttons.size()];
    for (int i = 0; i != selected.length; ++i)
    {
      int j = 0;
      while (j < buttons.get(i).length && !buttons.get(i)[j].enabled)
        j++;

      selected[i] = j;
      buttons.get(i)[selected[i]].mark = true;
    }

    updateFormula();

    for (Button[] bs: buttons)
      for (Button b: bs)
        super.buttons.add(b);

    super.buttons.add(buttonDone);
  }

  void select(int i, int j)
  {
    buttons.get(i)[selected[i]].mark = false;
    selected[i] = j;
    buttons.get(i)[selected[i]].mark = true;
  }

  void updateFormula()
  {
    if (op == opIdentity)
    {
      f = new Formula(op, -1, new Formula[]{
        new Formula(opIndConst, value.get(buttons.get(1)[selected[1]])),
        new Formula(opIndConst, value.get(buttons.get(0)[selected[0]]))
      });
    }
    else if (op == opPredicate)
    {
      f = new Formula(op, selected[1], new Formula[]{new Formula(opIndConst, value.get(buttons.get(0)[selected[0]]))});
    }
    else if (op == opRelation)
    {
      f = new Formula(op, selected[2], new Formula[]{
        new Formula(opIndConst, value.get(buttons.get(1)[selected[1]])),
        new Formula(opIndConst, value.get(buttons.get(0)[selected[0]]))
      });
    }
    else
    {
      Formula v = new Formula(opIndVar, value.get(buttons.get(1)[selected[1]]));
      f = new Formula(op, -1, new Formula[]{
        v, new Formula(fo, new Formula(opIndConst, value.get(buttons.get(0)[selected[0]])), v)
      });
    }
  }

  void paint()
  {
    old.paint();
    noStroke();
    fill(WHITE, 191);
    rect(0, 0, 32, 17);

    fill(LTGRAY);
    rect(15.5 - 0.5 * maxX, 10.5 - 2.5 * buttons.size(), maxX + 1, 4.5 + 2.5 * buttons.size(), 1);

    fill(BLACK);
    textFont(nFont);
    textAlign(CENTER);
    if (f != null)
      myText(formatFormula(f, true), 16, 13.5 - 2.5 * (1 + buttons.size()));

    super.paint();
  }

  State click(float x, float y)
  {
    Button b = find(x, y);
    if (b == buttonDone)
      return old;

    for (int i = 0; i != buttons.size(); ++i)
      for (int j = 0; j != buttons.get(i).length; ++j)
        if (b == buttons.get(i)[j])
          select(i, j);

    updateFormula();
    redraw();

    return this;
  }

}

class SelectSentence extends SelectSentenceCore
{
  FormulaEditor fe;
  SelectSentence(FormulaEditor fe, Operator op)
  {
    super(fe, fe.get(), op);
    this.fe = fe;
  }
  State click(float x, float y)
  {
    State r = super.click(x, y);
    if (r == fe)
      fe.addFormula(f, true);

    return r;
  }
}

class FormulaEditor extends State
{

  State old;
  boolean propositional;
  boolean brackets;

  Button buttonDone   = new Button(translations.getString("buttonDone"), 27.5, 15, 4.5, 2);
  Button buttonHelp   = new Button("\u2139", 25.0, 15, 2, 2);
  Button buttonDelete = new Button("\u267B", 22.5, 15, 2, 2);
  Button buttonNOT    = new Button("$-$",     0.0, 15, 2, 2);
  Button buttonAND    = new Button("$&$",     2.5, 15, 2, 2);
  Button buttonOR     = new Button("$v$",     5.0, 15, 2, 2);
  Button buttonIFTHEN = new Button("$>$",     7.5, 15, 2, 2);
  Button buttonIFF    = new Button("$#$",    10.0, 15, 2, 2);
  Button buttonXOR    = new Button("$+$",    12.5, 15, 2, 2);
  Button buttonNAND   = new Button("$^$",    15.0, 15, 2, 2);
  Button buttonNOR    = new Button("$_$",    17.5, 15, 2, 2);
  Button buttonIF     = new Button("$<$",    20.0, 15, 2, 2);
  Button buttonIdentity  = new Button("$b0=b1$",  0.0, 12.5, 2, 2);
  Button buttonPredicate = new Button("$B0b0$",   2.5, 12.5, 2, 2);
  Button buttonRelation  = new Button("$C0b0b1$", 5.0, 12.5, 2, 2);
  Button buttonFORALL    = new Button("$Ac0$",    7.5, 12.5, 2, 2);
  Button buttonEXISTS    = new Button("$Ec0$",   10.0, 12.5, 2, 2);

  float x = 0.0;
  int y = 1;
  FormulaBox curFormula = null;
  Button curOperator = null;

  void addFormula(Formula f, boolean deletable)
  {
    FormulaBox nb = new FormulaBox(f, brackets, deletable, this.x, this.y);
    if (this.x != 0 && nb.x + nb.w > 32)
    {
      this.x = 0;
      this.y++;
      nb.reset(this.x, this.y);
    }
    this.x += nb.w + 0.5;
    buttons.add(nb);
    setFormula(nb);
  }

  FormulaEditor(State old, boolean propositional, boolean exotic, boolean brackets)
  {
    this.old = old;
    this.propositional = propositional;
    this.brackets = brackets;
    buttons.add(buttonDone);
    buttons.add(buttonHelp);
    buttons.add(buttonDelete);
    buttons.add(buttonNOT);
    buttons.add(buttonAND);
    buttons.add(buttonOR);
    buttons.add(buttonIFTHEN);
    buttons.add(buttonIFF);
    if (exotic && config.getUse(opXOR))
      buttons.add(buttonXOR);

    if (exotic && config.getUse(opNAND))
      buttons.add(buttonNAND);

    if (exotic && config.getUse(opNOR))
      buttons.add(buttonNOR);

    if (exotic && config.getUse(opIF))
      buttons.add(buttonIF);

    if (propositional)
    {
      for (int i = 0; i != numMarks(opSentence); ++i)
        addFormula(new Formula(opSentence, i), false);
    }
    else
    {
      buttons.add(buttonIdentity);
      buttons.add(buttonPredicate);
      buttons.add(buttonRelation);
      buttons.add(buttonFORALL);
      buttons.add(buttonEXISTS);
      x = 12.5;
    }

    setFormula(null);
  }

  State click(float x, float y)
  {
    Button b = find(x, y);
    if (b == null)
    {
      setOperator(null);
      setFormula(null);
      redraw();
      return this;
    }

    if (b == buttonDone)
    {
      setOperator(null);
      return old;
    }

    if (b == buttonHelp)
      return new Info(this, translations.getString("syntax_edit_help_title"),
        translations.getString(propositional ? "syntax_edit_propositional_help" : "syntax_edit_predicate_help"));

    if (b == buttonDelete)
    {
      buttons.remove(curFormula);
      setOperator(null);
      setFormula(null);

      this.x = propositional ? 0 : 12.5;
      this.y = 1;
      for (Button bb: buttons)
      {
        if (bb instanceof FormulaBox)
        {
          FormulaBox fb = (FormulaBox)bb;
          if (this.x != 0 && this.x + fb.w > 32)
          {
            this.x = 0;
            ++this.y;
          }
          fb.reset(this.x, this.y);
          this.x += fb.w + 0.5;
        }
      }
    }
    else if (b == buttonNOT || b == buttonAND || b == buttonOR || b == buttonIFTHEN ||
      b == buttonIFF || b == buttonXOR || b == buttonNAND || b == buttonNOR)
    {
      if (b == buttonNOT)
      {
        setOperator(b);
        setFormula(null);
      }
      else if (curFormula != null)
        setOperator(b);
      else
        soundDing.play();
    }
    else if (b == buttonFORALL)
    {
      try
      {
        return new SelectSentence(this, opFORALL);
      }
      catch (Exception e)
      {
        soundDing.play();
        return this;
      }
    }
    else if (b == buttonEXISTS)
    {
      try
      {
        return new SelectSentence(this, opEXISTS);
      }
      catch (Exception e)
      {
        soundDing.play();
        return this;
      }
    }
    else if (b == buttonIdentity)
    {
      return new SelectSentence(this, opIdentity);
    }
    else if (b == buttonPredicate)
    {
      return new SelectSentence(this, opPredicate);
    }
    else if (b == buttonRelation)
    {
      return new SelectSentence(this, opRelation);
    }
    else
    {
      FormulaBox fb = (FormulaBox)b;
      if (curOperator == null)
      {
        setFormula(fb);
      }
      else
      {
        if (curOperator == buttonNOT)
        {
          addFormula(fb.f.negate(), true);
        }
        else
        {
          Operator[] ops = {opAND, opOR, opIFTHEN, opIF, opIFF, opXOR, opNAND, opNOR};
          for (Operator o: ops)
            if (curOperator.label.equals("$" + o.transition + "$"))
              addFormula(new Formula(o, -1, new Formula[]{curFormula.f, fb.f}), true);
        }
        setOperator(null);
      }
    }

    redraw();
    return this;
  }

  void setOperator(Button b)
  {
    if (curOperator != null)
      curOperator.mark = false;

    curOperator = b;
    buttonIdentity.enabled = buttonPredicate.enabled = buttonRelation.enabled = curOperator == null;

    if (curOperator != null)
      curOperator.mark = true;
  }

  void setFormula(FormulaBox f)
  {
    if (curFormula != null)
      curFormula.mark = false;

    curFormula = f;
    buttonDelete.enabled = curFormula != null && curFormula.deletable;
    buttonAND.enabled = buttonOR.enabled = buttonIFTHEN.enabled = buttonIF.enabled =
      buttonIFF.enabled = buttonXOR.enabled = buttonNAND.enabled = buttonNOR.enabled = curFormula != null;
    buttonFORALL.enabled = buttonEXISTS.enabled = curFormula != null && curFormula.f.allTerminals(opIndVar).size() < 3 && !curFormula.f.allTerminals(opIndConst).isEmpty();

    if (curFormula != null)
      curFormula.mark = true;
  }

  Formula get()
  {
    return curFormula != null ? curFormula.f : null;
  }

}

class SyntaxTask
{
  String task;
  boolean brackets;
  Formula[] solutions;
  SyntaxTask(String task, boolean brackets, Formula[] solutions)
  {
    this.task = task;
    this.brackets = brackets;
    this.solutions = solutions;
  }

  boolean check(Formula f)
  {
    boolean correct = false;
    for (Formula s: solutions)
      correct |= formatFormula(f, brackets).equals(formatFormula(s, brackets));

    return correct;
  }
}

class Syntax extends FormulaEditor
{

  SyntaxTask[] tasks;
  boolean solutions;
  Progress p;

  Syntax(State old, String id, boolean propositional, boolean exotic, SyntaxTask[] tasks, boolean brackets, boolean solutions)
  {
    super(old, propositional, exotic, brackets);
    this.tasks = tasks;
    this.solutions = solutions;
    p = new Progress(id, tasks.length);
  }

  void paint()
  {
    fill(LTGRAY);
    rect(0, 0, 32, 0.5);
    fill(BLUE);
    rect(0, 0, (32.0 * p.numCorrect()) / p.done.length, 0.5);

    textFont(nFont);
    textAlign(LEFT);
    fill(BLACK);
    textWriter(tasks[p.cur].task + (p.checking ? !p.done[p.cur] ? " <<par>> Richtig w\u00E4re: '" + formatFormula(tasks[p.cur].solutions[0], brackets) + "'": " <<par>> Richtig!" : ""), 32, 1);

    super.paint();
  }

  State click(float x, float y)
  {
    if (!p.checking)
    {
      State s = super.click(x, y);
      if (s != old)
        return s;

      if (get() == null)
      {
        soundDing.play();
        return new Message(this, "Achtung", "Bitte Formel ausw\u00E4hlen.", new String[]{"OK", "Abbruch"});
      }
      else
      {
        p.set(tasks[p.cur].check(get()));
      }
    }
    else if (find(x, y) == buttonDone)
    {
      int c = p.cur;
      p.next();
      if (p.done[p.cur])
        return old;

      if (c != p.cur)
      {
        this.x = propositional ? 2.5 * numMarks(opSentence) : 12.5;
        this.y = 1;
        setFormula(null);
        for (int j = buttons.size(); j > 0; --j)
          if (buttons.get(j-1) instanceof FormulaBox && ((FormulaBox)buttons.get(j-1)).deletable)
            buttons.remove(j-1);
      }
    }

    redraw();
    return this;
  }

  State feedback(int x)
  {
    if (x == 1)
      return old;

    return this;
  }

}

class ProblemSyntax extends Problem
{
  boolean propositional;
  boolean exotic;
  SyntaxTask[] tasks;
  boolean brackets;
  boolean solutions;

  ProblemSyntax(String id, String[] depends, String label, String infos[], boolean propositional, boolean exotic, SyntaxTask[] tasks, boolean brackets, boolean solutions)
  {
    super(id, depends, label, infos);
    this.propositional = propositional;
    this.exotic = exotic;
    this.tasks = tasks;
    this.brackets = brackets;
    this.solutions = solutions;
  }

  State run(State old)
  {
    return new Syntax(old, id, propositional, exotic, tasks, brackets, solutions);
  }
}

class ProblemSyntaxIntro extends Problem
{
  boolean propositional;
  String[] formulas;
  boolean brackets;

  ProblemSyntaxIntro(String id, String[] depends, String label, String infos[], boolean propositional, String[] formulas, boolean brackets)
  {
    super(id, depends, label, infos);
    this.propositional = propositional;
    this.formulas = formulas;
    this.brackets = brackets;
  }

  State run(State old)
  {
    RandomFormula r = new RandomFormula(5, propositional, false);
    Formula[] f = new Formula[formulas.length + 9];
    for (int i = 0; i != formulas.length; ++i)
      f[i] = new Formula(formulas[i]);

    for (int i = 0; i != 9; ++i)
    {
      Formula sf;
      do
      {
        sf = r.generate(2 + i/3);
      } while (!brackets && countBracketPrecedence(sf) < 1 + i/5);
      f[formulas.length + i] = sf;
    }
  
    SyntaxTask[] tasks = new SyntaxTask[f.length];
    for (int i = 0; i != f.length; ++i)
      tasks[i] = new SyntaxTask((brackets ? "Erzeuge die Formel '" : "Erzeuge eine Formel äquivalent zu '")+ formatFormula(f[i], brackets) + "' schrittweise. W\u00E4hle '\u2139' f\u00FCr Hilfe.", brackets, new Formula[]{f[i]});

    return new Syntax(old, id, propositional, false, tasks, true, false);
  }
}

class ProblemSyntaxRandom extends Problem
{
  boolean propositional;
  boolean brackets;
  Range[] ranges;

  ProblemSyntaxRandom(String[] depends, String label, boolean propositional, boolean brackets)
  {
    super("", depends, label, new String[]{"Zuf\u00E4llige Formel nachbauen (optional)"});
    this.propositional = propositional;
    this.brackets = brackets;
    ranges = new Range[]{new Range("Operatoren", 2, 12, 8), new Range(propositional ? "Satzbuchstaben" : "Individuenkonst.", 1, 5, 3)};
  }

  State run(State old)
  {
    return new ParamSelect(old, ranges){
      public State click(float x, float y){
        State s = super.click(x, y);
        if (s == old)
        {
          Formula f;
          do
          {
            f = make(propositional, true, min(ranges[1].value, ranges[0].value + 1), ranges[0].value);
          } while (!brackets && countBracketPrecedence(f) < ranges[0].value/2);

          return new Syntax(old, "", propositional, true, new SyntaxTask[]{
            new SyntaxTask("Erzeuge die Formel '" + formatFormula(f, brackets) +
              "' schrittweise. W\u00E4hle '\u2139' f\u00FCr Hilfe.", brackets, new Formula[]{f})}, true, false);
        }

        return s;
      }
    };
  }
}
