/*
 * A Game of Logic (not to be confused with the book "The Game of Logic" by Lewis Caroll)
 * Copyright (C) 2018-2019  Christian Heine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

class TruthTable
{

  int numSentences, numLines;
  Formula[] order;
  Map<Integer, Integer> scolumn;
  Map<Formula, Integer> column;
  boolean[] values;
  
  int getColumn(Formula f)
  {
    if (f.op == opSentence)
      return scolumn.get(f.value);
    else
      return column.get(f);
  }

  void parse(Formula f, Set<Integer> sentences, List<Formula> order)
  {
    if (f.op == opSentence)
    {
      if (!sentences.contains(f.value))
        sentences.add(f.value);
    }
    else
    {
      for (Formula sf: f.subformulas)
        parse(sf, sentences, order);

      order.add(f);
    }
  }

  void initOrder(Formula f)
  {
    List<Formula> o = new ArrayList<Formula>();
    Set<Integer> s = new java.util.HashSet<Integer>();
    parse(f, s, o);

    order = new Formula[s.size() + o.size()];

    int n = 0;
    for (Integer e: s)
      order[n++] = new Formula(opSentence, e);

    numSentences = n;

    for (Formula e: o)
      order[n++] = e;

    for (int i = 0; i != numSentences; ++i)
    {
      for (int j = i; j != 0 && order[j].value < order[j-1].value; --j)
      {
        Formula x = order[j];
        order[j] = order[j-1];
        order[j-1] = x;
      }
    }

    scolumn = new HashMap<Integer, Integer>();
    for (int i = 0; i != numSentences; ++i)
      scolumn.put(order[i].value, i);

    column = new HashMap<Formula, Integer>();
    for (int i = numSentences; i != order.length; ++i)
      column.put(order[i], i);
  }

  void fill()
  {
    for (int i = numSentences; i != order.length; ++i)
    {
      Formula x = order[i];
      int base = i*numLines;
      int inputs[] = new int[x.subformulas.length];
      for (int j = 0; j != x.subformulas.length; ++j)
        inputs[j] = numLines*getColumn(x.get(j));

      for (int j = 0; j != numLines; ++j)
      {
        if (x.op == opNOT)
        {
          values[base + j] = !values[inputs[0] + j];
        }
        else if (x.op == opAND)
        {
          values[base + j] = values[inputs[0] + j] && values[inputs[1] + j];
        }
        else if (x.op == opOR)
        {
          values[base + j] = values[inputs[0] + j] || values[inputs[1] + j];
        }
        else if (x.op == opIFTHEN)
        {
          values[base + j] = !values[inputs[0] + j] || values[inputs[1] + j];
        }
        else if (x.op == opIFF)
        {
          values[base + j] = values[inputs[0] + j] == values[inputs[1] + j];
        }
        else if (x.op == opXOR)
        {
          values[base + j] = values[inputs[0] + j] != values[inputs[1] + j];
        }
        else if (x.op == opNAND)
        {
          values[base + j] = !values[inputs[0] + j] || !values[inputs[1] + j];
        }
        else if (x.op == opNOR)
        {
          values[base + j] = !values[inputs[0] + j] && !values[inputs[1] + j];
        }
      }
    }
  }

  TruthTable(Formula f)
  {
    initOrder(f);
    
    numLines = 1<<numSentences;
    values = new boolean[order.length * numLines];
    for (int i = 0; i != numSentences; ++i)
      for (int j = 0; j != numLines; ++j)
        values[i*numLines+j] = (j>>(numSentences-1-i))%2 == 0;

    fill();
  }

  TruthTable(Formula f, Set<Integer> interp)
  {
    initOrder(f);
    
    numLines = 1;
    values = new boolean[order.length];
    for (int i = 0; i != numSentences; ++i)
      values[i] = interp.contains(i);

    fill();
  }
}

class TruthTableTest extends State
{

  /*
   * difficulty
   * 0
   * 1 no guide
   * 2 no hints
   * 3 no automatic checks
   * 4 no automatic jumping
   * 5 no highlights
   * 6 no filled
   */

  State old;
  Formula f;
  int difficulty;
  String id, lesson;
  TruthTable t;
  float[] col;
  List<String> marks = new ArrayList<String>();
  List<Float> markpos = new ArrayList<Float>();
  float x, separator;
  char[] status;
  int curcol, curline;

  void fillCol(Formula f)
  {
    if (f.op == opSentence)
    {
      String s = formatFormula(f, true);
      marks.add(s);
      markpos.add(x);
      x += myTextWidth(s);
    }
    else
    {
      String s = config.get(f.op);
      float d = myTextWidth(s);
      if (f.op == opNOT)
      {
        x += myTextWidth(" ");
        col[t.getColumn(f)] = x + 0.5 * d;
        marks.add(s);
        markpos.add(x);
        x += d;
        fillCol(f.get(0));
      }
      else
      {
        marks.add("(");
        markpos.add(x);
        x += myTextWidth("(");
        fillCol(f.get(0));
        marks.add(s);
        markpos.add(x);
        col[t.getColumn(f)] = x + 0.5 * d;
        x += d;
        fillCol(f.get(1));
        marks.add(")");
        markpos.add(x);
        x += myTextWidth(")");
      }
    }
  }

  TruthTableTest(State old, Formula f, int difficulty, String id, String lesson)
  {
    this.old = old;
    this.f = f;
    this.difficulty = difficulty;
    this.id = id;
    this.lesson = lesson;
    t = new TruthTable(f);

    textFont(nFont);
    textAlign(LEFT);

    col = new float[t.order.length];
    x = 0.0;
    for (int i = 0; i != t.numSentences; ++i)
    {
      x += myTextWidth(" ");
      String s = formatFormula(t.order[i], true);
      float d = myTextWidth(s);
      marks.add(s);
      markpos.add(x);
      col[i] = x + 0.5 * d;
      x += d + myTextWidth(" ");
    }
    x += myTextWidth(" ");
    separator = x;
    x += myTextWidth("  ");
    fillCol(f);
    x += myTextWidth(" ");

    status = new char[t.order.length * t.numLines];
    for (int i = 0; i != status.length; ++i)
      status[i] = '-';

    curcol = t.order.length;
    if (difficulty < 6)
    {
      for (int i = 0; i != t.numSentences * t.numLines; ++i)
        status[i] = markTruthValue(t.values[i]);

      if (difficulty < 4)
        curcol = t.numSentences;
    }

    buttons.add(new Button(config.get(opTrue),  25.0, 0, 2, 2));
    buttons.add(new Button(config.get(opFalse), 27.5, 0, 2, 2));
    buttons.add(new Button("Fertig", 25, 15, 7, 2));
    buttons.add(new Button("Tipp", 30, 0, 2, 2));
    if (difficulty >= 2)
      buttons.get(3).enabled = false;
  }

  void paint()
  {
    noStroke();
    if (curcol < col.length)
    {
      fill(LTRED);
      rect(col[curcol] - 0.5, 1, 1.0, t.numLines);
      fill(RED);
      rect(col[curcol] - 0.5, 1 + curline, 1.0, 1);

      if (difficulty < 5)
      {
        Formula n = t.order[curcol];
        if (n.op != opSentence)
        {
          for (Formula st: n.subformulas)
          {
            fill(LTBLUE);
            rect(col[t.getColumn(st)] - 0.5, 1, 1.0, t.numLines);
            fill(BLUE);
            rect(col[t.getColumn(st)] - 0.5, 1 + curline, 1.0, 1);
          }
        }
      }
    }

    fill(BLACK);
    textFont(nFont);
    textAlign(LEFT);
    for (int i = 0; i != marks.size(); ++i)
      myText(marks.get(i), markpos.get(i), 0);

    textAlign(CENTER);
    for (int i = 0; i != t.order.length; ++i)
      for (int j = 0; j != t.numLines; ++j)
        myText("" + status[i * t.numLines + j], col[i], 1 + j);

    textAlign(LEFT);
    if (difficulty < 1 && t.numLines < 14 && curcol < col.length)
      textWriter(guides.get(t.order[curcol].op), 24.5, 15);

    stroke(0);
    strokeCap(SQUARE);
    strokeWeight(strokeWeightH);
    line(0, 1, x, 1);
    strokeWeight(strokeWeightV);
    line(separator, 0, separator, 1 + t.numLines);

    super.paint();
  }

  void enter(boolean b)
  {
    if (curcol < t.order.length)
    {
      if (difficulty < 3 && b != t.values[curcol * t.numLines + curline])
      {
        soundDing.play();
        return;
      }
      
      status[curcol * t.numLines + curline] = markTruthValue(b);
      if (++curline == t.numLines)
      {
        curline = 0;
        curcol = difficulty < 4 ? curcol + 1 : t.order.length;
      }
      redraw();
    }
  }

  String check()
  {
    int missing = 0, error = 0;
    for (int i = 0; i != status.length; ++i)
    {
      if (status[i] != config.get(opTrue).charAt(0) && status[i] != config.get(opFalse).charAt(0))
        missing++;
      else if (status[i] != markTruthValue(t.values[i]))
        error++;
    }

    return missing == 0 && error == 0 ? "" : "Es gibt <<red>> <<emph>> " + error + " <<normal>> Fehler und <<blue>> <<emph>> " + missing + " <<normal>> fehlende Werte.";
  }

  State click(float x, float y)
  {
    if (buttons.get(0).inside(x, y))
    {
      enter(true);
    }
    else if (buttons.get(1).inside(x, y))
    {
      enter(false);
    }
    else if (buttons.get(3).inside(x, y))
    {
      if (curcol < t.order.length)
        enter(t.values[curcol * t.numLines + curline]);
    }
    else if (buttons.get(2).inside(x, y))
    {
      String c = check();

      if (c.equals(""))
      {
        if (!id.equals(""))
          config.setSolved(id);

        soundWin.play();
        return new Message(this, "Richtig", lesson, new String[]{"zur Aufgabe", "zum Men\u00FC"});
      }
      else
      {
        soundDing.play();
        return new Message(this, "Nicht ganz richtig", c, new String[]{"zur Aufgabe", "zum Men\u00FC", "Korrektur"});
      }
    }
    else if (difficulty >= 3)
    {
      for (int i = 0; i < col.length; ++i)
      {
        if (abs(x - col[i]) <= 0.5)
        {
          int n = int(y) - 1;
          if (0 <= n && n < t.numLines)
          {
            curcol = i;
            curline = n;
            redraw();
          }
        }
      }
    }

    return this;
  }

  State key(char code)
  {
    if (code == 'w' || code == 'W' || code == 't' || code == 'T' || code == '1' || code == 'l')
      enter(true);
    else if(code == 'f' || code == 'F' || code == '0')
      enter(false);
    else
      soundDing.play();

    return this;
  }

  State feedback(int x)
  {
    if (x == 1)
      return old;

    if (x == 2)
    {
      for (int i = 0; i != t.order.length; ++i)
      {
        Formula f = t.order[i];
        List<Integer> inputs = new ArrayList<Integer>();
        for (Formula sf: f.subformulas)
          inputs.add(t.getColumn(sf));

        for (int j = 0; j != t.numLines; ++j)
        {
          for (Integer input: inputs)
            if (status[input * t.numLines + j] == '-')
              status[i * t.numLines + j] = '-';

          if (status[i * t.numLines + j] != markTruthValue(t.values[i * t.numLines + j]))
            status[i * t.numLines + j] = '-';
        }
      }
    }
    return this;
  }
}

class TruthTableType extends TruthTableTest
{
  Button buttonTrue  = new Button("tautologisch", 25, 7.5, 7, 2);
  Button buttonCont  = new Button("kontingent", 25, 10.0, 7, 2);
  Button buttonFalse = new Button("kontradiktorisch", 25, 12.5, 7, 2);
  TruthTableType(State old, Formula f, int difficulty, String id, String lesson)
  {
    super(old, f, difficulty, id, lesson);
    buttons.add(buttonTrue);
    buttons.add(buttonCont);
    buttons.add(buttonFalse);
  }
  void paint()
  {
    textAlign(CENTER);
    textFont(nFont);
    fill(BLACK);
    noStroke();
    myText("logischer Status?", 28.5, 6.0);
    super.paint();
  }
  String check()
  {
    String c = super.check();
    if (!c.equals(""))
      return c;

    int ts = 0, fs = 0;
    for (int i = 0; i != t.numLines; ++i)
    {
      if (t.values[(col.length - 1) * t.numLines + i])
        ts++;
      else
        fs++;
    }

    if (buttonTrue.mark != (ts == t.numLines) || buttonFalse.mark != (fs == t.numLines) || buttonCont.mark != (ts > 0 && fs > 0))
      return "Typ der Formel ist falsch.";

    return "";
  }
  State click(float x, float y)
  {
    if (buttonTrue.inside(x, y))
    {
      buttonTrue.mark = !(buttonFalse.mark = buttonCont.mark = false);
    }
    else if (buttonFalse.inside(x, y))
    {
      buttonFalse.mark = !(buttonTrue.mark = buttonCont.mark = false);
    }
    else if (buttonCont.inside(x, y))
    {
      buttonCont.mark = !(buttonFalse.mark = buttonTrue.mark = false);
    }
    else
    {
      return super.click(x, y);
    }
    redraw();
    return this;
  }
}

class ProblemTruthTable extends Problem
{
  Formula f;
  int difficulty;
  String lesson;

  ProblemTruthTable(String id, String[] depends, String label, String[] infos, Formula f, int difficulty, String lesson)
  {
    super(id, depends, label, infos.length != 0 ? infos : new String[]{formatFormula(f, true)});
    this.f = f;
    this.difficulty = difficulty;
    this.lesson = lesson;
  }

  State run(State old)
  {
    return new TruthTableTest(old, f, difficulty, id, lesson);
  }
}

class ProblemTruthTableType extends Problem
{
  Formula f;
  int difficulty;
  String lesson;

  ProblemTruthTableType(String id, String[] depends, String label, String[] infos, Formula f, int difficulty, String lesson)
  {
    super(id, depends, label, infos.length != 0 ? infos : new String[]{formatFormula(f, true)});
    this.f = f;
    this.difficulty = difficulty;
    this.lesson = lesson;
  }

  State run(State old)
  {
    return new TruthTableType(old, f, difficulty, id, lesson);
  }
}

class ProblemTruthTableRandom extends Problem
{
  Range[] ranges = {new Range("Schwierigkeit", 1, 6, 5), new Range("Operatoren", 1, 12, 6), new Range("Satzbuchstaben", 1, 4, 3)};

  ProblemTruthTableRandom()
  {
    super("", new String[]{"sem_truth_table_complex"}, "Z", new String[]{"Zuf\u00E4llige Formel mit Wahrheitswerttabelle l\u00F6sen"});
  }

  State run(State old)
  {
    return new ParamSelect(old, ranges){
      public State click(float x, float y){
        State s = super.click(x, y);
        if (s == old)
          return new TruthTableTest(old, make(true, true, ranges[2].value, max(ranges[2].value - 1, ranges[1].value)), ranges[0].value, "", "");

        return s;
      }
    };
  }
}

class FormulaEditorTruthTable extends FormulaEditor
{
  FormulaEditorTruthTable(State old)
  {
    super(old, true, true, false);
  }
  State click(float x, float y)
  {
    State s = super.click(x, y);
    if (s == old && get() != null)
      return new TruthTableTest(old, get(), 6, "", "");

    return s;
  }
}

class ProblemTruthTableSelect extends Problem
{
  FormulaEditorTruthTable fe = new FormulaEditorTruthTable(null);

  ProblemTruthTableSelect()
  {
    super("", new String[]{"sem_truth_table_complex"}, "W", new String[]{"Formel f\u00FCr Wahrheitswerttabelle w\u00E4hlen (maximaler Schwierigkeitsgrad)"});
  }

  State run(State old)
  {
    fe.old = old;
    return fe;
  }
}

class ProblemTruthTableTypeRandom extends Problem
{
  Range[] ranges = {new Range("Schwierigkeit", 1, 6, 5), new Range("Operatoren", 1, 12, 6), new Range("Satzbuchstaben", 1, 4, 3)};

  ProblemTruthTableTypeRandom()
  {
    super("", new String[]{"log_prop_truth_tables"}, "Z", new String[]{"Zuf\u00E4llige Formel mit Wahrheitswerttabelle l\u00F6sen"});
  }

  State run(State old)
  {
    return new ParamSelect(old, ranges){
      public State click(float x, float y){
        State s = super.click(x, y);
        if (s == old)
          return new TruthTableType(old, make(true, true, ranges[2].value, max(ranges[2].value - 1, ranges[1].value)), ranges[0].value, "", "");

        return s;
      }
    };
  }
}

class FormulaEditorTruthTableType extends FormulaEditor
{
  FormulaEditorTruthTableType(State old)
  {
    super(old, true, true, false);
  }
  State click(float x, float y)
  {
    State s = super.click(x, y);
    if (s == old && get() != null)
      return new TruthTableType(old, get(), 6, "", "");

    return s;
  }
}

class ProblemTruthTableTypeSelect extends Problem
{
  FormulaEditorTruthTableType fe = new FormulaEditorTruthTableType(null);

  ProblemTruthTableTypeSelect()
  {
    super("", new String[]{"log_prop_truth_tables"}, "W", new String[]{"Formel f\u00FCr Wahrheitswerttabelle w\u00E4hlen (maximaler Schwierigkeitsgrad)"});
  }

  State run(State old)
  {
    fe.old = old;
    return fe;
  }
}

static final String[] truthTableTypeProblems = {"p0vp1", "&p0vp1-p0", "vp0vp1-p0", ">&>p0p1p1p0"};

Problem[][] makeProblemsTruthTable()
{
  JSONArray pi = loadJSONArray("problems_propositional_implications.json");
  JSONArray pe = loadJSONArray("problems_propositional_equivalences.json");
  Problem[][] r = new Problem[pi.size() + pe.size() + 2][];
  int n = 0;
  r[n++] = new Problem[]{
    new ProblemInfo("log_prop", new String[]{"sem_models"}, "Logische Eigenschaften", toArray(translations.getJSONArray("log_prop"))),
    new ProblemInfo("log_relation", new String[]{"log_prop"}, "Logische Beziehungen", toArray(translations.getJSONArray("log_relation")))
  };

  r[n++] = new Problem[]{
    new ProblemInfo("log_prop_truth_table0", new String[]{"sem_truth_table_complex", "log_prop"},
      "Wahrheitswerttabellen", toArray(translations.getJSONArray("log_prop_truth_tables"))),
    new ProblemTruthTableType("log_prop_truth_table1", new String[]{"log_prop_truth_table0"}, "(1)", new String[]{}, new Formula("vp0p1"), 4, ""),
    new ProblemTruthTableType("log_prop_truth_table2", new String[]{"log_prop_truth_table0"}, "(2)", new String[]{}, new Formula("&p0v-p1-p0"), 4, ""),
    new ProblemTruthTableType("log_prop_truth_table3", new String[]{"log_prop_truth_table0"}, "(3)", new String[]{}, new Formula("vp0v-p1-p0"), 4, ""),
    new ProblemTruthTableType("log_prop_truth_tables", new String[]{"log_prop_truth_table1", "log_prop_truth_table2", "log_prop_truth_table3"}, "(4)", new String[]{}, new Formula(">&>p0p1p1p0"), 4, ""),
    new ProblemTruthTableTypeRandom(),
    new ProblemTruthTableTypeSelect()
};

  for (int i = 0; i != pi.size(); ++i)
  {
    JSONArray s = pi.getJSONArray(i);
    r[n] = new Problem[s.size()];
    for (int j = 0; j != s.size(); ++j)
    {
      JSONObject o = s.getJSONObject(j);
      String id = "truth_table_propositional_implication_" + o.getString("id");
      String[] depends = toArray(o.getJSONArray("depends"));
      for (int k = 0; k != depends.length; ++k)
        depends[k] = "truth_table_propositional_implication_" + depends[k];

      String[] premises = toArray(o.getJSONArray("premises"));
      String target = o.getString("target");

      Formula f;
      if (premises.length == 0)
      {
        f = new Formula(target);
      }
      else
      {
        f = new Formula(premises[0]);
        for (int k = 1; k != premises.length; ++k)
          f = new Formula(opAND, -1, new Formula[]{f, new Formula(premises[k])});

        f = new Formula(opIFTHEN, -1, new Formula[]{f, new Formula(target)});
      }
      r[n][j] = new ProblemTruthTableType(id, prepend("log_prop_truth_tables", depends), "" + char('A' + n - 2) + char ('1' + j), new String[]{}, f, o.getInt("difficulty", 4), o.getString("lesson", ""));
    }
    ++n;
  }
  for (int i = 0; i != pe.size(); ++i)
  {
    JSONArray s = pe.getJSONArray(i);
    r[n] = new Problem[s.size()];
    for (int j = 0; j != s.size(); ++j)
    {
      JSONObject o = s.getJSONObject(j);
      String id = "truth_table_propositional_equivalence_" + o.getString("id");
      String[] depends = toArray(o.getJSONArray("depends"));
      for (int k = 0; k != depends.length; ++k)
        depends[k] = "truth_table_propositional_equivalence_" + depends[k];

      String[] formulas = toArray(o.getJSONArray("formulas"));
      Formula f = new Formula(opIFF, -1, new Formula[]{new Formula(formulas[0]), new Formula(formulas[1])});
      r[n][j] = new ProblemTruthTableType(id, prepend("log_prop_truth_tables", depends), "" + char('A' + n - 2) + char ('1' + j), new String[]{}, f, o.getInt("difficulty", 4), o.getString("lesson", ""));
    }
    ++n;
  }
  return r;
}
