/*
 * A Game of Logic (not to be confused with the book "The Game of Logic" by Lewis Caroll)
 * Copyright (C) 2018-2019  Christian Heine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

static Formula invert(Formula f)
{
  return f.op == opNOT ? f.get(0) : f.negate();
}

// give type of formula for tableaux
static String tableauType(Formula f)
{
  if (f.op == opSentence)
    return "P";

  if (f.op == opNOT)
  {
    Formula sf = f.get(0);
    if (sf.op == opNOT)
      return "DN";
    else
      return "N" + tableauType(sf);
  }

  if (f.op == opAND)
    return "K";

  if (f.op == opOR)
    return "D";

  if (f.op == opIFTHEN)
    return "I";

  if (f.op == opIF)
    return "C";

  if (f.op == opIFF)
    return "B";

  if (f.op == opXOR)
    return "X";

  if (f.op == opNAND)
    return "S";

  if (f.op == opNOR)
    return "W";

  if (f.op == opFORALL)
    return "A";

  if (f.op == opEXISTS)
    return "E";

  return "";
}

class TruthTree extends State
{

  class Node
  {
    Formula formula;
    Node parent;
    List<Node> children = new ArrayList<Node>();
    Node closed = null;
    int closing = 0;
    int step = 0, made = 0;
    String rule = "";
    float x = 0, w = 32;

    Node(Formula formula, Node parent)
    {
      this.formula = formula;
      this.parent = parent;

      if (parent != null)
        parent.children.add(this);
    }

    void extend(Formula[][] formulas, int made)
    {
      if (closed != null)
        return;

      if (children.isEmpty())
      {
        float x = this.x;
        float w = this.w / formulas.length;
        for (Formula[] fs: formulas)
        {
          Node n = this;
          for (Formula f: fs)
          {
            n = new Node(f, n);
            n.made = made;
            n.x = x;
            n.w = w;
          }
          x += w;
        }
      }
      else
      {
        for (Node n: children)
          n.extend(formulas, made);
      }
    }

    boolean execute(int n, String type)
    {
      if (type.equals("S") || type.equals("NS"))
        return false;

      Formula sf = formula;
      Formula[][] f = null;

      if (sf.op == opNOT)
        sf = sf.get(0);

      if (sf.subformulas.length == 1)
      {
        if (type.equals("DN"))
          f = new Formula[][]{{sf.get(0)}};
      }
      else if (sf.isQuantification())
      {
        if (type.equals("E") || type.equals("NA"))
        {
          Set<Integer> l = new TreeSet<Integer>();
          for (Node node = parent; node != null; node = node.parent)
            node.formula.allTerminals(opIndConst, l);

          moreTerminals(l);
          f = new Formula[][]{{new Formula(sf.get(1), sf.get(0), new Formula(opIndConst, l.size()))}};
          if (type.equals("NA"))
            f[0][0] = f[0][0].negate();
        }
        else if (type.equals("A") || type.equals("NE"))
        {
          Set<Integer> l = new TreeSet<Integer>();
          for (Node node = parent; node != null; node = node.parent)
            node.formula.allTerminals(opIndConst, l);

          moreTerminals(l);
          if (l.isEmpty())
            l.add(0);

          f = new Formula[1][l.size()];
          for (int i = 0; i != l.size(); ++i)
          {
            f[0][i] = new Formula(sf.get(1), sf.get(0), new Formula(opIndConst, i));
            if (type.equals("NE"))
              f[0][i] = f[0][i].negate();
          }
        }
      }
      else if (sf.subformulas.length == 2)
      {
        if (type.equals("K") || type.equals("NS"))
          f = new Formula[][]{{sf.get(0), sf.get(1)}};
        else if (type.equals("NK") || type.equals("S"))
          f = new Formula[][]{{sf.get(0).negate()}, {sf.get(1).negate()}};
        else if (type.equals("D") || type.equals("NW"))
          f = new Formula[][]{{sf.get(0)}, {sf.get(1)}};
        else if (type.equals("ND") || type.equals("W"))
          f = new Formula[][]{{sf.get(0).negate(), sf.get(1).negate()}};
        else if (type.equals("I"))
          f = new Formula[][]{{sf.get(0).negate()}, {sf.get(1)}};
        else if (type.equals("NI"))
          f = new Formula[][]{{sf.get(0), sf.get(1).negate()}};
        else if (type.equals("C"))
          f = new Formula[][]{{sf.get(0)}, {sf.get(1).negate()}};
        else if (type.equals("NC"))
          f = new Formula[][]{{sf.get(0).negate(), sf.get(1)}};
        else if (type.equals("B") || type.equals("NX"))
          f = new Formula[][]{{sf.get(0), sf.get(1)}, {sf.get(0).negate(), sf.get(1).negate()}};
        else if (type.equals("X") || type.equals("NB"))
          f = new Formula[][]{{sf.get(0), sf.get(1).negate()}, {sf.get(0).negate(), sf.get(1)}};
        else if (type.equals("NQ"))
          f = new Formula[][]{{new Formula(sf.op == opFORALL ? opEXISTS : opFORALL, -1, new Formula[]{sf.get(0), sf.get(1).negate()})}};
      }

      if (f != null)
      {
        extend(f, n);
        step = n;
        rule = type;
      }
      
      return f != null;
    }

    void moreTerminals(Set<Integer> l)
    {
      formula.allTerminals(opIndConst, l);
      for (Node c: children)
        c.moreTerminals(l);
    }

    void undo(int step)
    {
      if (this.step == step)
      {
        rule = "";
        if (closed != null)
          --closed.closing;
        closed = null;
        this.step = 0;
      }

      for (int i = children.size(); i > 0; --i)
        if (children.get(i-1).made == step)
          children.remove(i-1);

      for (Node c: children)
        c.undo(step);
    }

    boolean findUpwards(Node n)
    {
      for (Node nn = this; nn != null; nn = nn.parent)
        if (nn == n)
          return true;

      return false;
    }

    boolean isClosed()
    {
      return parentCloses() || subtreeCloses();
    }

    private boolean parentCloses()
    {
      for (Node n = parent; n != null; n = n.parent)
        if (n.closed != null)
          return true;

      return false;
    }

    private boolean subtreeCloses()
    {
      if (closed != null)
        return true;

      for (Node c: children)
        if (!c.isClosed())
          return false;

      return !children.isEmpty();
    }

    void incorrect(IntList l)
    {
      if (step > 0 && ((formula.isLiteral() && (!invert(formula).equals(closed.formula) || !findUpwards(closed))) || (!formula.isLiteral() && !rule.equals(tableauType(formula)))))
        l.append(step);

      for (Node c: children)
        c.incorrect(l);
    }

    Node unfinished()
    {
      if (isClosed())
        return null;

      if (formula.isLiteral())
      {
        for (Node n = parent; n != null; n = n.parent)
          if (invert(n.formula).equals(formula))
            return this;
      }
      else if (step == 0)
        return this;

      for (Node c: children)
      {
        Node n = c.unfinished();
        if (n != null)
          return n;
      }

      return null;
    }
  }

  State old;
  Node root;
  int difficulty;
  String id, lesson;
  Integer step = 0;
  Node curNode;
  Button buttonDone = new Button("Fertig", 27.5, 15, 4.5, 2);
  Button buttonUndo = new Button("\u232B", 25.0, 15, 2.0, 2);

  TruthTree(State old, boolean propositional, Formula[] formulas, int difficulty, String id, String lesson)
  {
    this.old = old;
    root = new Node(formulas[0], null);
    Formula[][] f = {new Formula[formulas.length - 1]};
    for (int i = 1; i != formulas.length; ++i)
      f[0][i-1] = formulas[i];

    root.extend(f, 0);

    this.difficulty = difficulty;
    this.id = id;
    this.lesson = lesson;

    buttons.add(buttonDone);
    buttons.add(buttonUndo);

    buttons.add(new Button("K",   2.5, 15.0, 2, 2));
    buttons.add(new Button("D",   7.5, 15.0, 2, 2));
    buttons.add(new Button("I",  12.5, 15.0, 2, 2));

    buttons.add(new Button("NK",  5.0, 15.0, 2, 2));
    buttons.add(new Button("ND", 10.0, 15.0, 2, 2));
    buttons.add(new Button("NI", 15.0, 15.0, 2, 2));

    buttons.add(new Button("DN",  0.0, 15.0, 2, 2));
    buttons.add(new Button("B",  17.5, 15.0, 2, 2));
    buttons.add(new Button("NB", 20.0, 15.0, 2, 2));

    if (!propositional)
    {
      buttons.add(new Button("A",  0.0, 12.5, 2, 2));
      buttons.add(new Button("E",  5.0, 12.5, 2, 2));
      buttons.add(new Button("NA", 2.5, 12.5, 2, 2));
      buttons.add(new Button("NE", 7.5, 12.5, 2, 2));
    }

    setCurNode(null);
  }

  void paint(Node n, float y, boolean open)
  {
    noStroke();
    if (n == curNode)
      fill(RED);
    else if (n.closed != null)
      fill(BLUE);
    else
      fill(GRAY);

    rect(n.x + 2/50.0, y + 2/50.0, n.w - 4/50.0, 1 - 4/50.0);

    if (n.closed != null)
    {
      open = false;
      int d = 0;
      for (Node c = n; c != n.closed; c = c.parent)
        ++d;

      fill(BLUE);
      rect(n.x + 2/50.0, y - d + 2/50.0, n.w - 4/50.0, 1 - 4/50.0);
    }

    for (Node c: n.children)
      paint(c, y + 1, open);

    String tl1 = n.step > 0 ? "" + n.step : "";
    String tl = "";
    for (int i = 0; i != tl1.length(); ++i)
      tl += superscriptDigit.charAt(tl1.charAt(i) - '0');

    String s = formatFormula(n.formula);
    String tr1 = n.rule;
    String tr = "";
    for (int i = 0; i != tr1.length(); ++i)
      tr += superscriptLatin.charAt(tr1.charAt(i) - 'A');

    fill(BLACK);
    if (myTextWidth(tl + "  " + s + "  " + tr) < n.w - 8/50.0)
    {
      textAlign(LEFT);
      myText(tl, n.x + 4/50.0, y);
      textAlign(CENTER);
      myText(s, n.x + 0.5 * n.w, y);
      textAlign(RIGHT);
      myText(tr, n.x + n.w - 4/50.0, y);
    }
    else if (n == curNode)
    {
      textAlign(CENTER);
      myText("*", n.x + 0.5 * n.w, y);
      textAlign(RIGHT);
      myText("*:  " + tl + " " + s + "  " + tr, 32, 13.5);
    }
    else
    {
      textAlign(CENTER);
      myText("!", n.x + 0.5 * n.w, y);
    }

    if (n.children.isEmpty() && !open)
    {
      textAlign(CENTER);
      myText("" + markBallow, n.x + 0.5 * n.w, y + 1);
    }
  }

  void paint()
  {
    fill(BLACK);
    textAlign(CENTER);
    paint(root, 0, true);

    super.paint();
  }

  Node inside(Node n, float x, float y)
  {
    if (n.x <= x && x < n.x + n.w && 0 <= y && y < 1)
      return n;

    for (Node c: n.children)
    {
      Node r = inside(c, x, y - 1);
      if (r != null)
        return r;
    }
    
    return null;
  }

  void setCurNode(Node n)
  {
    curNode = n;
    if (difficulty == 0)
      for (int i = 2; i != buttons.size(); ++i)
        buttons.get(i).enabled = curNode != null && buttons.get(i).label.equals(tableauType(curNode.formula));
  }

  State click(float x, float y)
  {
    Button curButton = find(x,y);
    if (curButton != null)
    {
      if (curButton == buttonDone)
      {
        IntList l = new IntList();
        root.incorrect(l);
        if (l.size() != 0)
        {
          soundDing.play();
          setCurNode(null);
          return new Message(this, "Inkorrekt", "Schritt " + l.get(0) + " inkorrekt!", new String[]{"zur Aufgabe", "zum Men\u00FC"});
        }

        setCurNode(root.unfinished());
        if (curNode != null)
        {
          soundDing.play();
          return new Message(this, "Unvollst\u00E4ndig", "Die markierte Formel muss ausgewertet werden.", new String[]{"zur Aufgabe", "zum Men\u00FC"});
        }
        else
        {
          if (!id.equals(""))
            config.setSolved(id);

          soundWin.play();
          return new Message(this, "Alles richtig", lesson, new String[]{"zur Aufgabe", "zum Men\u00FC"});
        }
      }
      else if (curButton == buttonUndo)
      {
        if (step == 0)
        {
          soundDing.play();
          return this;
        }

        root.undo(step--);
        setCurNode(null);
        buttonUndo.enabled = step > 0;
      }
      else if (curNode != null)
      {
        if (curNode.step > 0 || curNode.parentCloses() || (difficulty < 2 && !curButton.label.equals(tableauType(curNode.formula))))
        {
          soundDing.play();
          return this;
        }

        if (curNode.execute(step+1, curButton.label))
          step++;
        else
          soundDing.play();

        setCurNode(null);
        buttonUndo.enabled = step > 0;
      }
    }
    else
    {
      Node n = inside(root, x, y);
      if (n != null && curNode != null && curNode.step == 0 && n.formula.isLiteral() && curNode.formula.isLiteral() &&
          curNode.findUpwards(n) && (difficulty >= 2 || (invert(n.formula).equals(curNode.formula))))
      {
        curNode.step = ++step;
        curNode.closed = n;
        n.closing++;
        buttonUndo.enabled = step > 0;
        setCurNode(null);
      }
      else
      {
        setCurNode(n);
      }
    }

    redraw();
    return this;
  }

  State feedback(int x)
  {
    if (x == 1)
      return old;

    return this;
  }

}

class ProblemTruthTree extends Problem
{
  boolean propositional;
  Formula f;
  int difficulty;
  String lesson;

  ProblemTruthTree(String id, String[] depends, String label, String infos[], boolean propositional, Formula f, int difficulty, String lesson)
  {
    super(id, depends, label, infos);
    this.propositional = propositional;
    this.f = f;
    this.difficulty = difficulty;
    this.lesson = lesson;
    if (this.infos.length == 0)
      this.infos = new String[]{formatFormula(f)};
  }

  State run(State old)
  {
    return new TruthTree(old, propositional, new Formula[]{f}, difficulty, id, lesson);
  }
}

class ProblemTruthTreeRandom extends Problem
{
  Range[] ranges;
  boolean propositional;

  ProblemTruthTreeRandom(boolean propositional)
  {
    super("", new String[]{propositional ? "truth_tree_expand" : "truth_tree_tut_quant"}, "Z", new String[]{"zuf\u00E4llige Formel mit Tableau l\u00F6sen"});
    this.ranges = new Range[]{
      new Range("Schwierigkeit", 1, 2, 2),
      new Range("Operatoren", 1, 12, 6),
      new Range(propositional ? "Satzbuchstaben" : "Individuenkonst.", 1, 5, 3)
    };
    this.propositional = propositional;
  }

  State run(State old)
  {
    return new ParamSelect(old, ranges){
      public State click(float x, float y){
        State s = super.click(x, y);
        if (s != old)
          return s;

        Formula f;
        do
        {
          f = make(propositional, false, ranges[2].value, max(ranges[2].value - 1, ranges[1].value));
        } while (!f.allTerminals(opIdentity).isEmpty());
        return new TruthTree(old, propositional, new Formula[]{f}, ranges[0].value, "", "");
      }
    };
  }
}

class FormulaEditorTruthTree extends FormulaEditor
{
  FormulaEditorTruthTree(State old, boolean propositional)
  {
    super(old, propositional, false, false);
  }
  State click(float x, float y)
  {
    State s = super.click(x, y);
    if (s == old && get() != null)
      return new TruthTree(old, propositional, new Formula[]{get()}, 2, "", "");

    return s;
  }
}

class ProblemTruthTreeSelect extends Problem
{
  FormulaEditorTruthTree fe;
  
  ProblemTruthTreeSelect(boolean propositional)
  {
    super("", new String[]{propositional ? "truth_tree_expand" : "truth_tree_tut_quant"}, "W", new String[]{"Formel f\u00FCr Tableau w\u00E4hlen"});
    fe = new FormulaEditorTruthTree(null, propositional);
  }

  State run(State old)
  {
    fe.old = old;
    return fe;
  }
}

Problem[][] makeProblemsTruthTreePropositional()
{
  JSONArray pi = loadJSONArray("problems_propositional_implications.json");
  JSONArray pe = loadJSONArray("problems_propositional_equivalences.json");
  Problem[][] r = new Problem[pi.size() + pe.size() + 2][];
  int n = 0;

  r[n++] = new Problem[]{
    new ProblemTruthTree("truth_tree_intro", new String[]{"lang_propositional_formula_types"}, "Intro", toArray(translations.getJSONArray("truth_tree_intro")), true, new Formula("--&p0vp1p2"), 0, ""),
    new ProblemTruthTree("truth_tree_close", new String[]{"truth_tree_intro"}, "Schließen", toArray(translations.getJSONArray("truth_tree_close")), true, new Formula("vp1&p0-p0"), 0, ""),
    new ProblemTruthTree("truth_tree_expand", new String[]{"truth_tree_close"}, "Expandieren", toArray(translations.getJSONArray("truth_tree_expand")), true, new Formula("&vp0vp1p2&p2-p2"), 0, ""),
    new ProblemInfo("truth_tree_prop", new String[]{"truth_tree_expand", "log_prop"}, "Erfüllbarkeit", toArray(translations.getJSONArray("truth_tree_prop")))
  };
  r[n++] = new Problem[]{
    new ProblemTruthTree("truth_tree_intro_nk", new String[]{"truth_tree_intro"}, "NK", toArray(translations.getJSONArray("truth_tree_intro_nk")), true, new Formula("-&p0p1"), 0, ""),
    new ProblemTruthTree("truth_tree_intro_nd", new String[]{"truth_tree_intro"}, "ND", toArray(translations.getJSONArray("truth_tree_intro_nd")), true, new Formula("-vp0p1"), 0, ""),
    new ProblemTruthTree("truth_tree_intro_i", new String[]{"truth_tree_intro"}, "I", toArray(translations.getJSONArray("truth_tree_intro_i")), true, new Formula(">p0p1"), 0, ""),
    new ProblemTruthTree("truth_tree_intro_ni", new String[]{"truth_tree_intro"}, "NI", toArray(translations.getJSONArray("truth_tree_intro_ni")), true, new Formula("->p0p1"), 0, ""),
    new ProblemTruthTree("truth_tree_intro_b", new String[]{"truth_tree_intro"}, "B", toArray(translations.getJSONArray("truth_tree_intro_b")), true, new Formula("#p0p1"), 0, ""),
    new ProblemTruthTree("truth_tree_intro_nb", new String[]{"truth_tree_intro"}, "NB", toArray(translations.getJSONArray("truth_tree_intro_nb")), true, new Formula("-#p0p1"), 0, ""),
    new ProblemTruthTreeRandom(true), new ProblemTruthTreeSelect(true)
  };

  for (int i = 0; i != pi.size(); ++i)
  {
    JSONArray s = pi.getJSONArray(i);
    r[n] = new Problem[s.size()];
    for (int j = 0; j != s.size(); ++j)
    {
      JSONObject o = s.getJSONObject(j);
      String id = "truth_tree_propositional_implication_" + o.getString("id");
      String[] depends = toArray(o.getJSONArray("depends"));
      for (int k = 0; k != depends.length; ++k)
        depends[k] = "truth_tree_propositional_implication_" + depends[k];

      String[] premises = toArray(o.getJSONArray("premises"));
      String target = o.getString("target");
      
      Formula f;
      if (premises.length == 0)
      {
        f = new Formula(target);
      }
      else
      {
        f = new Formula(premises[0]);
        for (int k = 1; k != premises.length; ++k)
          f = new Formula(opAND, -1, new Formula[]{f, new Formula(premises[k])});

        f = new Formula(opIFTHEN, -1, new Formula[]{f, new Formula(target)});
      }
      r[n][j] = new ProblemTruthTree(id, prepend("truth_tree_prop", depends), "" + char('A' + i) + char ('1' + j), new String[]{"Zu zeigen: \u22A7" + formatFormula(f)}, true, f.negate(), o.getInt("difficulty", 2), o.getString("lesson", ""));
    }
    ++n;
  }
  for (int i = 0; i != pe.size(); ++i)
  {
    JSONArray s = pe.getJSONArray(i);
    r[n] = new Problem[s.size()];
    for (int j = 0; j != s.size(); ++j)
    {
      JSONObject o = s.getJSONObject(j);
      String id = "truth_tree_propositional_equivalence_" + o.getString("id");
      String[] depends = toArray(o.getJSONArray("depends"));
      for (int k = 0; k != depends.length; ++k)
        depends[k] = "truth_tree_propositional_equivalence_" + depends[k];

      String[] formulas = toArray(o.getJSONArray("formulas"));
      Formula f = new Formula(opIFF, -1, new Formula[]{new Formula(formulas[0]), new Formula(formulas[1])});
      r[n][j] = new ProblemTruthTree(id, prepend("truth_tree_prop", depends), "" + char('A' + n) + char ('1' + j), new String[]{"Zu zeigen: \u22A7" + formatFormula(f)}, true, f.negate(), o.getInt("difficulty", 2), o.getString("lesson", ""));
    }
    ++n;
  }
  return r;
}

Problem[][] makeProblemsTruthTreePredicate()
{
  JSONArray pi = loadJSONArray("problems_predicate_implications.json");
  JSONArray pe = loadJSONArray("problems_predicate_equivalences.json");
  Problem[][] r = new Problem[pi.size() + pe.size() + 2][];
  int n = 0;

  r[n++] = new Problem[]{
      new ProblemTruthTree("truth_tree_tut_quant", new String[]{"lang_predicate_formula_types", "truth_tree_intro"}, "Q", new String[]{"Regeln f\u00FCr Quantoren"}, false, new Formula("-Ax0Ex1R0x0x1"), 0, ""),
    };
  r[n++] = new Problem[]{
      new ProblemTruthTreeRandom(false),
      new ProblemTruthTreeSelect(false)
    };

  for (int i = 0; i != pi.size(); ++i)
  {
    JSONArray s = pi.getJSONArray(i);
    r[n] = new Problem[s.size()];
    for (int j = 0; j != s.size(); ++j)
    {
      JSONObject o = s.getJSONObject(j);
      String id = "truth_tree_predicate_implication_" + o.getString("id");
      String[] depends = toArray(o.getJSONArray("depends"));
      for (int k = 0; k != depends.length; ++k)
        depends[k] = "truth_tree_predicate_implication_" + depends[k];

      String[] premises = toArray(o.getJSONArray("premises"));
      String target = o.getString("target");

      Formula f;
      if (premises.length == 0)
      {
        f = new Formula(target);
      }
      else
      {
        f = new Formula(premises[0]);
        for (int k = 1; k != premises.length; ++k)
          f = new Formula(opAND, -1, new Formula[]{f, new Formula(premises[k])});

        f = new Formula(opIFTHEN, -1, new Formula[]{f, new Formula(target)});
      }
      r[n][j] = new ProblemTruthTree(id, prepend("truth_tree_tut_quant", depends), "" + char('A' + i) + char ('1' + j), new String[]{"Zu zeigen: \u22A7" + formatFormula(f)}, false, f.negate(), o.getInt("difficulty", 2), o.getString("lesson", ""));
    }
    ++n;
  }
  for (int i = 0; i != pe.size(); ++i)
  {
    JSONArray s = pe.getJSONArray(i);
    r[n] = new Problem[s.size()];
    for (int j = 0; j != s.size(); ++j)
    {
      JSONObject o = s.getJSONObject(j);
      String id = "truth_tree_predicate_equivalence_" + o.getString("id");
      String[] depends = toArray(o.getJSONArray("depends"));
      for (int k = 0; k != depends.length; ++k)
        depends[k] = "truth_tree_predicate_equivalence_" + depends[k];

      String[] formulas = toArray(o.getJSONArray("formulas"));
      Formula f = new Formula(opIFF, -1, new Formula[]{new Formula(formulas[0]), new Formula(formulas[1])});
      r[n][j] = new ProblemTruthTree(id, prepend("truth_tree_tut_quant", depends), "" + char('A' + n) + char ('1' + j), new String[]{"Zu zeigen: \u22A7" + formatFormula(f)}, false, f.negate(), o.getInt("difficulty", 2), o.getString("lesson", ""));
    }
    ++n;
  }
  return r;
}
